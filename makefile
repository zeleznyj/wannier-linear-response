SHELL = /bin/sh

json_fortran_path = /home/zeleznyj/bin/json-fortran/lib
link = ${MKLROOT}/lib/intel64/libmkl_blas95_lp64.a ${MKLROOT}/lib/intel64/libmkl_lapack95_lp64.a -Wl,--start-group ${MKLROOT}/lib/intel64/libmkl_intel_lp64.a ${MKLROOT}/lib/intel64/libmkl_core.a ${MKLROOT}/lib/intel64/libmkl_sequential.a ${MKLROOT}/lib/intel64/libmkl_core.a -Wl,--end-group -lpthread -lm -ldl $(json_fortran_path)/libjsonfortran.a
compile =  -I${MKLROOT}/include/intel64/lp64 -I${MKLROOT}/include  -I$(json_fortran_path)/mod

objects = constants.o input.o funcs.o k_integrate.o generic_model.o wann_model.o tb_model.o fplo_model.o Rashba_model.o eval_k.o

DEBUG ?= 0
ifeq ($(DEBUG),1)
    opt = -warn all -check all -traceback -g -debug extended  -check bounds -fp-stack-check -gen-interfaces -warn interfaces
else ifeq ($(DEBUG),0)
    opt = -O3
else ifeq ($(DEBUG),2)
    opt = -O3 -qopt-report
endif

MIC ?= 0
ifeq ($(MIC),1)
    opt += -mmic
endif

f90comp = mpiifort

linres.x: $(objects) linres.o
	$(f90comp) -o linres.x $(objects) linres.o $(opt) $(link)

linres-mic.x: $(objects) linres.o
	$(f90comp) -o linres.x $(objects) linres.o $(opt) $(link)

bands.x: $(objects) bands.o
	$(f90comp) -o bands.x $(objects) bands.o $(opt) $(link)

mag_moms.x: $(objects) mag_moms.o
	$(f90comp) -o mag_moms.x $(objects) mag_moms.o $(opt) $(link)

%.o: %.f90
	$(f90comp) -c $< $(compile) $(opt)

.PHONY: clean
clean:
	rm *.o *.mod
