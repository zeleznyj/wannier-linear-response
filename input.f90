module input
use constants
use json_module
use iso_fortran_env, only: stdin => input_unit

implicit none
!--------------------------------------------------------------------------
!Contains all the necessary input parameters and subroutine for reading them.
!--------------------------------------------------------------------------

type params_type
    !--------------------------------------------------------------------------
    !This class is used within input_type to store all the iterable parameters
    !
    !The parameters are stored as a 1D array, which contains all combinations of
    !parameters
    !Right now the iterable parameters are: 
    !Formula, Fermi level, Gamma, Projection, Omega
    !This could easily be expanded in the future
    !
    !That is for each l, we have (formula,Ef,gamma,projection, omega)
    !
    !To initialize do
    !    params%init(Formulas,Efs,gammas)
    !or
    !    params%init(Formulas,Efs,gammas,projections, omegas)
    !
    !If projections are not included then only the total projection is added.
    !If omegas are not included then [0._dp] is added.
    !
    !The parameter ranges for individual parameters can be accessed through:
    !
    !-params%formulas
    !-params%Efs
    !-params%gammas
    !-params%projs
    !-params%omegas
    !
    !The parameters for a parameter set with index l can be accessed by:
    !-param%formula(l)
    !-param%Ef(l)
    !-param%gamma(l)
    !-param%projection(l)
    !-param%omega(l)
    !
    !Or using get, it is possible to get positions in the individual parameter ranges:
    !
    ![p1,p2,p3,p4,p5] = param%get(l)
    !param%formula(l) = param%formulas(p1) ...
    !
    !Conversely the index of a parameter set can be obtained through get_r:
    !
    !For (formula=params%formulas(p1),Ef=params%Efs(p2),..)
    !l = params%get_r([p1,p2,p3,p4,p5])
    !
    !--------------------------------------------------------------------------

    integer :: n_params
    integer :: n_gammas
    integer :: n_Efs
    integer :: n_formulas
    integer :: n_projs
    integer :: n_projs_inp
    integer :: n_omegas

    real(dp), allocatable :: gammas(:)
    real(dp), allocatable :: Efs(:)
    integer, allocatable :: formulas(:)
    integer, allocatable :: projs(:)
    real(dp), allocatable :: omegas(:)

    integer, allocatable :: params_list(:,:)

    logical :: initialized = .false.

    contains
        procedure :: init => init_params
        procedure :: get
        procedure :: gamma => get_gamma
        procedure :: Ef => get_Ef
        procedure :: formula => get_formula
        procedure :: proj => get_proj
        procedure :: omega => get_omega
        procedure :: get_r
end type

type k_resolved_type
   integer :: d
   integer :: normal
   real(dp) :: k_fixed
   character(len=:), allocatable :: output_file

end type

type input_type
    !These are variables that have to be specified as an input
    character(len=:), allocatable :: linres
    character(len=:), allocatable :: model
    character(len=:), allocatable :: mode
    integer :: nk(3)
    real(dp) :: eps
    real(dp) :: T
    real(dp) :: zero_cutoff
    type(params_type) :: params
    type(k_resolved_type) :: k_resolved_input

    !these variables are derived from the previous variables and 
    !don't have to be specified as an input
    !They can be set automatically by the def_derived_vars method
    integer :: n_params
    integer :: n_projs_inp !This should be zero if no projection is done
    integer :: n_op1
    integer :: n_X
    integer :: order
    integer :: rank

    contains
        procedure :: def_derived_vars 
        procedure :: test
end type

type projs_type
    integer :: n_atoms
    integer, allocatable :: n_orbs(:)
    integer, allocatable :: orbs(:,:)
end type

type bands_input_type
    integer :: n_kpts
    integer :: n_sym_kpts
    real(dp), allocatable :: sym_kpts(:,:)
    character(1), allocatable :: kpts_names(:)
    logical :: print_Ham
    logical :: plot_3d
    character(len=:), allocatable :: mode
end type

contains

function parse_input() result(json)


    type(json_file) :: json

    character(len=32) :: arg

    character(len=32) :: filename
    logical :: found

    integer :: i

    found = .false.
    do i=1,command_argument_count()

        call get_command_argument(i,arg)
        if ( arg == '-i' ) then
            call get_command_argument(i+1,filename)
            found = .true.
        endif

    end do
    if (.not. found) stop 'You must specify input file'

    call json%initialize(comment_char = '#')
    call json%load_file(filename)

end function

subroutine json_get_matrix(json,variable_path,matrix,is_matrix,trans)
    !this mostly taken from https://github.com/jacobwilliams/json-fortran/issues/203
    type(json_file), intent(inout) :: json
    character(len=:), allocatable, intent(in) :: variable_path
    real(dp), allocatable, intent(out) :: matrix(:,:)
    logical, intent(out) :: is_matrix
    logical, intent(in), optional :: trans

    type(json_value),pointer :: matrix_p, child
    type(json_core) :: core
    real(dp), allocatable :: vec(:)
    integer :: i,n_rows,n_cols
    logical :: trans_in

    if (.not. present(trans) ) then
        trans_in = .false.
    else
        trans_in = trans
    endif

    call json%matrix_info(variable_path,is_matrix,n_sets=n_rows,set_size=n_cols)
    if (is_matrix) then
        if (.not. trans_in) then
            allocate(matrix(n_rows,n_cols))
        else
            allocate(matrix(n_cols,n_rows))
        endif
        allocate(vec(n_cols))
        call json%get(variable_path,matrix_p)
        do i=1,n_rows
            call core%get_child(matrix_p,i,child)
            call core%get(child,vec)
            if (.not. trans_in) then
                matrix(i,:) = vec
            else
                matrix(:,i) = vec
            endif
        end do
    end if

end subroutine


subroutine read_input(inp)
    type(input_type), intent(out) :: inp

    type(json_file) :: json
    logical :: found
    integer, allocatable :: nk(:)
    integer :: nk2,n_gammas,i
    real(dp) :: gamma_min,gamma_max,gam
    real(dp), allocatable :: gammas(:)
    integer, allocatable :: formulas(:),atoms(:)
    integer :: nEf
    real(dp) :: de,Ef
    real(dp), allocatable :: Efs(:), Efs2(:)
    integer ::  f1
    integer, allocatable :: f1a(:)
    integer :: n_omegas
    real(dp) :: omega_min,omega_max,omg
    real(dp), allocatable :: omegas(:)

    json = parse_input()

    call json%get('calculation.calc',inp%linres,found)
    if (.not. found) inp%linres = 'None'
    
    call json%get('model.model_type',inp%model,found)
    if (.not. found) inp%model = 'None'

    call json%get('calculation.mode',inp%mode,found)
    if (.not. found) inp%mode = 'response'

    !read the k-points
    !can take either nk, [nk] or [nk,nk,nk] as input
    call json%get('calculation.nk',nk,found)
    if (.not. found) then
        call json%get('calculation.nk',nk2,found)
        if (.not. found) nk2 = -1
        allocate(nk(1))
        nk = nk2
    endif 
    if ( size(nk) == 3) then
        inp%nk = nk
    else if ( size(nk) == 1 ) then
            inp%nk = (/ nk, nk, nk /)
    else
        stop 'nk must contain exactly 1 or three values'
    endif
    
    !read gammas
    call json%get('calculation.gammas.min',gamma_min,found)
    if (found) then
        call json%get('calculation.gammas.max',gamma_max,found)
        if (.not. found) stop 'When gammas.min is present gammas.max must be present too.'

        call json%get('calculation.gammas.n',n_gammas,found)
        if (.not. found) stop 'When gammas.min is present n_gammas must be present too.'

        if (gamma_min < 0._dp .or. gamma_max < 0._dp) then
            stop 'gammas must be positive'
        end if

        allocate(gammas(n_gammas))
        if (n_gammas == 1) then
            gammas(1) = gamma_min
        else
            do i=1,n_gammas
                gammas(i) = 10._dp ** ( (log10(gamma_max)-log10(gamma_min)) * &
                    (i-1)/(n_gammas-1) + log10(gamma_min))
            end do
        endif
    else
        call json%get('calculation.gammas',gam,found)
        if (found) then
            allocate(gammas(1))
            gammas = gam
        else
            call json%get('calculation.gammas',gammas,found)
        endif
    endif

    !Fermi level
    call json%get('calculation.fermi',Ef,found)
    if (found) then
        allocate(Efs2(1))
        Efs2(1) = Ef
    else
        call json%get('calculation.fermi',Efs,found)
        if (found) then
            allocate(Efs2(size(Efs)))
            Efs2 = Efs
        else
            call json%get('calculation.fermi.n',nEf,found)
            if (found) then
                call json%get('calculation.fermi.Ef',Ef,found)
                if (.not. found) stop ' If fermi.n is defined then fermi.Ef &
                        must be specified'
                call json%get('calculation.fermi.dE',dE,found)
                if (.not. found) stop ' if Fermi level is not provided as a list of values then fermi.dE &
                        must be specified'
                if ( mod(nEf,2) == 0 ) then
                    nEf = nEf + 1
                end if
                allocate(Efs2(nEf))
                if (nEf == 1) then
                    Efs2(1) = Ef
                else
                    do i=1,nEf
                        Efs2(i) = Ef-dE + (i-1)/dble(nEf-1) * ( 2*dE )
                    end do
                endif
            endif
        endif
    endif

    ! read omegas
    call json%get('calculation.omegas.min',omega_min,found)
    if (found) then
        call json%get('calculation.omegas.max',omega_max,found)
        if (.not. found) stop 'When omegas.min is present omegas.max must be present too.'

        call json%get('calculation.omegas.n',n_omegas,found)
        if (.not. found) stop 'When omegas.min is present n_omegas must be present too.'

        if (omega_min < 0._dp .or. omega_max < 0._dp) stop 'omega must be positive'

        allocate(omegas(n_omegas))
        if (n_omegas == 1) then
            omegas(1) = omega_min
        else
            do i=1,n_omegas
                omegas(i) = (omega_max-omega_min)*(i-1)/(n_omegas-1) + omega_min
            end do
        endif
    else
        call json%get('calculation.omegas',omg,found)
        if (found) then
            allocate(omegas(1))
            omegas(1) = omg
        else
            call json%get('calculation.omegas',omegas,found)
        endif
    endif

    if (.not. allocated(omegas) )then
        allocate(omegas(1))
        omegas(1) = 0._dp
    endif
    
    call json%get('calculation.formulas',f1,found)
    if (found) then
        allocate(formulas(1))
        formulas(1) = f1
    else
        call json%get('calculation.formulas',f1a,found)
        if (found) then
            allocate(formulas(size(f1a)))
            formulas = f1a
        endif
    endif

    call json%get('calculation.projections',atoms,found)
    if (found) then
        inp%n_projs_inp = size(atoms)
    else
        inp%n_projs_inp = 0
    endif


    call json%get('calculation.epsilon',inp%eps,found)
    if (.not. found) inp%eps = -1._dp

    call json%get('calculation.temperature',inp%T,found)
    if (.not. found) inp%T = -1._dp

    if ( (.not. allocated(formulas)) .or. &
         (.not. allocated(Efs2)) .or. &
         (.not. allocated(gammas)) ) then
         stop ' You must specify formulas, Gamma and Ef'
     endif
    
    if (allocated(atoms)) then
        call inp%params%init(formulas,Efs2,gammas,atoms,omegas=omegas)
    else
        call inp%params%init(formulas,Efs2,gammas,omegas=omegas)
    endif

    if (inp%mode == 'k_resolved' .or. inp%mode == 'bands') then

        call json%get('calculation.k_resolved.output_file',inp%k_resolved_input%output_file,found)
        if (.not. found) inp%k_resolved_input%output_file = 'k_resolved'

        call json%get('calculation.k_resolved.dimension',inp%k_resolved_input%d,found)
        if (.not. found) inp%k_resolved_input%d = 3

        if (inp%k_resolved_input%d == 2) then

            call json%get('calculation.k_resolved.normal',inp%k_resolved_input%normal,found)
            if (.not. found) stop 'When k_resolved.dimension=2 you must specify k_resolved.normal'

            call json%get('calculation.k_resolved.k_fixed',inp%k_resolved_input%k_fixed,found)
            if (.not. found) stop 'When k_resolved.dimension=2 you must specify k_resolved.k_fixed'

        endif 
    endif

    call json%get('calculation.zero_cutoff',inp%zero_cutoff,found)
    if (.not. found) inp%zero_cutoff = 1000

    call inp%def_derived_vars()
    call inp%test()

end subroutine

subroutine def_derived_vars(self)
    class(input_type) :: self

    real(dp), allocatable :: formula_orders(:)
    integer :: i,n_fors

    self%n_projs_inp = self%params%n_projs_inp
    self%n_params = self%params%n_params

    if ( self%params%initialized ) then
        n_fors = self%params%n_formulas
        allocate(formula_orders(self%params%n_formulas))
        do i=1,n_fors
            formula_orders(i) = get_formula_order(self%params%formulas(i))
        end do
        do i=2,n_fors
            if ( formula_orders(i) /= formula_orders(1) ) then
                stop 'You cannot mix formulas corrresponding to different response orders'
            endif
        enddo
        self%order = formula_orders(1)
    endif

    if (self%linres == 'cisp') then
        self%n_op1 = self%params%n_projs*3
    else if (self%linres == 'cond') then
        self%n_op1 = 3
    else if (self%linres == 'she') then
        self%n_op1 = 9
    endif

    if ( self%order == 1 ) then
        if (self%linres == 'she') then
            self%n_X = 27
        else
            self%n_X = 9
        endif
    else if (self%order == 2 ) then
        if (self%linres == 'she') then
            self%n_X = 27*3
        else
            self%n_X = 27
        endif
    endif


end subroutine

subroutine test(self,mode)
    class(input_type) :: self
    character(len=:), allocatable, intent(in), optional :: mode
    
    character(len=:), allocatable :: mode2
    integer :: i

    if (.not. present(mode)) mode2 = 'linres'

    !calc
    if ( mode2 == 'linres' ) then
        if ( self%linres == 'None' ) stop 'Calc must be defined.'
        !if (.not. any((/ 'cond','cisp','she'/) == self%linres)) then
        if (.not. any( [ character(len=5) ::  'cond','cisp','she' ] == self%linres)) then
            stop 'calc must be one of cond, cisp, she'
        endif
    endif

    !model
    if (mode2 == 'linres' ) then
        if ( self%model == 'None' ) stop 'Model must be defined.'
        if (.not. any( [ character(len=6) ::  'tb','wann','Rashba','fplo' ] == self%model)) then
            stop 'model must be one of wann,tb,Rashba,fplo'
        endif
    endif

    !nk
    if (mode2 == 'linres' ) then
        if (self%nk(1) == -1) stop 'nk must be defined'
        if ( any( self%nk <= 0 ) ) stop 'nk must be positive'
    endif

    !gammas
    if (mode2 == 'linres' ) then
        if (.not. self%params%initialized ) then
            stop 'params must be set.'
        endif
    endif

    !epsilon and T
    if (mode2 == 'linres' ) then
        if ( any (self%params%formulas == 1)) then
            if ( self%eps == -1._dp ) stop 'Epsilon must be specified if formula 1 is used'
        endif
        if ( any (self%params%formulas == [3,21,22,23])) then
            if ( self%T == -1._dp ) stop 'temperature must be specified if formulas 3,21,22,23 are used'
        endif
    endif

    !omega
    if (mode2 == 'linres' ) then
        if ( any (self%params%formulas == [101,102])) then
            if ( self%params%n_omegas == 1 ) then
                if ( self%params%omegas(1) == 0._dp ) stop 'Omega must be specified if formulas 101 and 102 are used'
            end if
        endif
    endif

    !projections
    if (mode2 == 'linres' ) then
        if ( self%n_projs_inp > 0 .and. ( self%linres == 'cond' .or. &
                                     self%linres == 'she' .or. & 
                                     self%linres == 'cond2') ) then
            stop 'Projections only allowed for cisp.'
        endif
    endif

end subroutine

subroutine init_params(self,formulas,Efs,gammas,projs, omegas)
    class(params_type) :: self
    integer, intent(in) :: formulas(:)
    real(dp), intent(in) :: Efs(:)
    real(dp), intent(in) :: gammas(:)
    integer, intent(in), optional :: projs(:)
    real(dp), intent(in), optional :: omegas(:)

    integer :: i,j,k,l,m,p

    self%n_gammas = size(gammas)
    self%n_Efs = size(Efs)
    self%n_formulas = size(formulas)

    if (.not. present(projs) ) then
        self%n_projs_inp = 0
        self%n_projs = 1
        self%projs = [0]
    else
        self%n_projs = size(projs)
        self%n_projs_inp = self%n_projs
        self%projs = projs
    endif

    if (.not. present(omegas) ) then
        self%n_omegas = 1
        self%omegas = [0._dp]
    else
        self%n_omegas = size(omegas)
        self%omegas = omegas
    endif

    !allocate(self%gammas(self%n_gammas))
    !allocate(self%Efs(self%n_Efs))
    !allocate(self%formulas(self%n_formulas))

    self%gammas = gammas
    self%Efs = Efs
    self%formulas = formulas

    self%n_params = self%n_gammas * self%n_Efs * self%n_formulas * self%n_projs * self%n_omegas
    allocate(self%params_list(self%n_params,5))

    l = 0
    do i=1,self%n_formulas
        do j=1,self%n_Efs
            do k=1,self%n_gammas
                do m=1,self%n_projs
                    do p=1,self%n_omegas
                        l = l + 1
                        self%params_list(l,:) = [i,j,k,m,p]
                    enddo
                end do
            end do
        end do
    end do

    self%initialized = .true.

end subroutine

function get(self,l) result(par)
    class(params_type) :: self
    integer, intent(in) :: l
    integer :: par(5)

    par = self%params_list(l,:)
end function

function get_formula(self,l) result(var)
    class(params_type) :: self
    integer, intent(in) :: l
    integer :: var

    integer :: i

    i = self%params_list(l,1)
    var = self%formulas(i)
end function

function get_Ef(self,l) result(var)
    class(params_type) :: self
    integer, intent(in) :: l
    real(dp) :: var

    integer :: i 

    i = self%params_list(l,2)
    var = self%Efs(i)
end function

function get_gamma(self,l) result(var)
    class(params_type) :: self
    integer, intent(in) :: l
    real(dp) :: var

    integer :: i 

    i = self%params_list(l,3)
    var = self%gammas(i)
end function

function get_proj(self,l) result(var)
    class(params_type) :: self
    integer, intent(in) :: l
    integer :: var

    integer :: i 

    i = self%params_list(l,4)
    var = self%projs(i)
end function

function get_omega(self,l) result(var)
    class(params_type) :: self
    integer, intent(in) :: l
    real(dp) :: var

    integer :: i 

    i = self%params_list(l,5)
    var = self%omegas(i)
end function

function get_r(self,par) result(l)
    class(params_type) :: self
    integer, intent(in) :: par(5)
    integer :: l

    l = (par(1)-1) * self%n_Efs * self%n_gammas * self%n_projs * self%n_omegas &
        + (par(2)-1) * self%n_gammas * self%n_projs * self%n_omegas &
        + (par(3)-1) * self%n_projs * self%n_omegas &
        + (par(4)-1) * self%n_omegas &
        + par(5)

    if (.not. all( par == self%get(l) ) ) then
        write(*,*) par,l
        write(*,*) self%get(l)
        stop 'error in get_r'
    endif
end function

subroutine write_input()

    type(json_file) :: json

    json = parse_input()

    call json%print_file()

end subroutine

subroutine read_projs(projs)
    type(projs_type), intent(out) :: projs

    integer :: i
    integer :: max_norb

    type(json_file) :: json
    type(json_core) :: core
    integer :: var_type
    logical :: found
    type(json_value), pointer :: projs_p,child
    integer, allocatable :: orbs(:)

    json = parse_input()

    call json%info('model.def_projections',found,var_type,projs%n_atoms)
    if (.not. found) stop 'def_projections not found'

    call json%get('model.def_projections',projs_p)

    allocate(projs%n_orbs(projs%n_atoms))
    do i=1,projs%n_atoms
        call core%get_child(projs_p,i,child)
        call core%get(child,orbs)
        projs%n_orbs(i) = size(orbs)
    end do
    max_norb = maxval(projs%n_orbs)
    allocate(projs%orbs(max_norb,projs%n_atoms))

    projs%orbs = 0._dp
    do i=1,projs%n_atoms
        call core%get_child(projs_p,i,child)
        call core%get(child,orbs)
        projs%orbs(1:projs%n_orbs(i),i) = orbs
    end do

end subroutine

subroutine read_bands_input(band_inp)

    type(bands_input_type), intent(out) :: band_inp

    type(json_file) :: json
    character(len=:), allocatable :: variable_path
    logical :: found,is_matrix

    json = parse_input()

    call json%get('bands.mode',band_inp%mode,found)
    if (.not. found) band_inp%mode = 'path'

    call json%get('bands.n_kpts',band_inp%n_kpts,found)
    if (.not. found) then
        if (band_inp%mode /= 'k_resolved') then
            stop 'bands.n_kpts input missing'
        endif
    endif

    call json%get('bands.3d',band_inp%plot_3d,found)
    if (.not. found) band_inp%plot_3d = .false.

    if (.not. band_inp%plot_3d .and. band_inp%mode /= 'k_resolved') then
        variable_path = 'bands.kpts'
        call json_get_matrix(json,variable_path,band_inp%sym_kpts,is_matrix,trans=.true.)
        if (.not. is_matrix) stop 'bands.kpts missing or format is wrong'
        if (size(band_inp%sym_kpts,1) /= 3) stop 'wrong shape of the bands.kpts array'
        band_inp%n_sym_kpts = size(band_inp%sym_kpts,2)
    endif

    call json%get('bands.print_hamiltonian',band_inp%print_ham,found)
    if (.not. found) band_inp%print_ham = .false.


end subroutine

subroutine read_structure(latt_vecs)
    !--------------------------------------------------------------------------
    !Reads the lattice vectors from the input file
    !--------------------------------------------------------------------------

    real(dp),intent(out) :: latt_vecs(3,3)

    real(dp), allocatable :: latt_vecs_alloc(:,:)
    type(json_file) :: json
    real(dp) :: latt_scale
    logical :: found, is_matrix
    integer :: rows,cols
    character(len=:), allocatable :: variable_path
    character(len=:), allocatable :: latt_unit

    json = parse_input()

    call json%matrix_info('model.lattice_vectors',is_matrix,n_sets=rows,set_size=cols)
    if (.not. is_matrix) stop 'wrong lattice_vectors input'
    if (rows /= 3 .or. cols /= 3) stop 'wrong lattice_vectors input'

    variable_path = 'model.lattice_vectors'
    call json_get_matrix(json,variable_path,latt_vecs_alloc,is_matrix,trans=.true.)

    latt_vecs = latt_vecs_alloc

    call json%get('model.lattice_scale',latt_scale,found)
    if (found) latt_vecs = latt_scale * latt_vecs
    
    call json%get('model.lattice_unit',latt_unit,found)
    if (found) then
        if (latt_unit == 'a.u.') then
            latt_vecs = latt_vecs * bohr
        elseif (latt_unit /= 'ang') then
            stop 'Unrecognized latt_unit, only "a.u." and "ang" allowed' 
        endif
    endif
end subroutine

function get_formula_order(n_for) result(order)
    integer, intent(in) :: n_for
    integer :: order
    
    if (any(n_for == [1,2,3,11,12,101,102])) order = 1

    if (any(n_for == [21,22,23])) order = 2

end function

function get_tensor_rank(inp) result(rank)
    type(input_type), intent(in) :: inp
    integer :: rank

    if (inp%linres == 'cisp' .or. inp%linres == 'cond') then
        rank = 1
    elseif (inp%linres == 'she') then
        rank = 2
    endif
    rank = rank + inp%order
end function

end module



