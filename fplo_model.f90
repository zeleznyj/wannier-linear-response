module fplo_model_mod
!--------------------------------------------------------------------------
!This module reads a Wannier Hamiltonian produced by the FPLO DFT code.
!--------------------------------------------------------------------------
use mpi
use generic_model
use constants
use input
use blas95
use json_module
use funcs, only: test_projs,rotate_spin_matrix

implicit none
type, extends (gen_model) :: fplo_model

    integer :: n_Hrs
    integer :: n_wann
    integer :: num_orb
    integer :: nspin
    integer :: half
    integer, allocatable :: half_leno(:)
    real(dp), allocatable :: rhos(:,:)
    integer, allocatable :: orbs(:,:)
    complex(dp), allocatable :: Hr(:)
    complex(dp), allocatable :: H_rhos(:,:)
    complex(dp), allocatable :: exp_tot(:)
    real(dp) :: exp_tot_k(3)
    integer, allocatable :: len_orb(:)
    type(projs_type) :: projs
    logical :: projs_defined
    real(dp) :: q_axis(3)
    real(dp) :: latt_vecs(3,3)
    
    contains
        procedure :: init
        procedure :: create_smat
        procedure :: create_Hk
        procedure :: create_vk
        procedure :: def_projs
        procedure :: create_vk_dk
        procedure :: fourrier_trans
        procedure :: get_latt_vecs => get_latt_vecs_fplo
end type

contains

subroutine init(self)
    !--------------------------------------------------------------------------
    !Reads the hamiltonian matrix elements in the tight-binding basis outputted by
    !sd_model.py.
    !
    !Returns (module variables):
    !   n_orb (integer): number of basis functions
    !   n_Hrs (integer): number of Hamiltonian matrix elements
    !   rhos (real(3,n_orb)): rhos that appear in the exponential for each matrix element
    !       rho is given in fractional coordinates and is defined as:
    !   orbs (integer(3,n_orb): the orbitals of each matrix element
    !   Hr (complex(:)): the Hamiltonian matrix elements stored as:
    !       Hr(i) = < orbs(1,i)|Hr|orbs(2,i) >
    !--------------------------------------------------------------------------

    class(fplo_model) :: self
    
    integer :: j,a,i
    integer :: u,n,m
    real(dp) :: r(3)
    real(dp) :: val(2)
    real(dp) :: latt_vecs(3,3)
    integer :: startpoint 
    integer :: x
    integer :: l_orb
    character(50) :: detect
    character(50) :: detective
    type(json_file) :: json
    character(len=:), allocatable :: ham_filename
    logical :: found,found2
    real(dp) :: rot_angle_t
    real(dp), allocatable :: q_axis_t(:)
    integer :: current_spin

    json = parse_input()
    call json%get('model.fplo.hamiltonian_file',ham_filename,found)
    if (.not. found) then
        stop 'model.fplo.hamiltonian_file must be present'
    endif

    call json%get('model.fplo.quantization_axis',q_axis_t,found)
    if (found) then
        if (size(q_axis_t) /= 3) then
            stop 'wrong model.fplo.s_rot_axis dimension'
        endif
        self%q_axis = q_axis_t/norm2(q_axis_t)
    endif

    if (.not. found) then
        write(*,*) 'quantization_axis not defined in the input file, setting to [0,0,1]'
        self%q_axis = [0,0,1]
    endif

    open(newunit=u, file=ham_filename, status='old')
   
    do i=1, 3
       read(u,*) 
    end do
    do i=1, 3
        read(u,*) latt_vecs(:,i)
        self%latt_vecs(:,i)=latt_vecs(:,i)*bohr
    end do
    do i=1, 9
        read(u,*)
    end do
    read(u,*) self%n_wann
    read(u,*)
    read(u,*) self%nspin
    if (self%nspin == 2) then
        self%n_wann = 2 * self%n_wann
    endif
    
    if (self%nspin == 1) then
        self%num_orb = self%n_wann**2
    else
        self%num_orb = self%n_wann**2 / 2
    endif


    allocate(self%len_orb(self%num_orb))
    self%len_orb(:)=0_dp
    self%n_orb = self%n_wann

    if (self%nspin == 1) then
        startpoint = 2 * self%n_wann + 22
    else
        startpoint = 2 * self%n_wann/2 + 22
    endif

    rewind u
    do i=1, startpoint
        read(u,*)
    end do

! We calculate here the number of values for each orbital combination
    x = 0
    current_spin = 1
    do 
        read(u,*) detect
        if (detect == 'Tij') then
                x = x + 1
                l_orb = - 2
        endif
        if (detect == 'end') then
                self%len_orb(x) = l_orb
                l_orb = l_orb - 1
                if ((detect==detective) .and. (detective=='end')) then
                    if (self%nspin == 1) then
                        exit
                    else
                        if (current_spin == 1) then
                            current_spin = 2
                        else
                            exit
                        endif
                    endif
                endif
        endif
        detective = detect
        l_orb = l_orb + 1
    end do
    rewind u
    
    self%n_Hrs = 0
    startpoint = startpoint + 1
    do i=1, startpoint
        read(u,*)
    end do
    if (self%nspin == 1) then
        self%half = self%n_wann*(self%n_wann + 1)/2
    else
        self%half = (self%n_wann/2) * ((self%n_wann/2) + 1)
    endif
    allocate(self%orbs(2,self%half))
    allocate(self%half_leno(self%half))

! Just keep the upper part of the Hamiltonian and count number of values
    x = 1
    do i=1, self%num_orb
        read(u,*) n,m
        if (n<m) then
                do j=1, self%len_orb(i)
                    read(u,*)
                end do
        else
                l_orb=0
                do j=1, self%len_orb(i)
                    read(u,*) 
                    self%n_Hrs = self%n_Hrs + 1
                    l_orb = l_orb + 1
                end do
                self%half_leno(x) = l_orb
                x = x + 1
        endif
        read(u,*)
        read(u,*)
        if (self%nspin == 2) then
            if (i == self%num_orb / 2) then
                read(u,*)
                read(u,*)
                read(u,*)
            endif
        endif
    end do

    rewind u
    
    allocate(self%rhos(3,self%n_Hrs))
    allocate(self%Hr(self%n_Hrs))
    allocate(self%H_rhos(3,self%n_Hrs))
    self%rhos(:,:)=0_dp
    self%Hr(:)=0_dp
    self%H_rhos(:,:)=0_dp
    do i=1, startpoint
        read(u,*)
    end do

! Save the data for the upper part
    x = 0
    a = 0
    current_spin = 1
    do i=1, self%num_orb
        read(u,*) n,m
        if (n<m) then
            do j=1, self%len_orb(i)
                read(u,*)
            end do
        else  
            a = a + 1       
            if (self%nspin == 1) then
                self%orbs(1,a) = n
                self%orbs(2,a) = m
            else
                if (current_spin == 1) then
                    self%orbs(1,a) = 2*n-1
                    self%orbs(2,a) = 2*m-1
                else
                    self%orbs(1,a) = 2*n
                    self%orbs(2,a) = 2*m
                endif
            endif
            do j=1, self%len_orb(i)
                x = x + 1
                read(u,*) r(:), val(:)
                r(:)= r(:)*bohr
                self%rhos(:,x) = r(:)
                self%Hr(x) = dcmplx(val(1),val(2))
                self%H_rhos(:,x) = ii*r(:)*self%Hr(x)
            end do
        endif
        read(u,*)
        read(u,*)
        if (self%nspin == 2) then
            if (i == self%num_orb / 2) then 
                read(u,*)
                read(u,*)
                read(u,*)
                current_spin = 2
            endif
        endif
    end do

    !x = 26
    !write(*,*) self%orbs(1,x)
    !write(*,*) self%orbs(2,x)
    !write(*,*) self%half_leno(x)
    !a = 0
    !do i=1,26-1
    !    a = a + self%half_leno(i)
    !end do
    !write(*,*) self%rhos(:,a+1:a+self%half_leno(x))
    !write(*,*) self%Hr(a+1:a+self%half_leno(x))

    close(u)
end subroutine

subroutine fourrier_trans(self,k,R_vec,K_mat)
    class(fplo_model) :: self
    real(dp), intent(in) :: k(3)
    complex(dp), intent(in) :: R_vec(:)
    complex(dp), intent(out) :: K_mat(:,:)

    integer :: a,j,i,n,m
    logical :: calculate_exp
    real(dp) :: r(3)

    if (allocated(self%exp_tot)) then
        if (norm2(self%exp_tot_k-k) > norm2(self%exp_tot_k+k)*1e-13) then
            calculate_exp = .true.
        else
            calculate_exp = .false.
        endif
    else 
         allocate(self%exp_tot(self%n_Hrs))
         calculate_exp = .true.
    endif

    if (calculate_exp) then
        do i=1, self%n_Hrs
            r = self%rhos(:,i)
            self%exp_tot(i)= dconjg(exp(ii*dot_product(k,r)))
        end do
        self%exp_tot_k = k
    endif

    j = 0
    a = 1
    do i=1, self%half
        n = self%orbs(1,i)
        m = self%orbs(2,i)
        j = j + self%half_leno(i)
        K_mat(n,m) = dotc(self%exp_tot(a:j),R_vec(a:j))
        a = j + 1
    end do

end subroutine

subroutine create_Hk(self,k,Hk)
    !--------------------------------------------------------------------------
    !Creates the k-dependent Hamiltonian at point k by Fourrier transforming Hr
    !
    !Description:
    !   H_k(n,m) = sum_R exp(i*k*rho)H_R(n,m)
    !   rho = R + p_m - p_n,
    !   we consider that orbital n is in the first unit cell, R is then vector
    !   of the cell of orbital m
    !   p_n,p_m are coordinates of atoms on which orbitals n,m are defined in
    !   the first unit cell
    !
    !Args:
    !   k (real(3)): the k point
    !Returns:
    !   Hk: the lower diagonal part of Hk
    !--------------------------------------------------------------------------

    class(fplo_model) :: self
    real(dp), intent(in) :: k(3)
    complex(dp), allocatable, intent(out) :: Hk(:,:)

    integer :: i,n,m,j,a
    real(dp) :: r(3)


    allocate(Hk(self%n_wann,self%n_wann))
    Hk(:,:)=0_dp

    call self%fourrier_trans(k,self%Hr,Hk)
    
end subroutine

subroutine create_vk(self,k,vk,ind)
    !--------------------------------------------------------------------------
    !Creates the velocity operator at point k as a derivative of Hk
    !
    !Description:
    !   Very similar to how Hk is calculated
    !   v_k(n,m) = 1/hbar * \partial(Hk)\partial(k) = 
    !       = 1/hbar * sum_R exp(i*k*rho)H_R(n,m) * i * rho
    !   When ind is present only the ind component of the velocity operator
    !   is calculated otherwise all three components are calculated
    !
    !Args: (needs output from read_Hr)
    !   k: k point 
    !   ind (optional): when present, only this component of the v operator is 
    !       returned
    !
    !Returns:
    !   vk: the velocity operator at point k
    !--------------------------------------------------------------------------

    class(fplo_model) :: self
    real(dp), intent(in) :: k(3)
    complex(dp), allocatable, intent(out) :: vk(:,:,:)
    integer, intent(in), optional :: ind

    integer :: i,j,n,m,s,a
    real(dp) :: r(3)

    logical :: use_subroutine
    
    allocate(vk(self%n_wann,self%n_wann,3))
    vk(:,:,:) = 0_dp
   
    do j=1,3
        if (present(ind)) then
            if (ind /= j) cycle
        endif
        call self%fourrier_trans(k,self%H_rhos(j,:),vk(:,:,j))
        do m=2, self%n_wann
            do n=1, m-1
                vk(n,m,j) = dconjg(vk(m,n,j))
            end do
        end do
    end do
    vk = vk / hbar
end subroutine

subroutine create_smat(self,k,smat,atom)
    !--------------------------------------------------------------------------
    !Defines the matrix of the spin-operator
    !
    !Notes: 
    !   Assumes that first half othe basis functions are spin-up and the other half
    !   are spin-down.
    !   dimensionless, i.e. S=sigma. Divide by 2 to get spin in the units of hbar
    !
    !Args:
    !   n_wann: magnitude of the Hamiltonian
    !   atom(optional): number of atom on which the spin-operator is projected
    !       if atom is not present, no projection is done
    !   projs(optional): must be present if atom is present. defines how the 
    !       wannier orbitals are localized, which defines how the projections are
    !       done. This is outputed by read_projs
    !
    !Returns:
    !   smat: all three components of the Hamiltonian
    !--------------------------------------------------------------------------
    class(fplo_model) :: self
    real(dp), intent(in) :: k(3) 
    complex(dp), allocatable, intent(out) :: smat(:,:,:)
    integer, optional, intent(in) :: atom

    type(projs_type) :: projs

    integer :: i,orb,n_wann,atom_index
    real(dp) :: eul_angle(2)
    real(dp) :: new_z(3)
    logical :: rotate

    if ((.not. present(atom)) .or. (atom == 0)) then
        atom_index = 0
    else
        atom_index = atom
    end if

    if (atom_index /= 0) then
        if (.not. self%projs_defined) then
            stop 'projs must be defined to use projections'
        end if
        projs = self%projs
    end if

    n_wann = self%n_orb

    allocate(smat(n_wann,n_wann,3))
    smat(:,:,:) = 0._dp
    
    if ( atom_index == 0 ) then
    
        do i =1,n_wann/2
            !sigma_x
            smat(2*i-1, 2*i, 1) = 1._dp
            smat(2*i, 2*i-1, 1) = 1._dp
            !sigma_y
            smat(2*i-1, 2*i, 2) = (0._dp,-1._dp)
            smat(2*i, 2*i-1, 2) = (0._dp,1._dp)
            !sigma_z
            smat(2*i-1,2*i-1,3) = 1._dp 
            smat(2*i,2*i,3) = -1._dp 
        end do

    else
        do i =1,projs%n_orbs(atom)
            orb = projs%orbs(i,atom)
            if ( modulo(orb,2) == 0 ) then
                smat(orb-1,orb,1) = 1._dp 
                smat(orb-1,orb,2) = (0._dp,-1._dp)
                smat(orb,orb,3) = -1._dp
            else
                smat(orb+1,orb,1) = 1._dp
                smat(orb+1,orb,2) = (0._dp,1._dp) 
                smat(orb,orb,3) = 1._dp
            end if
        end do
    endif

    rotate = .true.
    if (rotate) then
        !rotate the spin matrices
        !in FPLO the quantization axis is always rotated to 001
        !The way this is done is according to the source code of FPLO:
        !First: rotate along z axis by eul_angle(1)
        !Second: rotate along new y axis by eul_angle(2)
        !The euler angles are defined below
        eul_angle(2)=acos(min(max(-1.0_dp,self%q_axis(3)),1.0_dp))
        eul_angle(1)=atan2(self%q_axis(2),self%q_axis(1))
        new_z = [ -sin(eul_angle(2)), 0.0_dp, cos(eul_angle(2)) ]
        if (atom_index == 0) then
            call rotate_spin_matrix(smat,[0.0_dp,1.0_dp,0.0_dp],-eul_angle(2))
            call rotate_spin_matrix(smat,[0.0_dp,0.0_dp,1.0_dp],-eul_angle(1))
        else
            call rotate_spin_matrix(smat,[0.0_dp,1.0_dp,0.0_dp],-eul_angle(2),self%projs,atom_index)
            call rotate_spin_matrix(smat,[0.0_dp,0.0_dp,1.0_dp],-eul_angle(1),self%projs,atom_index)
        endif
    endif

end subroutine

subroutine def_projs(self,projs)
    class(fplo_model) :: self
    type(projs_type), intent(in) :: projs

    self%projs = projs
    self%projs_defined = .true.

    if (.not. test_projs(projs)) then
        stop 'Projs need to always include both spin-up and spin-down orbitals: &
            the projections must be pairs of orb,orb+1.'
    endif
end subroutine

subroutine create_vk_dk(self,k,vk_dk)

    class(fplo_model) :: self
    real(dp), intent(in) :: k(3)
    complex(dp), allocatable, intent(out) :: vk_dk(:,:,:,:)

    stop 'not implemented'
end subroutine

subroutine get_latt_vecs_fplo(self,latt_vecs)
    class(fplo_model) :: self
    real(dp) :: latt_vecs(3,3)

    latt_vecs = self%latt_vecs
end subroutine

end module

