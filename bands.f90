module bands

use constants
use input
use eval_k, only: eigen
use k_integrate, only: find_recvecs,kindex2k
use generic_model

contains

subroutine calculate_bands(inp,bands_inp,model)
    type(input_type), intent(in) :: inp
    type(bands_input_type), intent(in) :: bands_inp
    class(gen_model), allocatable, intent(in) :: model

    integer :: n_wann
    complex(dp), allocatable :: Hk(:,:)
    real(dp) :: latt_vecs(3,3)
    real(dp), allocatable :: w(:)
    real(dp) :: rec_vecs(3,3)
    real(dp) :: rec_vol

    integer :: nk,i,j,n,u
    real(dp), dimension (3) :: k,kf,k1,k2
    integer :: ki(3)
    real(dp), allocatable :: klist(:,:),dists(:)
    real(dp) :: dist,dist_tot


    call model%get_latt_vecs(latt_vecs)
    call find_recvecs(latt_vecs,rec_vecs,rec_vol)
    n_wann = model%n_orb

    nk = bands_inp%n_kpts * (bands_inp%n_sym_kpts - 1)

    allocate(klist(3,nk))
    allocate(dists(nk))

    if (.not. bands_inp%plot_3d) then

        dist_tot = 0_dp
        do i=1,bands_inp%n_sym_kpts-1
            k1 = bands_inp%sym_kpts(1,i) * rec_vecs(:,1) + bands_inp%sym_kpts(2,i) * rec_vecs(:,2) + &
                bands_inp%sym_kpts(3,i) * rec_vecs(:,3)
            k2 = bands_inp%sym_kpts(1,i+1) * rec_vecs(:,1) + bands_inp%sym_kpts(2,i+1) * rec_vecs(:,2) + &
                bands_inp%sym_kpts(3,i+1) * rec_vecs(:,3)
            dist = norm2(k2-k1)
            do j = 1,bands_inp%n_kpts
                klist(:, (i-1)*bands_inp%n_kpts + j) = bands_inp%sym_kpts(:,i) + &
                    (j-1)/dble(bands_inp%n_kpts) * (bands_inp%sym_kpts(:,i+1) - bands_inp%sym_kpts(:,i)) 
                dists((i-1)*bands_inp%n_kpts + j) = dist_tot + dist * (j-1)/dble(bands_inp%n_kpts)
            end do
            dist_tot = dist_tot + dist
        end do

        open(newunit=u, file='bands.dat', recl=300)
        do i=1,nk
            kf = klist(:,i)
            k = kf(1) * rec_vecs(:,1) + kf(2) * rec_vecs(:,2) + kf(3) * rec_vecs(:,3)
            call model%create_Hk(k,Hk)
            if ( bands_inp%print_Ham ) then
                write(u,*) ''
                do n=1,n_wann
                    write(u,'(12(F8.5))') Hk(n,:)
                end do
            end if

            call eigen(Hk,w)
            do n=1,n_wann
                write(u,*) i, dists(i), k, w(n)
            end do
            write(u,*) ''
            write(u,*) ''
        end do

    else
        open(newunit=u, file='bands3d.dat', recl=300)
        nk = bands_inp%n_kpts
        do i=1,nk**2
            ki = kindex2k(i,[nk,nk,1])
            kf(1) = dble(ki(1))/dble(nk)
            kf(2) = dble(ki(2))/dble(nk)
            kf(3) = dble(ki(3))/dble(nk)
            
            
            k(:) = kf(1) * rec_vecs(:,1) + kf(2) * rec_vecs(:,2) + &
                kf(3) * rec_vecs(:,3)

            call model%create_Hk(k,Hk)
            call eigen(Hk,w)

            do n=1,n_wann
                write(u,*) kf(1),kf(2),w(n)
            end do
        end do
    end if

end subroutine
        
end module



