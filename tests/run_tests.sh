#!/bin/bash

#----------- PARAMETERS----------------
#location of the linres variable
linres=../../../linres.x

#location of the testdir
testdir=.

#folder in which the output is saved
outdir=./test_run

#mpicommand="mpirun -bind-to core"
mpicommand="mpirun -n 14"
#----------------------------------------

if [ $# -eq 0 ]; then
    calc=all
else
    calc=$1
fi

cd $testdir

mkdir -p $outdir/GaAs
mkdir -p $outdir/NiMnSb

cd files

if [ $calc = "all" ] || [ $calc = "GaAs" ]; then

    echo "running GaAs tests"
    cd GaAs

    $mpicommand $linres -i input_she.json > ../../$outdir/GaAs/she_out

    $mpicommand $linres -i input_cond.json > ../../$outdir/GaAs/cond_out

    $mpicommand $linres -i input_cond_f1.json > ../../$outdir/GaAs/cond_out_f1

    $mpicommand $linres -i input_cond_f0.json > ../../$outdir/GaAs/cond_out_f0

    cd ..
fi

if [ $calc = "all" ] || [ $calc = "NiMnSb" ]; then
    echo "running NiMnSb tests"
    cd NiMnSb

    $mpicommand $linres -i input_cond_w90.json > ../../$outdir/NiMnSb/cond_out_w90
    $mpicommand $linres -i input_cond_FPLO.json > ../../$outdir/NiMnSb/cond_out_FPLO

    $mpicommand $linres -i input_cisp_w90.json > ../../$outdir/NiMnSb/cisp_out_w90
    $mpicommand $linres -i input_cisp_FPLO.json > ../../$outdir/NiMnSb/cisp_out_FPLO

    $mpicommand $linres -i input_cond2_w90.json > ../../$outdir/NiMnSb/cond2_out_w90
    $mpicommand $linres -i input_cond2_FPLO.json > ../../$outdir/NiMnSb/cond2_out_FPLO

    $mpicommand $linres -i input_she2_FPLO.json > ../../$outdir/NiMnSb/she2_out_FPLO

    $linres -i input_k_resolved_FPLO_2d.json 
    mv k_resolved.out ../../$outdir/NiMnSb/k_resolved_2d_out.out
    mv k_resolved.spec ../../$outdir/NiMnSb/k_resolved_2d_out.spec
    $linres -i input_k_resolved_FPLO_3d.json
    mv k_resolved_3d.out ../../$outdir/NiMnSb/k_resolved_3d_out.out
    mv k_resolved_3d.spec ../../$outdir/NiMnSb/k_resolved_3d_out.spec

    
fi
