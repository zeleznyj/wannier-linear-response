 Version: 0.4.20    
 Mode: k_resolved
 Shape:           20          20           9           5
 k-dimension:            2
 Tensor rank:            2
 n_params:            5
 Params order: (n_param: n_formula,n_E_F,n_gamma,n_proj)
           1 :           1           1           1           1
           2 :           2           1           1           1
           3 :           3           1           1           1
           4 :           4           1           1           1
           5 :           5           1           1           1
