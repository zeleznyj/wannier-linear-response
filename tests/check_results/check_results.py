import sys
from os import listdir
from os.path import isfile, join
import itertools
import numpy as np
from numpy.linalg import norm
import argparse
import re

sys.path.append('../../python_scripts')
from analyze_res import read_result, read_k_resolved
from zero_values import Calc,is_zero

def is_ok(val,typ):
    if typ == 0:
        if val < 1e-11:
            return 'OK'
        else:
            return '!!! BAD !!!'
    elif typ == 1:
        if val < 1e3:
            return 'OK'
        else:
            return '!!! BAD !!!'

def clean_files(files):
    f_out = []
    include = True
    for f in files:
        if re.match('^\..+',f):
            include = False
        if not 'out' in f:
            include = False
        if '.spec' in f:
            include = False
        if include:
            f_out.append(f)
        include = True

    return f_out


parser = argparse.ArgumentParser()
parser.add_argument('outdir',help='The directory in which the output files are located.')
parser.add_argument('refdir',help='The directory in which the reference output files are located.')
parser.add_argument('-v','--verbosity',help='Verbosity level. Set to 0 or 1.',default=0,type=int)
args = vars(parser.parse_args())
outdir = args['outdir']
refdir = args['refdir']
verbosity = args['verbosity']

materials = ['GaAs','NiMnSb']

oks = []
for material in materials:
    print ''
    print 'Material: ', material
    ref_files = [f for f in listdir(refdir+material) if isfile(join(refdir+material, f))]
    ref_files = clean_files(ref_files)
    out_files = [f for f in listdir(outdir+material) if isfile(join(outdir+material, f))]
    out_files = clean_files(out_files)
    out_files_normal = []
    out_files_k_resolved = []
    for f in out_files:
        if 'k_resolved' in f:
            out_files_k_resolved.append(f)
        else:
            out_files_normal.append(f)
    for f in out_files_normal:
        if 'cond_' in f:
            calc_typ = 'cond'
        elif 'she_' in f:
            calc_typ = 'she'
        elif 'cisp_' in f:
            calc_typ = 'cisp'
        elif 'cond2' in f:
            calc_typ = 'cond2'
        elif 'she2' in f:
            calc_typ = 'she2'
        calc = Calc(material,calc_typ)
        if verbosity > 1:
            print ''
            print '----------------------------------------'
            print 'FILE: {0}'.format(f)

        if f not in ref_files:
            print 'WARNING: file {0} not present in the ref directory'.format(f) 
            continue
        try:
            res_ref = read_result(dire=refdir+material,fil=f)
        except Exception as e:
            print 'WARNING: problem with reading result from reference file {0}'.format(f)
            print(e)
            continue
        try:
            res = read_result(dire=outdir+material,fil=f)
        except Exception as e:
            print 'WARNING: problem with reading result from output file {0}'.format(f)
            print(e)
            continue

        dims = [res[dim].values for dim in res.dims]
        coords_iter = list(itertools.product(*dims))

        difs = []
        rel_difs = []
        difs_zeros = []
        rel_difs_zeros = []
        difs_nzs = []
        rel_difs_nzs = []

        for i,c in enumerate(coords_iter):
            res_c = res.loc[c]
            try:
                res_c_ref = res_ref.loc[c]
            except:
                if verbosity > 0:
                    print 'WARNING: coordinate {0} not present in ref file'.format(c)
                continue
            res_c_v = res_c.values
            res_c_v_ref = res_c_ref.values
            dif = res_c_v - res_c_v_ref
            rel_dif = dif / res_c_v_ref
            difs.append(dif)
            rel_difs.append(rel_dif)
            zero = is_zero(calc,c)
            if zero is not None:
                if zero:
                    difs_zeros.append(dif)
                    rel_difs_zeros.append(rel_dif)
                else:
                    difs_nzs.append(dif)
                    rel_difs_nzs.append(rel_dif)
            if verbosity > 1:
                print ''
                print res_c.coords
                print 'Zero value: {0}'.format(zero)
                print 'output value: {0}'.format(res_c_v)
                print 'reference value: {0}'.format(res_c_v)
                print 'absolute difference: {0}'.format(dif)
                print 'relative difference: {0}'.format(rel_dif)

        print ''
        if verbosity > 1:
            print 'REPORT:'
        print 'FILE: {0}'.format(f)
        abs_difs = np.abs(np.array(difs))
        abs_rel_difs = np.abs(np.array(rel_difs))
        abs_difs_zeros = np.abs(np.array(difs_zeros))
        abs_rel_difs_zeros = np.abs(np.array(rel_difs_zeros))
        abs_difs_nzs = np.abs(np.array(difs_nzs))
        abs_rel_difs_nzs = np.abs(np.array(rel_difs_nzs))
        if verbosity > 0:
            vars_names = ['difference','relative difference','difference zero values','relative difference zero values',
                    'difference nonzero values','relative difference nonzero values']
            varss = [abs_difs,abs_rel_difs,abs_difs_zeros,abs_rel_difs_zeros,abs_difs_nzs,abs_rel_difs_nzs]
            print 'average, standard deviation, maximum:'
            for i,var in enumerate(varss):
                print vars_names[i]
                if len(var) > 0:
                    print ' {0}, {1}, {2}'.format(np.mean(var),
                            np.std(var),
                            np.max(var))
                else:
                    print 'empty'
        if len(abs_rel_difs_zeros) > 0: 
            ok = is_ok(np.max(abs_rel_difs_zeros),1)
            oks.append(ok)
            print 'maximum relative difference for zero values: {0}'.format(ok)
        else:
            print 'maximum relative difference for zero values: EMPTY'
        if len(abs_rel_difs_nzs) > 0:
            ok = is_ok(np.max(abs_rel_difs_nzs),0)
            oks.append(ok)
            print 'maximum relative difference for nonzero values: {0}'.format(ok)
        else:
            print 'maximum relative difference for nonzero values: EMPTY'

    for f in out_files_k_resolved:
        try:
            X_ref = read_k_resolved(refdir+material+'/'+f)
        except Exception as e:
            print 'WARNING: problem with reading result from reference file {0}'.format(f)
            print(e)
            continue
        try:
            X_out = read_k_resolved(outdir+material+'/'+f)
        except Exception as e:
            print 'WARNING: problem with reading result from output file {0}'.format(f)
            print(e)
            continue
        rel_dif = (norm(X_ref) - norm(X_out)) / norm(X_ref)
        ok = is_ok(rel_dif,0)
        oks.append(ok)
        print('')
        print('FILE: {}'.format(f))
        print('relative difference: {0}'.format(ok))
        if verbosity > 0:
            print('Reference norm: ', norm(X_ref))
            print('Output norm: ', norm(X_out))
            print('Relative difference: ',rel_dif)


if set(oks) == set(['OK']):
    print 'ALL TESTS OK!'
else:
    print '!!! THERE WERE SOME ERRORS !!!'

    
    
