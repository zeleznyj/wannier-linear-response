class Calc:
    def __init__(self,material,typ):
        self.material = material
        self.typ = typ

def is_zero(calc,coord):
    if calc.material == 'GaAs':
        if calc.typ == 'cond':
            if coord[-1] == coord[-2]:
                return False
            else:
                return True
        elif calc.typ == 'she':
            if len(set([coord[-1],coord[-2],coord[-3]])) == 3:
                return False
            else:
                return True
        elif calc.typ == 'cisp':
            return True
    elif calc.material == 'NiMnSb':
        if calc.typ == 'cond':
            if coord[0] in [1,2,3]:
                if coord[-1] == coord[-2]:
                    return False
                else:
                    return True
            elif coord[0] in [11,12]:
                if coord[-1] < 2 and coord[-2] < 2 and (coord[-1] != coord[-2]):
                    return False
                else:
                    return True
        if calc.typ == 'cisp':
            if coord[0] in [1,2,3]:
                if (coord[-1] == coord[-2]) and coord[-1] < 2 and coord[-2] < 2:
                    return False
                else:
                    return True
            elif coord[0] in [11,12]:
                if coord[-1] < 2 and coord[-2] < 2 and (coord[-1] != coord[-2]):
                    return False
                else:
                    return True

        if calc.typ == 'cond2':
            a = [coord[-1],coord[-2],coord[-3]]
            a.sort()
            if a == [0,0,2] or a == [1,1,2]:
                return False
            else:
                return True

        if calc.typ == 'she2':
            nzs = [(0, 0, 0, 0),
                    (1, 1, 0, 0),
                    (2, 2, 0, 0),
                    (1, 0, 1, 0),
                    (0, 1, 1, 0),
                    (2, 0, 2, 0),
                    (0, 2, 2, 0),
                    (1, 0, 0, 1),
                    (0, 1, 0, 1),
                    (0, 0, 1, 1),
                    (1, 1, 1, 1),
                    (2, 2, 1, 1),
                    (2, 1, 2, 1),
                    (1, 2, 2, 1),
                    (2, 0, 0, 2),
                    (0, 2, 0, 2),
                    (2, 1, 1, 2),
                    (1, 2, 1, 2),
                    (0, 0, 2, 2),
                    (1, 1, 2, 2)]

            a = (coord[-4],coord[-3],coord[-2],coord[-1])
            if a in nzs:
                return False
            else:
                return True
            
    else:
        return None


