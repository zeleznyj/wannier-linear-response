program linres
!--------------------------------------------------------------------------
!This calculates various linear response quantities using either a wannier
!tb Hamiltonian or an empirical tight-binding Hamiltonian
!--------------------------------------------------------------------------

use mpi
use constants
use input, only: read_input
use response, only: calculate_response
use k_resolved, only: calculate_k_resolved_para
use bands, only: calculate_bands
use wann_model_mod
use tb_model_mod
use Rashba_model_mod
use fplo_model_mod
implicit none

type(input_type) :: inp
type(bands_input_type) :: bands_inp
class(gen_model), allocatable :: model
integer :: ierr,rank,ntasks

call mpi_init(ierr)
call mpi_comm_rank(mpi_comm_world,rank,ierr)
call mpi_comm_size(mpi_comm_world,ntasks,ierr)

if (rank == 0) then
    write(*,*) 'Linres ', version
endif

if (rank == 0) then 
    write(*,*) 'Parsed input'
    write(*,*) '*****************************'
    write(*,*) 'Version: ', version
    call write_input()
    write(*,*) '*****************************'
    write(*,*) ''
endif

call read_input(inp)

!we initialize the model chosen in the input
if (inp%model == 'wann') then
    allocate(wann_model::model)
elseif (inp%model == 'tb') then
    allocate(tb_model::model)
else if (inp%model == 'Rashba') then
    allocate(Rashba_model::model)
elseif (inp%model == 'fplo') then
    allocate(fplo_model::model)
else
    stop 'wrong model type in input'
endif
call model%init()

if (inp%mode == 'response') then
    call calculate_response(inp,model)
elseif (inp%mode == 'bands') then
    call read_bands_input(bands_inp)
    if (bands_inp%mode == 'k_resolved') then
        call calculate_k_resolved_para(inp,model)
    else
        call calculate_bands(inp,bands_inp,model)
    endif
elseif (inp%mode == 'k_resolved') then
    call calculate_k_resolved_para(inp,model)
else
    stop 'ERROR: Unknown mode'
endif

if (rank == 0) then
    write(*,*) 'Linres finished'
endif

call mpi_finalize(ierr)
end program
