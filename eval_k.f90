module eval_k
use lapack95
use blas95
use mpi
use constants
use input
use generic_model
use funcs, only: Fermi_Dirac_d1,Fermi_Dirac_d2
implicit none
private
public :: eigen, mat_ele,response_k

contains

subroutine eigen(A,w)
    !--------------------------------------------------------------------------
    !Finds eigenvalues and eigenvectors of a Hermitian matrix A.
    !
    !Note:!!! Tha matrix A is overwritten!!!
    !
    !Args:
    !   A(complex(:,:)): the matrix - assumes the lower diagonal part
    !
    !Returns:
    !   A: eigenvectors - stored as the columns of A
    !   w: eigenvalues
    !--------------------------------------------------------------------------

    complex(dp),intent(inout) :: A(:,:)
    real(dp), intent(out), allocatable :: w(:)

    integer :: n
    complex(dp), allocatable :: z(:,:)

    n = size(A,1)
    allocate(w(n))
    allocate(z(n,n))

    !call heevd(A,w,'V','L')
    call heevr(A,w,z=z,uplo='L')
    A=z

end subroutine

subroutine mat_ele(mat,evecs,ele)
    !--------------------------------------------------------------------------
    !Calculates matrix elements of operator
    !
    !Args:
    !   mat: matrix of the operator
    !Returns:
    !   evecs: Matrix, which contains the eigenvectors in a row form, i.e., the
    !       rows of evecs are the eigenvectors.
    !
    !Note: Requires BLAS.
    !--------------------------------------------------------------------------

    complex(dp), intent(in) :: mat(:,:)
    complex(dp), intent(in) :: evecs(:,:)
    complex(dp), intent(out) :: ele(:,:)

    integer :: n_wann
    complex(dp), allocatable ::mat2(:,:)
    
    n_wann = size(mat,1)
    allocate(mat2(n_wann,n_wann))

    call zgemm('N','N',n_wann,n_wann,n_wann,(1.D0,0.D0),mat(:,:),n_wann,evecs(:,:),n_wann,(0.D0,0.D0),mat2(:,:),n_wann)
    call zgemm('C','N',n_wann,n_wann,n_wann,(1.D0,0.D0),evecs(:,:),n_wann,mat2(:,:),n_wann,(0.D0,0.D0),ele(:,:),n_wann)

end subroutine

subroutine response_k(k,model,inp,X,times)
    !--------------------------------------------------------------------------
    !Evaluates response at one k-point for model and the input parameters in inp
    !
    !Args:
    !   k: the k points
    !   model (class(gen_model)): can be any class that extends the gen_model
    !       class. We only need procedures for creating Hk, vk and smat from
    !       this class.
    !   inp (type(input_type)): the input type
    !       contains all the necessary information about parameters of the
    !       calculation, in particular the gamma values, Fermi level, projections
    !       and the actual quantity we are calculating
    !       - some parameters stored in inp can contain a range of values, then
    !         the response is calculated for all of them
    ! 
    !Returns:
    !   X: containts the response tensor
    !      format: 
    !       - the first index contains the response for one set of parameters
    !       - it is reshaped to 1d form using the reshape function, to get
    !         the correct form, just reshape it back
    !       - For example, for SHE linear response: X_r = reshape(X(:,l),[3,3,3])
    !       - the second index labels the paramters, each value correpons to one set
    !         of parameters as they are stored in inp%params    
    !--------------------------------------------------------------------------


    real(dp), intent(in) :: k(3)
    class(gen_model) :: model
    type(input_type), intent(in) :: inp
    real(dp), intent(out), allocatable :: X(:,:)
    real(dp), optional, intent(inout) :: times(10,2)

    complex(dp), allocatable :: Hk(:,:)
    complex(dp), allocatable :: vk(:,:,:),vk_dk(:,:,:,:)
    complex(dp), allocatable :: smat(:,:,:)
    real(dp), allocatable :: w(:)
    complex(dp), allocatable :: vk_ele(:,:,:),vk_dk_ele(:,:,:,:)
    complex(dp), allocatable :: op1_ele(:,:,:)
    complex(dp), allocatable :: svmat(:,:)
    integer :: n_wann
    integer :: n_gam,nEf
    integer :: n_projs
    integer :: n_omegas
    integer :: n_op1,n_op12
    integer :: i,j,l,m,n,q,l2,itr_omg
    real(dp) :: t
    real(dp), allocatable :: Xlinres(:,:)
    real(dp), allocatable :: Xboltz2(:,:,:)
    integer :: var(5),formula,gam_l,omg_l
    real(dp) :: gam,Ef,omg
    integer :: atom
    logical, allocatable :: calculated(:)

    !timing6<<<<<<<<<<<<<<<<<<<<
    if (present(times)) then
        t = mpi_wtime() 
        times(6,1) = times(6,1) + t
    endif
    !timing6>>>>>>>>>>>>>>>>>>>>

    n_wann = model%n_orb
    allocate(svmat(n_wann,n_wann))

    !timing7<<<<<<<<<<<<<<<<<<<<
    if (present(times)) then
        t = mpi_wtime() 
        times(7,1) = times(7,1) + t
    endif
    !timing7>>>>>>>>>>>>>>>>>>>>
    
    !creates the k-dependent Hamiltonian at point k
    call model%create_Hk(k,Hk)

    !timing7<<<<<<<<<<<<<<<<<<<<
    if (present(times)) then
        t = mpi_wtime() 
        times(7,2) = times(7,2) + t
    endif
    !timing7>>>>>>>>>>>>>>>>>>>>

    !timing8<<<<<<<<<<<<<<<<<<<<
    if (present(times)) then
        t = mpi_wtime() 
        times(8,1) = times(8,1) + t
    endif
    !timing8>>>>>>>>>>>>>>>>>>>>

    !finds eigenvalues and eigenfunctions of Hk
    call eigen(Hk,w)

    !timing8<<<<<<<<<<<<<<<<<<<<
    if (present(times)) then
        t = mpi_wtime() 
        times(8,2) = times(8,2) + t
    endif
    !timing8>>>>>>>>>>>>>>>>>>>>

    
    !timing1<<<<<<<<<<<<<<<<<<<<
    if (present(times)) then
        t = mpi_wtime() 
        times(1,1) = times(1,1) + t
    endif
    !timing1>>>>>>>>>>>>>>>>>>>>

    !constructs the velocity operator
    call model%create_vk(k,vk)

    !timing1<<<<<<<<<<<<<<<<<<<<
    if (present(times)) then
        t = mpi_wtime() 
        times(1,2) = times(1,2) + t
    endif
    !timing1>>>>>>>>>>>>>>>>>>>>

    allocate(vk_ele(n_wann,n_wann,3))

    !n_op1 is the number of first operators in the linear response formula
    n_op1 = inp%n_op1
    if (inp%linres == 'she') then
        n_op12 = 9
    elseif (any(inp%linres == ['cond','cisp'])) then
        n_op12 = 3
    endif

    allocate(op1_ele(n_wann,n_wann,n_op1))
    allocate(X(inp%n_X,inp%n_params))
    allocate(Xlinres(inp%n_op1,3))
    allocate(Xboltz2(inp%n_op1,3,3))
    allocate(calculated(inp%n_params))
    calculated = .false.
    X = 0_dp

    !timing2<<<<<<<<<<<<<<<<<<<<
    if (present(times)) then
        t = mpi_wtime() 
        times(2,1) = times(2,1) + t
    endif
    !timing2>>>>>>>>>>>>>>>>>>>>

    !matrix elements of the velocity operator, these are always needed
    do i=1,3
        call mat_ele(vk(:,:,i),Hk,vk_ele(:,:,i))
    end do

    !timing2<<<<<<<<<<<<<<<<<<<<
    if (present(times)) then
        t = mpi_wtime() 
        times(2,2) = times(2,2) + t
    endif
    !timing2>>>>>>>>>>>>>>>>>>>>

    !for cisp we construct the spin-operator for each projection
    if ( inp%linres == 'cisp' ) then

        !a loop over the atom projections
        do n=1,inp%params%n_projs

            !timing3<<<<<<<<<<<<<<<<<<<<
            if (present(times)) then
                t = mpi_wtime() 
                times(3,1) = times(3,1) + t
            endif
            !timing3>>>>>>>>>>>>>>>>>>>>

            call model%create_smat(k,smat,inp%params%projs(n))

            !timing3<<<<<<<<<<<<<<<<<<<<
            if (present(times)) then
                t = mpi_wtime() 
                times(3,2) = times(3,2) + t
            endif
            !timing3>>>>>>>>>>>>>>>>>>>>

            !timing4<<<<<<<<<<<<<<<<<<<<
            if (present(times)) then
                t = mpi_wtime() 
                times(4,1) = times(4,1) + t
            endif
            !timing4>>>>>>>>>>>>>>>>>>>>

            !loops over the components of the cisp tensor
            do i =1,3
                call mat_ele(smat(:,:,i),Hk,op1_ele(:,:,(n-1)*3+i))
            end do

            !timing4<<<<<<<<<<<<<<<<<<<<
            if (present(times)) then
                t = mpi_wtime() 
                times(4,2) = times(4,2) + t
            endif
            !timing4>>>>>>>>>>>>>>>>>>>>

        end do

    else if (inp%linres == 'cond') then
        !for conductivit the first operator is just the velocity operator
        op1_ele = vk_ele
    else if ( inp%linres == 'she' ) then
        !for SHE the first operator is s*v

        !timing3<<<<<<<<<<<<<<<<<<<<
        if (present(times)) then
            t = mpi_wtime() 
            times(3,1) = times(3,1) + t
        endif
        !timing3>>>>>>>>>>>>>>>>>>>>

        call model%create_smat(k,smat)

        !timing3<<<<<<<<<<<<<<<<<<<<
        if (present(times)) then
            t = mpi_wtime() 
            times(3,2) = times(3,2) + t
        endif
        !timing3>>>>>>>>>>>>>>>>>>>>

        do i=1,3
            do j=1,3
                !calculates 1/2 of anticommutator of s and v
                call gemm(vk(:,:,j),smat(:,:,i),svmat,alpha=dcmplx(0.5_dp),beta=dcmplx(0))
                call gemm(smat(:,:,i),vk(:,:,j),svmat,alpha=dcmplx(0.5_dp),beta=dcmplx(1))
                call mat_ele(svmat,Hk,op1_ele(:,:,3*(j-1)+i))
            end do
        end do
    endif

    if ( any(inp%params%formulas == 22) .or. any(inp%params%formulas == 23)) then
        call model%create_vk_dk(k,vk_dk)
        allocate(vk_dk_ele(n_wann,n_wann,3,3))
        do i=1,3
            do j=1,3
                call mat_ele(vk_dk(:,:,i,j),Hk,vk_dk_ele(:,:,i,j))
            end do
        end do
    endif

    !timing5<<<<<<<<<<<<<<<<<<<<
    if (present(times)) then
        t = mpi_wtime() 
        times(5,1) = times(5,1) + t
    endif
    !timing5>>>>>>>>>>>>>>>>>>>>

    !a loop over all the formulas
    do l=1,inp%n_params
        var = inp%params%get(l)
        formula = inp%params%formulas(var(1))
        !formula = inp%params%formula(l)
        Ef = inp%params%Efs(var(2))
        !Ef = inp%params%Ef(l)
        gam_l = var(3)
        gam = inp%params%gammas(gam_l)
        !gam = inp%params%gamma(l)
        atom = inp%params%proj(l)
        omg_l = var(5)
        omg = inp%params%omegas(omg_l)

        !calculate only if this hasn't been previously calculated
        if (calculated(l)) cycle
        
        !for this formula, the gamma dependence is 1/Gamma so we don't have to calculate for
        !all gammas
        if ( any(formula == [1,3]) ) then
            if (formula == 1) then
                Xlinres(:,:) = linres_eval(gam,Ef,w,op1_ele,vk_ele,formula,inp%zero_cutoff,eps=inp%eps)
            elseif (formula == 3) then
                Xlinres(:,:) = linres_eval(gam,Ef,w,op1_ele,vk_ele,formula,inp%zero_cutoff,T=inp%T)
            endif
                !X(:,l) = reshape(Xlinres, [inp%n_X])
            do i=1,inp%params%n_gammas
                !All the projections are calculated together so these have to be properly distributed
                do j=1,inp%params%n_projs
                    do itr_omg = 1, inp%params%n_omegas
                        l2 = inp%params%get_r([var(1),var(2),i,j,itr_omg])
                        X(:,l2) = reshape(Xlinres((j-1)*3+1:(j-1)*3+n_op12,:),[inp%n_X]) * gam / inp%params%gammas(i)
                        calculated(l2) = .true.
                    end do
                end do
            end do

        !here the gamma dependence is 1/Gamma^2 and the gamma factor is not included in the 
        !eval function
        else if ( any( formula == [21,22,23,24] ) ) then
            if (formula == 21) then
                Xboltz2 = boltzmann2_eval(Ef,w,op1_ele,vk_ele,inp%T,formula)
            endif
            if ( any( formula == [22,23] ) ) then
                Xboltz2 = boltzmann2_eval(Ef,w,op1_ele,vk_ele,inp%T,formula,vk_dk_ele)
            endif

            do i=1,inp%params%n_gammas
                !All the projections are calculated together so these have to be properly distributed
                do j=1,inp%params%n_projs
                    do itr_omg = 1, inp%params%n_omegas
                        l2 = inp%params%get_r([var(1),var(2),i,j,itr_omg])
                        X(:,l2) = reshape(Xboltz2((j-1)*3+1:(j-1)*3+n_op12,:,:),[inp%n_X]) / inp%params%gammas(i)**2
                        calculated(l2) = .true.
                    end do
                end do
            end do
        !for this formula, we have to consider frequency dependence
        else if ( any( formula == [101,102]) ) then
            Xlinres(:,:) = linres_eval(gam,Ef,w,op1_ele,vk_ele,formula,inp%zero_cutoff,inp%eps,omega=omg)
            do j=1,inp%params%n_projs
                l2 = inp%params%get_r([var(1),var(2),var(3),j,var(5)])
                X(:,l2) = reshape(Xlinres((j-1)*3+1:(j-1)*3+n_op12,:),[inp%n_X])
                calculated(l2) = .true.
            end do
        !other formulas need to be calculated separately for each gamma
        else
            Xlinres(:,:) = linres_eval(gam,Ef,w,op1_ele,vk_ele,formula,inp%zero_cutoff,inp%eps)
            do j=1,inp%params%n_projs
                do itr_omg = 1, inp%params%n_omegas
                    l2 = inp%params%get_r([var(1),var(2),var(3),j,itr_omg])
                    X(:,l2) = reshape(Xlinres((j-1)*3+1:(j-1)*3+n_op12,:),[inp%n_X])
                    calculated(l2) = .true.
                end do
            end do
        endif
        
    end do

    !timing5<<<<<<<<<<<<<<<<<<<<
    if (present(times)) then
        t = mpi_wtime() 
        times(5,2) = times(5,2) + t
    endif
    !timing5>>>>>>>>>>>>>>>>>>>>

    !timing6<<<<<<<<<<<<<<<<<<<<
    if (present(times)) then
        t = mpi_wtime() 
        times(6,2) = times(6,2) + t
    endif
    !timing6>>>>>>>>>>>>>>>>>>>>

end subroutine

function linres_eval(gam,Ef,w,mat1,mat2,formula,zero_cutoff,eps,T,omega) result(x)
    !--------------------------------------------------------------------------
    !Evaluates the interband linear response formula at point k.
    !
    !At this point this evaluates only linear response to electric field
    !Then units are: C*Ang/eV*[mat1]
    !--------------------------------------------------------------------------

    real(dp), intent(in) :: gam
    real(dp), intent(in) :: Ef
    real(dp), intent(in) :: w(:)
    complex(dp), intent(in) :: mat1(:,:,:)
    complex(dp), intent(in) :: mat2(:,:,:)
    integer, intent(in) :: formula
    real(dp), intent(in) :: zero_cutoff
    real(dp), intent(in), optional :: eps
    real(dp), intent(in), optional :: T
    real(dp), intent(in), optional :: omega

    real(dp), allocatable :: x(:,:)

    integer :: i,j,n,m,n_wann
    integer :: nmat1,nmat2
    real(dp) :: f,f2,f3
    complex(dp), allocatable :: vec1(:),vec2(:),vec3(:),vec4(:)
    complex(dp), allocatable :: mat3(:,:)
    real(dp) :: machine_eps

    n_wann = size(w)
    allocate(vec1(n_wann**2),vec2(n_wann**2),vec3(n_wann**2),vec4(n_wann**2))
    allocate(mat3(n_wann,n_wann))

    nmat1 = size(mat1,3)
    nmat2 = size(mat2,3)

    if (.not. allocated(x)) then
        allocate(x(nmat1,nmat2))
    endif
    x = 0._dp

    if (formula == 1) then
        if (.not. present(eps)) stop 'you must provide epsilon'
        do n=1,n_wann
            do i=1,nmat1
                do j=1,nmat2
                    !Representing the delta function, using the Poisson kernel
                    x(i,j) = x(i,j) + mat1(n,n,i) * mat2(n,n,j) * (-eps / ( eps**2 + (w(n)-Ef)**2 ) / pi)
                end do
            end do
        end do
        x = x * (eele) * hbar / (2*gam)

    elseif (formula == 2) then
        do n=1,n_wann
            do m=1,n_wann
                f = gam**2 / ( ((Ef-w(n))**2 + gam**2) * ((Ef-w(m))**2 + gam**2) )
                do i=1,nmat1
                    do j=1,nmat2
                        x(i,j) = x(i,j) + dble(mat1(n,m,i)*mat2(m,n,j)) * (-f)
                    end do
                end do
            end do
        end do
        x = x * (eele)*hbar/pi

    elseif (formula == 3) then
        if (.not. present(T)) stop 'you must provide T'
        do n=1,n_wann
            f = Fermi_Dirac_d1(w(n),Ef,T)
            do i=1,nmat1
                do j=1,nmat2
                    x(i,j) = x(i,j) + mat1(n,n,i) * mat2(n,n,j) * f
                end do
            end do
        end do
        x = x * (eele) * hbar / (2*gam)

    elseif (formula == 11) then
        do n=1,n_wann
            do m=1,n_wann
                if ( n /= m) then
                    if ((w(n) < Ef .and. w(m) > Ef)) then
                        f =  (-( w(n) - w(m) ) ** 2.D0 + gam ** 2.D0 ) / ( ( ( w(n) - w(m) ) ** 2.D0 + gam ** 2.D0 ) ** 2.D0 )
                        do i=1,nmat1
                            do j=1,nmat2
                                x(i,j) = x(i,j) + dimag(mat1(n,m,i)*mat2(m,n,j)) * f 
                            end do
                        end do
                    end if
                endif
            end do
        end do
        x = x * 2
        x = x * eele * hbar

    elseif (formula == 12) then
        machine_eps = epsilon(machine_eps)
        do n=1,n_wann
            do m=1,n_wann
                if ( n /= m) then
                    !if w(n) == w(m) then f+f2+f3 = 0 so we can skip this
                    !otherwise this would result in NaNs
                    if ( abs(w(n)-w(m)) / abs(w(n)+w(m)) > zero_cutoff * machine_eps ) then
                        f =  gam * (w(m)-w(n)) / ( ((Ef-w(n))**2 + gam**2) * ((Ef-w(m))**2 + gam**2) )
                        f2 = 2 * gam / ( (w(n)-w(m)) * ((Ef-w(m))**2 + gam**2) )
                        f3 = 2 / (w(n)-w(m))**2 * dimag(log((w(m)-Ef-ii*gam) / (w(n)-Ef-ii*gam)))
                        do i=1,nmat1
                            do j=1,nmat2
                                x(i,j) = x(i,j) - dimag(mat1(n,m,i)*mat2(m,n,j)) * (f + f2 + f3) 
                            end do
                        end do 
                    endif
                endif
            end do
        end do
        x = x * eele * hbar / (2*pi)
    
    ! Real part without intra-band term (so, Drude peak is disregarded)
    elseif (formula == 101) then
        machine_eps = epsilon(machine_eps)
        if (.not. present(omega)) then
            stop 'frequency omega is undefined'
        endif
        if (omega < zero_cutoff * machine_eps) then
            stop 'frequency omega is too small'
        endif
        do n=1,n_wann
            if ( w(n) > Ef ) then
            ! n is an unoccupied state
                do m=1,n_wann
                    if ( w(m) <= Ef ) then
                    ! m is an occupied state
                        f =  gam / (omega*( (omega -w(n)+w(m))**2 + gam**2 ))
                        do i=1,nmat1
                            do j=1,nmat2
                                x(i,j) = x(i,j) + dreal(mat1(n,m,i)*mat2(m,n,j)) * f 
                            end do
                        end do 
                    endif
                enddo
            endif
        end do
        x = x * (-eele) * hbar
    ! Imaginary part
    elseif (formula == 102) then
        machine_eps = epsilon(machine_eps)
        if (.not. present(omega)) then
            stop 'frequency omega is undefined'
        endif
        if (omega < zero_cutoff * machine_eps) then
            stop 'frequency omega is too small'
        endif
        do n=1,n_wann
            if ( w(n) > Ef ) then
            ! n is an unoccupied state
                do m=1,n_wann
                    if ( w(m) <= Ef ) then
                    ! m is an occupied state
                        f =  gam / (omega*( (omega -w(n)+w(m))**2 + gam**2 ))
                        do i=1,nmat1
                            do j=1,nmat2
                                x(i,j) = x(i,j) + dimag(mat1(n,m,i)*mat2(m,n,j)) * f 
                            end do
                        end do 
                    endif
                enddo
            endif
        end do
        x = x * (-eele) * hbar
    elseif (formula /= 0) then
        stop 'wrong intraband switch'
    endif


end function

recursive function boltzmann2_eval(Ef,w,mat1,vmat,T,formula,vdkmat) result(x)
    !--------------------------------------------------------------------------
    !Evaluates second-order Boltzmann formula
    !
    !THe output has units of C^2*Ang^2/(eV)^2*[mat1]
    !--------------------------------------------------------------------------

    real(dp), intent(in) :: Ef
    real(dp), intent(in) :: w(:)
    complex(dp), intent(in) :: mat1(:,:,:)
    complex(dp), intent(in) :: vmat(:,:,:)
    real(dp), intent(in) :: T
    integer, intent(in) :: formula
    complex(dp), intent(in), optional :: vdkmat(:,:,:,:)

    real(dp), allocatable :: x(:,:,:)

    integer :: i,j,k,n,m,n_wann
    real(dp) :: f,kT
    real(dp) :: machine_eps
    integer :: nmat1

    n_wann = size(w)
    nmat1 = size(mat1,3)
    allocate(x(nmat1,3,3))
    x = 0_dp

    if (formula == 21) then

        do n=1,n_wann
            f = Fermi_Dirac_d2(w(n),Ef,T)
            do i=1,nmat1
                do j=1,3
                    do k=1,3
                        x(i,j,k) = x(i,j,k) + mat1(n,n,i) * vmat(n,n,j) * vmat(n,n,k) * f
                    end do
                end do
            end do
        end do

        x = x/2.0_dp

    elseif (formula == 22) then
        machine_eps = epsilon(machine_eps)
        if (.not. present(vdkmat) ) then
            stop 'vdkmat must be present for formula 22'
        end if
        do n=1,n_wann
            f = Fermi_Dirac_d1(w(n),Ef,T)
            do i=1,nmat1
                do j=1,3
                    do k=1,3
                        x(i,j,k) = x(i,j,k) + mat1(n,n,i) * vdkmat(n,n,j,k) * f 
                        do m=1,n_wann
                            if  (n /= m) then
                               !we have to get rid of degenerate states, otherwise this would diverge
                                if ( abs(w(n)-w(m)) / abs(w(n)+w(m)) > 1000 * machine_eps ) then
                                    x(i,j,k) = x(i,j,k) + mat1(n,n,i) * 2 * dble(vmat(n,m,j)*vmat(m,n,k)) / (w(n)-w(m)) * f
                                end if 
                            end if
                        end do
                    end do
                end do
            end do
        end do
    elseif (formula == 23) then
        x = boltzmann2_eval(Ef,w,mat1,vmat,T,21) + boltzmann2_eval(Ef,w,mat1,vmat,T,22,vdkmat) 
    elseif (formula /= 0) then
        stop 'wrong formula switch'
    endif

    if (any(formula == [21,22])) then
        !prefactors
        x = eele**2 * ( hbar / 2 )**2 * x 
        !units (so that the output is in 1/(Omega*V))
        !x = eele**3 * ( hbar / 2 )**2 * x 
        !x = x / eV**2
    endif

end function

function boltzmann2_surface_eval(Ef,w,vmat,T,formula,normal) result(x)
    !--------------------------------------------------------------------------
    !Evaluates second-order Boltzmann formula at boundaries of Brillouin zone
    !Not used anywhere right now.
    !--------------------------------------------------------------------------

    real(dp), intent(in) :: Ef
    real(dp), intent(in) :: w(:)
    complex(dp), intent(in) :: vmat(:,:,:)
    real(dp), intent(in) :: T
    integer, intent(in) :: formula
    real(dp), intent(in) :: normal(3)

    real(dp) :: x(3,3,3)
    
    real(dp) :: f,kT
    integer :: n,n_wann,i,j,k

    n_wann = size(w)

    x = 0_dp

    kT = kB * T
    if (formula == 24) then
        do n=1,n_wann
            if ( abs( (w(n) - Ef) / kT ) > 50 ) then
                f = 0._dp
            else
                f = -1 / kT / (1 + exp( (w(n) - Ef) / kT ))**(2) * exp( (w(n) - Ef) / kT )
            endif
            do i=1,3
                do j=1,3
                    do k=1,3
                        x(i,j,k) = x(i,j,k) + vmat(n,n,i) * vmat(n,n,k) * normal(j) * f
                    end do
                end do
            end do
        end do
    else
        stop 'wrong formula switch'
    endif

    !prefactors
    x = eele**3 * ( hbar / 2 )**2 * x 
    !units (so that the output is in 1/(Omega*V))
    x = x / eV**2

end function

function inter_eval2(gam,Ef,w,mat1,mat2) result(x)
    !--------------------------------------------------------------------------
    !Same as inter_eval, except it evaluates the formula:
    !   2e\hbar \sum_{n\neq m} Im(mat1_nm * mat2_nm) (gam**2-(E_kn-E_km)**2)
    !       / ((E_kn-E_km)**2+gam**2) * f_kn
    !--------------------------------------------------------------------------

    real(dp), intent(in) :: gam
    real(dp), intent(in) :: Ef
    real(dp), intent(in) :: w(:)
    complex(dp), intent(in) :: mat1(:,:)
    complex(dp), intent(in) :: mat2(:,:)

    real(dp) :: x

    integer :: n_wann
    integer :: n,m
    real(dp) :: f

    n_wann = size(w)

    x = 0._dp
    do n=1,n_wann
        do m=1,n_wann
            if ( n /= m) then

                if (w(n) < Ef) then
                    f =  (( w(n) - w(m) ) ** 2.D0 - gam ** 2.D0 ) / ( ( w(n) - w(m) ) ** 2.D0 + gam ** 2.D0 ) ** 2.D0
                    x = x + aimag(mat1(n,m)*mat2(m,n)) * f 
                end if

            endif
        end do
    end do

    x = x * 2
    x = x * eele * hbar

end function



end module

