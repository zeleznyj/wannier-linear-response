module funcs

use constants
use input, only: input_type,projs_type
use blas95

implicit none

contains

function test_projs(projs) result(ok)
    !--------------------------------------------------------------------------
    !Tests if projs contain both spin-up and spin-down orbitals, which is
    !necessary in the FPLO mode. In the wannier mode, this seems to not be necessary
    !at this point, though rotations would probablly not work.
    !
    !Assumes the s_type = 1 type of spin matrix.
    !--------------------------------------------------------------------------

    type(projs_type), intent(in) :: projs
    logical :: ok

    integer :: a,i,orb

    ok = .true.
    do a =1,projs%n_atoms
        do i=1,projs%n_orbs(a)
            orb = projs%orbs(i,a)
            if (modulo(orb,2) == 1) then
                if (i == projs%n_orbs(a)) then
                    ok = .false.
                else
                    if (projs%orbs(i+1,a) /= orb+1) then
                        ok = .false.
                    endif
                endif
            endif
        enddo
    enddo
    
end function

subroutine rotate_spin_matrix(smat,s_rot_axis,s_rot_angle,projs,atom)
    !--------------------------------------------------------------------------
    !Rotates the spin matrices.
    !
    !The spin rotation matrix is given by:
    !   cos(phi/2)*I-i*sin(phi/2)* dot(sigma,n)
    !where phi is the rotation angle,n is is the rotation axis, I is the identity
    !matrix and sigma is vector of pauli matrices. In our case sigma are the spin
    ! matrices.
    !Args:
    !   smat: the spin matrices. This is overwritten on output
    !   s_rot_axis: the rotation axis
    !   s_rot_angle: the rotation angle
    !   projs: defines projections on atoms
    !   atom: atom on which the projection is done
    !Returns:
    !   smat: the rotated spin matrix
    !--------------------------------------------------------------------------
    complex(dp), intent(inout) :: smat(:,:,:)
    real(dp), intent(in) :: s_rot_axis(3)
    real(dp), intent(in) :: s_rot_angle
    type(projs_type), optional, intent(in) :: projs
    integer, optional, intent(in) :: atom

    integer :: n_wann,i,orb
    complex(dp), allocatable  :: smat_t(:,:,:),srot(:,:), I_mat(:,:)

    if (.not. (present(projs) .eqv. present(atom))) then
        stop 'either both projs and atom must be present or none of them must be present'
    endif

    n_wann = size(smat,1)

    allocate(srot(n_wann,n_wann))
    allocate(smat_t(n_wann,n_wann,3))
    allocate(I_mat(n_wann,n_wann))

    I_mat = 0._dp
    if (.not. present(projs)) then
        do i=1,n_wann
            I_mat(i,i) = (1._dp,0._dp)
        end do
    else
        do i =1,projs%n_orbs(atom)
            orb = projs%orbs(i,atom)
            I_mat(orb,orb) = (1._dp,0._dp)
        end do
    end if

    srot = cos(s_rot_angle/2) * I_mat - (0_dp,1_dp) * sin(s_rot_angle/2) * ( smat(:,:,1) * s_rot_axis(1) + &
        smat(:,:,2) * s_rot_axis(2) + smat(:,:,3) * s_rot_axis(3) )

    do i=1,3
        call gemm(smat(:,:,i),srot,smat_t(:,:,i),transb='C')
        call gemm(srot,smat_t(:,:,i),smat(:,:,i))
    end do
end subroutine

function Fermi_Dirac_d1(E,Ef,T,tres) result(f)
    !--------------------------------------------------------------------------
    !First derivative of the Fermi-Dirac distribution function
    !--------------------------------------------------------------------------
    real(dp), intent(in) :: E
    real(dp), intent(in) :: Ef
    real(dp), intent(in) :: T
    integer, intent(in), optional :: tres
    real(dp) :: f

    integer :: tres_in
    real(dp) :: kT

    if (not(present(tres))) then
        tres_in = 50
    else
        tres_in = tres
    endif
    kT = kB * T
    if ( abs( (E - Ef) / kT ) > tres_in ) then
        f = 0._dp
    else
        f = -1 / kT / (1 + exp( (E - Ef) / kT ))**(2) * exp( (E - Ef) / kT )
    endif

end function

function Fermi_Dirac_d2(E,Ef,T,tres) result(f)
    !--------------------------------------------------------------------------
    !Second derivative of the Fermi-Dirac distribution function
    !--------------------------------------------------------------------------
    real(dp), intent(in) :: E
    real(dp), intent(in) :: Ef
    real(dp), intent(in) :: T
    integer, intent(in), optional :: tres
    real(dp) :: f

    integer :: tres_in
    real(dp) :: kT

    if (not(present(tres))) then
        tres_in = 50
    else
        tres_in = tres
    endif
    kT = kB * T
    if ( abs( (E - Ef) / kT ) > tres_in ) then
        f = 0._dp
    else
        f = 1 / kT**2 *  2 * ( 1 + exp( (E - Ef) / kT ) )**(-3) * exp(  2*(E - Ef) / kT) - &
            1 / kT**2 * ( 1 + exp( (E - Ef) / kT ) )**(-2) * exp(  (E - Ef) / kT) 
    endif

end function


end module
