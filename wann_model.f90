module wann_model_mod
!--------------------------------------------------------------------------
!This is a module for working with tight-binding Hamiltonians produced by
!Wannier90.
!It reads the wannier90_hr.dat file using the init routine and contains 
!the subroutines necessary for linear response calculations:
!   create_Hk: creates the k-dependent Hamiltonian at point k.
!   create_vk: constructs the velocity operator at point k
!   create_smat: creates the spin-operator
!--------------------------------------------------------------------------

use generic_model
use constants
use input, only: read_structure, parse_input, json_get_matrix, projs_type
use blas95
use mpi
use json_module

implicit none

type, extends (gen_model) :: wann_model

    integer :: n_r
    integer, allocatable :: r_deg(:)
    integer, allocatable :: rpts(:,:)
    complex(dp), allocatable :: Hr(:,:,:)

    logical :: use_x
    character(len=:), allocatable :: wann_prefix

    real(dp), allocatable :: pos(:,:)
    complex(dp), allocatable :: vr(:,:,:,:)
    complex(dp), allocatable :: smat_r(:,:,:,:)

    integer :: s_type
    real(dp) :: s_rot_angle, s_rot_axis(3)
    type(projs_type) :: projs
    logical :: projs_defined
    logical, allocatable :: smat_is_calculated(:)
    complex(dp), allocatable :: smat_calc(:,:,:,:)
    real(dp) :: latt_vecs(3,3)

    contains
        procedure :: init
        procedure :: create_smat
        procedure :: create_Hk
        procedure :: create_vk
        procedure :: create_vk_dk
        procedure :: def_projs
        procedure, private :: read_pos
        procedure, private :: create_vr
        procedure, private :: find_r
        procedure, private :: read_smat_r
        procedure, private :: read_wann_input
        procedure, private :: read_wann_ham
end type

contains

subroutine read_pos(self)
    !--------------------------------------------------------------------------
    !reads the wannier centers from a file
    !--------------------------------------------------------------------------

    class(wann_model) :: self

    integer :: u
    integer :: i

    allocate(self%pos(3,self%n_orb))
    
    open(newunit=u, file='input_wann', status='old')

    rewind u

    read(u,*) 
    read(u,*) 
    read(u,*) 
    read(u,*) 

    do i=1,self%n_orb
        read(u,*) self%pos(:,i)
    end do

end subroutine

subroutine read_wann_input(self)

    class(wann_model) :: self
    
    type(json_file) :: json
    logical :: found,found2
    character(len=:), allocatable :: variable_path
    real(dp) :: rot_angle_t
    real(dp), allocatable :: rot_axis_t(:)
    character(len=:), allocatable :: centers_unit

    json = parse_input()

    call json%get('model.wann.prefix',self%wann_prefix,found)
    if (.not. found) stop 'model.wann.prefix missing'

    call json%get('model.wann.f_type',self%f_type,found)
    if (.not. found) then
        stop 'model.wannier.f_type not present in the input file'
    endif
    if (self%f_type < 0) then
        self%use_x = .true.
    else 
        self%use_x = .false.
    end if

    call json%get('model.wann.s_type',self%s_type,found)
    if (.not. found) then
        stop 'model.wannier.s_type not present in the input file'
    endif

    if ( self%f_type > 0) then

        variable_path = 'model.wann.centers'
        call json_get_matrix(json,variable_path,self%pos,found,trans=.true.)
        if (.not. found) stop 'model.wann.centers are missing or incorrect format'
        if (size(self%pos,1) /= 3) stop 'wrong dimension of the model.wann.centers'

        call json%get('model.wann.centers_unit',centers_unit,found)
        if (found) then
            if (centers_unit == 'a.u.') then
                self%pos = self%pos * bohr
            elseif (centers_unit /= 'ang') then
                stop 'Unrecognized centers_unit, only "a.u." and "ang" allowed' 
            endif
        endif
    
    endif


    call json%get('model.wann.s_rot_angle',rot_angle_t,found)
    call json%get('model.wann.s_rot_axis',rot_axis_t,found2)
    if ( (found .and. (.not. found2) ) .or. (found2 .and. (.not. found)) ) then
        stop 'When model.wannier.s_rot_angle is pressent then also s_rot_axis must be present and vice versa'
    endif
    if (found) then
        self%s_rot_angle = pi * rot_angle_t
        if (size(rot_axis_t) /= 3) then
            stop 'wrong model.wannier.s_rot_axis dimension'
        endif
        self%s_rot_axis = rot_axis_t
    endif

    if (.not. found) then
        self%s_rot_angle = 0._dp
        self%s_rot_axis = 0._dp
    endif

end subroutine

subroutine read_wann_ham(self)

    class(wann_model) :: self

    integer :: i,u,n_wann,nl,r_i,stat
    integer :: orb1,orb2
    real(dp) :: val(2)
    integer :: r(3),r_curr(3)
    character(len=:), allocatable :: Hr_file

    Hr_file = trim(self%wann_prefix) // '_hr.dat'
    open(newunit=u, file=Hr_file, status='old')

    read(u,*) 
    read(u,*) self%n_orb
    read(u,*) self%n_r

    n_wann = self%n_orb

    allocate(self%r_deg(self%n_r))
    allocate(self%rpts(3,self%n_r))
    allocate(self%Hr(self%n_r,n_wann,n_wann))

    self%Hr = 0._dp

    !read the degeneracies of the Wigner-Seitz grid points
    nl = self%n_r/15
    do i=1,nl
        read(u,*) self%r_deg(1+(i-1)*15:i*15)
    end do
    read(u,*) self%r_deg(nl*15+1:self%n_r)

    r_i = 0
    !reads the actual Hamiltonian and makes a list of all positions
    do 
        read(u,*,iostat=stat) r(:), orb1, orb2, val(:)
        if (stat /= 0) exit

        if (r_i == 0) then 
            r_i = 1
            r_curr = r
            self%rpts(:,r_i) = r
        endif 

        if (.not. all(r == r_curr)) then
            r_i = r_i + 1
            self%rpts(:,r_i) = r
            r_curr = r
        endif

        self%Hr(r_i,orb1,orb2) = dcmplx(val(1),val(2))

    end do

    close(u)

end subroutine

subroutine init(self)
    !--------------------------------------------------------------------------
    !Reads the hamiltonian matrix elements in the Wannier basis outputted by
    !wannier 90.
    !
    !Also reads the input_wann file.
    !The first line contains the wannier prefix.
    !The second line of this file contains an integer stored as f_type, which controls the behavior of the model
    !   following options are implemented:
    !   0: the default - H(k)_nm = \sum \exp(i*k*R) H(R)_nm; v(k) is a k-derivative of H(k)
    !   1: the Fourrier transform contains also positions of orbitals:
    !       H(k)_nm = \sum \exp(i*k*R+r_m-r_n) H(R)_nm
    !       v(k) is a k-deriveative: v(k)_nm = 1/hbar \sum \exp(i*k*R+r_m-r_n)*i*(R+r_m-r_n) H(R)_nm
    !   2: the standard Fourrier transform, but v(k) is not a k-derivative
    !       H(k)_nm = \sum \exp(i*k*R) H(R)_nm
    !       v(k)_nm = 1/hbar \sum \exp(i*k*R)*i*(R+r_m-r_n) H(R)_nm
    !  -1: The position matrix is used to calculate [H,x] in the wannier basis, which is then Fourrier transformed
    !  -2: Same as -1, but some cutoffs are implemented to speed up calculating the commutator. Only position matrix
    !       elements between orbitals in cells at most 2 unit cells apart (in each direction) are considered
    !  -3: only the diagonal elements of positio matrix are considered. This should give results comparable to 1 and 2.
    !  -4: The commutator is calculated but only position matrix elements within the first unit cell are considered
    !If f_type > 0 the input_wann file must contain positions of all wannier orbitals (can be generate by
    !   define_projs --print-pos)
    !f_type 1 and 2 should give the same results - this seems to be satisfied pretty well
    !f_type -1 and -2 should also give pretty similar result, which is also satisfied
    !f_type -3 should give very close result to 1 and 2 - this also seems to be satisfied
    !f_type -4 should in most cases be pretty similar to -1 and -2
    !
    !The third line of the input_wann file contains integer variable s_type, which controls
    !how the spin matrix is calculated.
    !   0: Assuming that the first half of the Wannier functions are spin up and the second half spin down (for VASP)
    !   1: Assmuing that the Wannier functions are alternating spin-up and spin-down
    !  -1: Calculating the full spin matrix from smat_wann.dat file generated by wanier. 

    !Returns  (module variables):
    !   n_orb (integer): number of Wannier functions used
    !   n_r (integer): number of Wigner-Seitz grid points
    !   r_deg (integer(:)): degeneracies of the Wigner-Seitz grid points
    !   rpts (integer(:,:)): Wigner-Seitz grid points
    !   Hr (complex(:,:,:)): the Hamiltonian matrix elements stored as:
    !       the first index is the point number (as in rpts), the other two are
    !       orbital numbers
    !   pos (real(n_wann,3)): the positions of wannier orbitals (only if f_type > 0)
    !   vr (complex(n_r,n_wann,n_wann,3)) : the velocity operator in the wannier basis,
    !       only if f_type < 0
    !--------------------------------------------------------------------------

    class(wann_model) :: self

    integer :: n_wann
    integer :: rank,ierr

    self%projs_defined = .false.

    call self%read_wann_input()

    call self%read_wann_ham()

    call self%get_latt_vecs(self%latt_vecs)

    n_wann = self%n_orb

    !check if we have the correct number of wannier centers
    if ( self%f_type > 0) then
        if (size(self%pos,2) /= n_wann) stop 'wrong dimension of the wann.centers'
    endif

    if (self%use_x) then
        call mpi_comm_rank(mpi_comm_world,rank,ierr)
        if (rank == 0) then
            call self%create_vr()
        end if
        if (rank /= 0) then
            allocate(self%vr(self%n_r,self%n_orb,self%n_orb,3))
        end if
        call mpi_bcast(self%vr,3*self%n_r*self%n_orb*self%n_orb,mpi_double_complex,0,mpi_comm_world,ierr)
    end if

    if ( self%s_type == -1) then
        call self%read_smat_r()
    endif

end subroutine

subroutine create_Hk(self,k,Hk)
    !--------------------------------------------------------------------------
    !Creates the k-dependent Hamiltonian at point k by Fourrier transforming Hr
    !
    !Description:
    !   H_k(n,m) = sum_R exp(i*k*R)H_R(n,m)
    !
    !Notes:
    !   !!!!Only the lower diagonal part of Hk is calculated!!!!
    !
    !Args:
    !   r_deg: degeneracies of the Weigner-Seitz grid points 
    !   rpts: Weigner-Seitz grid points
    !   Hr: the Hamiltonian in the basis of Wannier functions
    !   k: k point
    !
    !Returns:
    !   Hk: the lower diagonal part of Hk
    !--------------------------------------------------------------------------

    class(wann_model) :: self
    real(dp), intent(in) :: k(3)
    complex(dp), allocatable, intent(out) :: Hk(:,:)

    integer :: n_r !number of R points
    integer :: n_wann
    integer :: i,n,m
    complex(dp), allocatable :: exps_vec(:)
    real(dp) :: latt_vecs(3,3) !lattice vectors
    real(dp) :: r(3)

    n_r = size(self%Hr,1)
    n_wann = self%n_orb

    allocate(Hk(n_wann,n_wann))
    Hk(:,:) = 0_dp

    allocate(exps_vec(n_r))

    latt_vecs = self%latt_vecs

    !to speed up the calculation we first calculate exp(i*k*R) and then calculate the fourrier
    !transform as a dot product
    do i=1,self%n_r
        !express R in cartesian coordinates in the units of Ang
        r = self%rpts(1,i) * latt_vecs(:,1) + self%rpts(2,i) * latt_vecs(:,2) + self%rpts(3,i) * latt_vecs(:,3) 
        !the conjugate is here becase dotc also contains a conjg and we dont want that
        !has to be divided by r_deg, don't really know why
        !exps_vec(i) = conjg(exp(ii*dot_product(r,k)) / self%r_deg(i))
        exps_vec(i) = dconjg(exp(ii*dot(r,k)) / self%r_deg(i))
    end do

    !only the lower diagonal part is calculated
    do m=1,n_wann
        do n=m, n_wann
            
            if (self%f_type == 1) then
                Hk(n,m) = exp(ii*dot(k,self%pos(:,m) - self%pos(:,n))) * dotc(exps_vec,self%Hr(:,n,m))
            else 
                Hk(n,m) =  dotc(exps_vec,self%Hr(:,n,m))
            end if

        end do
    end do

end subroutine

subroutine create_vk(self,k,vk,ind)
    !--------------------------------------------------------------------------
    !Creates the velocity operator at point k as a derivative of Hk
    !
    !Description:
    !   Very similar to how Hk is calculated
    !   v_k(n,m) = 1/hbar * \partial(Hk)\partial(k) = 
    !       = 1/hbar * sum_R exp(i*k*R)H_R(n,m) * i * R
    !   When ind is present only the ind component of the velocity operator
    !   is calculated otherwise all three components are calculated
    !
    !Args: (needs output from read_Hr)
    !   r_deg: Degeneracies of Weigner-Seitz grid points
    !   rpts: Weigner-Seitz grid points
    !   Hr: the Wannier Hamiltonian
    !   k: k point 
    !   ind (optional): when present, only this component of the v operator is 
    !       returned
    !
    !Returns:
    !   vk: the velocity operator at point k
    !--------------------------------------------------------------------------

    class(wann_model) :: self
    real(dp), intent(in) :: k(3)
    complex(dp), allocatable, intent(out) :: vk(:,:,:)
    integer, intent(in), optional :: ind

    integer :: n_r
    integer :: n_wann
    integer :: i,j,n,m
    complex(dp), allocatable :: exps_vec(:),exps_vec2(:)
    real(dp) :: latt_vecs(3,3)
    real(dp) :: r(3),rh(3)

    n_r = size(self%Hr,1)
    n_wann = self%n_orb

    allocate(vk(n_wann,n_wann,3))
    vk(:,:,:) = 0_dp

    latt_vecs = self%latt_vecs

    if (self%use_x) then

        allocate(exps_vec(n_r))
        do i = 1,n_r
            r = self%rpts(1,i) * latt_vecs(:,1) + self%rpts(2,i) * latt_vecs(:,2) + self%rpts(3,i) * latt_vecs(:,3)
            exps_vec(i) = dconjg(exp(ii*dot(r,k))  / self%r_deg(i))
        end do
        do j=1,3
            if (present(ind)) then !if ind is present and j/=ind skip to the next cycle
                if (ind /= j) cycle
            endif
            do m=1,n_wann
                do n=m, n_wann
                    vk(n,m,j) = dotc(exps_vec,self%vr(:,n,m,j))
                end do
            end do
            !use the fact the vk must be Hermitian to find the lower part
            do m=2,n_wann
                do n=1,m-1
                    vk(n,m,j) = dconjg(vk(m,n,j)) 
                end do
            end do
        end do

    else

        if (self%f_type == 0) then

            allocate(exps_vec(n_r))
            !create the upper diagonal part of vk
            do j=1,3
                if (present(ind)) then !if ind is present and j/=ind skip to the next cycle
                    if (ind /= j) cycle
                endif
                do i = 1,n_r
                    r = self%rpts(1,i) * latt_vecs(:,1) + self%rpts(2,i) * latt_vecs(:,2) + self%rpts(3,i) * latt_vecs(:,3)
                    exps_vec(i) = dconjg(exp(ii*dot(r,k)) * ii * r(j) / self%r_deg(i))
                end do
                do m=1,n_wann
                    do n=m, n_wann
                        vk(n,m,j) = dotc(exps_vec,self%Hr(:,n,m))
                    end do
                end do

                !use the fact the vk must be Hermitian to find the lower part
                do m=2,n_wann
                    do n=1,m-1
                        vk(n,m,j) = dconjg(vk(m,n,j)) 
                    end do
                end do
            end do

        else if (self%f_type == 1) then

            allocate(exps_vec(n_r))
            allocate(exps_vec2(n_r))
            do j=1,3
                if (present(ind)) then !if ind is present and j/=ind skip to the next cycle
                    if (ind /= j) cycle
                endif
                do i = 1,n_r
                    r = self%rpts(1,i) * latt_vecs(:,1) + self%rpts(2,i) * latt_vecs(:,2) + self%rpts(3,i) * latt_vecs(:,3)
                    exps_vec(i) = dconjg(exp(ii*dot(r,k)) * ii * r(j) / self%r_deg(i))
                    exps_vec2(i) = dconjg(exp(ii*dot(r,k)) / self%r_deg(i))
                end do
                do m=1,n_wann
                    do n=m, n_wann
                        rh = ( self%pos(:,m)-self%pos(:,n) )
                        vk(n,m,j) = dotc(exps_vec,self%Hr(:,n,m)) * exp(ii*dot(k,rh)) + &
                            dotc(exps_vec2,self%Hr(:,n,m)) * exp(ii*dot(k,rh)) * ii * rh(j)
                    end do
                end do

                !use the fact the vk must be Hermitian to find the lower part
                do m=2,n_wann
                    do n=1,m-1
                        vk(n,m,j) = dconjg(vk(m,n,j)) 
                    end do
                end do
            end do

        else if (self%f_type == 2) then

            allocate(exps_vec(n_r))
            allocate(exps_vec2(n_r))
            do j=1,3
                if (present(ind)) then !if ind is present and j/=ind skip to the next cycle
                    if (ind /= j) cycle
                endif
                do i = 1,n_r
                    r = self%rpts(1,i) * latt_vecs(:,1) + self%rpts(2,i) * latt_vecs(:,2) + self%rpts(3,i) * latt_vecs(:,3)
                    exps_vec(i) = dconjg(exp(ii*dot(r,k)) * ii * r(j) / self%r_deg(i))
                    exps_vec2(i) = dconjg(exp(ii*dot(r,k)) / self%r_deg(i))
                end do
                do m=1,n_wann
                    do n=m, n_wann
                        rh = ( self%pos(:,m)-self%pos(:,n) )
                        vk(n,m,j) = dotc(exps_vec,self%Hr(:,n,m))  + &
                            dotc(exps_vec2,self%Hr(:,n,m)) * ii * rh(j)
                    end do
                end do

                !use the fact the vk must be Hermitian to find the lower part
                do m=2,n_wann
                    do n=1,m-1
                        vk(n,m,j) = dconjg(vk(m,n,j)) 
                    end do
                end do
            end do

        end if

        vk = vk / hbar

    end if


end subroutine

subroutine create_vk_dk(self,k,vk_dk)
    !--------------------------------------------------------------------------
    !Calculates the derivative of vk with respect to k
    !right now only f_type = 1 is implemented
    !--------------------------------------------------------------------------

    class(wann_model) :: self
    real(dp), intent(in) :: k(3)
    complex(dp), allocatable, intent(out) :: vk_dk(:,:,:,:)

    integer :: n_r
    integer :: n_wann
    integer :: i,j,n,m,r_i
    complex(dp), allocatable :: exps_vec(:),exps_vec2(:),exps_vec3(:),exps_vec4(:)
    real(dp) :: latt_vecs(3,3)
    real(dp) :: r(3),rh(3)
    complex(dp) :: expr,exprh

    n_r = size(self%Hr,1)
    n_wann = self%n_orb

    allocate(vk_dk(n_wann,n_wann,3,3))
    vk_dk(:,:,:,:) = 0_dp

    latt_vecs = self%latt_vecs

    if (self%f_type /= 1) then
        stop 'vk_dk implemented only for f_type = 1'
    endif

    allocate(exps_vec(n_r))
    allocate(exps_vec2(n_r))
    allocate(exps_vec3(n_r))
    allocate(exps_vec4(n_r))

    do i=1,3
        do j=1,3
            do r_i=1,n_r
                r = self%rpts(1,r_i) * latt_vecs(:,1) + self%rpts(2,r_i) * latt_vecs(:,2) + self%rpts(3,r_i) * latt_vecs(:,3)
                expr = exp(ii*dot(r,k)) 

                exps_vec(r_i) = dconjg(expr / self%r_deg(i))
                exps_vec2(r_i) = dconjg(expr * r(i)  / self%r_deg(i))
                exps_vec3(r_i) = dconjg(expr * r(j)  / self%r_deg(i))
                exps_vec4(r_i) = dconjg(expr * r(i) * r(j) / self%r_deg(i))
            end do
            
            do m=1,n_wann
                do n=m, n_wann
                    rh = ( self%pos(:,m)-self%pos(:,n) )
                    exprh = exp(ii*dot(rh,k))
                    vk_dk(n,m,i,j) =  - exprh * ( &
                         rh(i) * rh(j) * dotc(exps_vec,self%Hr(:,n,m)) + &
                         rh(j) * dotc(exps_vec2,self%Hr(:,n,m)) + &
                         rh(i) * dotc(exps_vec3,self%Hr(:,n,m)) + &
                         dotc(exps_vec4,self%Hr(:,n,m)))
                 end do
             end do
             
             do m=2,n_wann
                 do n=1,m-1
                     vk_dk(n,m,i,j) = dconjg(vk_dk(m,n,i,j)) 
                 end do
             end do
         end do
    end do

    vk_dk = vk_dk / hbar / hbar

end subroutine

subroutine create_vr(self)
    !--------------------------------------------------------------------------
    !Creates the velocity operator in the wannier basis by calculating commuta-
    !tor of position operato and the Hamiltonian
    !
    !The position operator matrix elements are:
    !   x_rnm = <n0|x|mr>
    !Hamiltonian matrix elements are:
    !   H_rnm = <n0|H|mr>
    !velocity is given by v = i/hbar * [H,x]
    !The commutator can be expressed as:
    !   [H,x]_rnm = H_rnm*r + \sum_r'j H_r'nj*x_(r-r')jm - x_r'nj*H_(r-r')jm
    !
    !Needs the x_matrix as an input, in file xmat_wann.dat
    !
    !Different behavior based on self%f_type value
    !Returns:
    !   self%vr(n_r,n_wann,n_wann,3): the velocity matrix in the wannier basis
    !       i.e., vr(r,n,m,j) = <n0|v(j)|mr>
    !--------------------------------------------------------------------------
    class(wann_model) :: self

    integer :: n_wann
    complex(dp), allocatable :: x_mat(:,:,:,:)
    complex(dp) :: val
    integer :: u 
    integer :: i,r,n,m,j,r2,r3,r0
    integer :: nr1
    real(dp) :: latt_vecs(3,3)
    real(dp) :: rr(3)
    integer :: mode
    integer :: tr(3)
    logical :: take1,take2,debug

    write(*,*) 'Starting to create v_r'

    n_wann = self%n_orb

    allocate(x_mat(self%n_r,n_wann,n_wann,3))
    allocate(self%vr(self%n_r,n_wann,n_wann,3))
    self%vr = 0._dp

    open(newunit=u, file='xmat_wann.dat', status='old')

    do j=1,n_wann*n_wann*self%n_r*3
        read(u,*) i,r,n,m,val
        x_mat(r,n,m,i) = val
    end do

    write(*,*) 'x_mat read'

    self%vr = 0
    nr1 = int((self%n_r**(1/dble(3)) - 1) / 2)

    mode = 2 

    debug = .false.

    if (self%f_type >= -2) then

        do j=1,3
            do r=1,self%n_r
                do r2=1,self%n_r

                    if (self%f_type == -1) then
                        take1 = .true.
                        take2 = .true.
                    else if (self%f_type == -2) then
                        tr = self%rpts(:,r)-self%rpts(:,r2)
                        if (abs(tr(1)) <= 2 .and. abs(tr(2)) <= 2 .and. abs(tr(3)) <= 2) then
                            take1 = .true.
                        else
                            take1 = .false.
                        end if
                        if (abs(self%rpts(1,r2)) <= 2 .and. abs(self%rpts(2,r2)) <= 2 &
                            .and. abs(self%rpts(3,r2)) <= 2) then
                            take2 = .true.
                        else
                            take2 = .false.
                        endif
                    else
                        stop 'wrong f_type'
                    end if

                    if (take1 .or. take2) then
                        call self%find_r(r,r2,r3)
                        !we calculate the commutator only if we found r3=r-r2, which is not always the case
                        if (r3 /= 0) then
                            if (take1) then
                                call gemm(self%Hr(r2,:,:),x_mat(r3,:,:,j),self%vr(r,:,:,j),alpha=dcmplx(1),beta=dcmplx(1))
                            end if
                            if (take2) then
                                call gemm(x_mat(r2,:,:,j),self%Hr(r3,:,:),self%vr(r,:,:,j),alpha=dcmplx(-1),beta=dcmplx(1))
                            end if
                        end if
                        if (debug) then
                            write(*,*) 'r',self%rpts(:,r)
                            write(*,*) 'r2',self%rpts(:,r2)
                            if (r3 /= 0) then
                                write(*,*) 'r3',self%rpts(:,r3)
                            else
                                
                                if (abs(tr(1)) < 3 .and. abs(tr(2)) < 3 .and. abs(tr(3)) < 3) then
                                    write(*,*) 'r3 not found, small', tr
                                else
                                    write(*,*) 'r3 not found', tr
                                endif
                            end if
                        endif
                    end if
                end do
            end do
        end do

    else if (self%f_type == -4) then
        r2 = 1
        call self%find_r(r2,r2,r3)!this finds r3, which corresponds to [0,0,0], i.e. the first unit cell
        do j=1,3
            do r=1,self%n_r
                call gemm(self%Hr(r,:,:),x_mat(r3,:,:,j),self%vr(r,:,:,j),alpha=dcmplx(1),beta=dcmplx(1))
                call gemm(x_mat(r3,:,:,j),self%Hr(r,:,:),self%vr(r,:,:,j),alpha=dcmplx(-1),beta=dcmplx(1))
            end do
        end do
    end if


    write(*,*) 'the commutator is done'

    latt_vecs = self%latt_vecs

    if (self%f_type == -3) then
        do r=1,self%n_r
            if (self%rpts(1,r) == 0 .and. self%rpts(2,r) == 0 .and. self%rpts(3,r) == 0) then
                r0 = r
            end if
        end do
    end if

    !add the diagonal part
    do j=1,3
        do m=1,n_wann
            do n=1,n_wann
                do r=1,self%n_r
                    rr = self%rpts(1,r) * latt_vecs(:,1) + self%rpts(2,r) * latt_vecs(:,2) + self%rpts(3,r) * latt_vecs(:,3)
                    !for f_type == 3 we only add the diagonal elements of the position matrix, so we don't have to calculate the
                    !commutator, but rather we do it here
                    if (self%f_type /= -3) then
                        self%vr(r,n,m,j) = self%vr(r,n,m,j) + self%Hr(r,n,m) * rr(j)
                    else
                        self%vr(r,n,m,j) = self%vr(r,n,m,j) + self%Hr(r,n,m) * (rr(j) + x_mat(r0,m,m,j) - x_mat(r0,n,n,j)) 
                    endif
                end do
            end do
        end do
    end do

    self%vr = ii/hbar * self%vr

    write(*,*) 'v_r created'

end subroutine


subroutine find_r(self,r,r2,r3)
    class(wann_model) :: self
    integer, intent(in) :: r
    integer, intent(in) :: r2
    integer, intent(out) :: r3

    integer :: rp(3)
    integer :: i

    rp = self%rpts(:,r)-self%rpts(:,r2)

    r3 = 0
    do i=1,self%n_r
        if (self%rpts(1,i) == rp(1) .and. &
            self%rpts(2,i) == rp(2) .and. &
            self%rpts(3,i) == rp(3)) then
            r3 = i
        end if
    end do

end subroutine

            
subroutine read_smat_r(self)

    class(wann_model) :: self

    integer :: i,j,ri,n,m,u,n_wann
    complex(dp) :: val

    n_wann = self%n_orb

    allocate(self%smat_r(self%n_r,n_wann,n_wann,3))
    open(newunit=u, file='smat_wann.dat', status='old')

    do j=1,n_wann*n_wann*self%n_r*3
        read(u,*) i,ri,n,m,val
        self%smat_r(ri,n,m,i) = val
    end do

    close(u)

end subroutine
            
subroutine create_smat(self,k,smat,atom)
    !--------------------------------------------------------------------------
    !Defines the matrix of the spin-operator
    !
    !Several modes are implemented depending on the value of self%s_type
    !   0: Assuming that the first half of the Wannier functions are spin up and the second half spin down (for VASP)
    !   1: Assmuing that the Wannier functions are alternating spin-up and spin-down
    !  -1: Calculating the full spin matrix from smat_wann.dat file generated by wanier. 
    !
    ! Spin matrix is dimensionless and doesn't contain the 1/2 so S = sigma.
    !
    !Args:
    !   n_wann: magnitude of the Hamiltonian
    !   atom(optional): number of atom on which the spin-operator is projected
    !       if atom is not present, no projection is done
    !
    !Returns:
    !   smat: all three components of the spin matrix
    !--------------------------------------------------------------------------
    class(wann_model) :: self
    real(dp),  intent(in) :: k(3)
    complex(dp), allocatable, intent(out) :: smat(:,:,:)
    integer, optional, intent(in) :: atom

    type(projs_type) :: projs
    integer :: i,orb,n_wann,n,m,atom_index,n_orb,m_orb
    real(dp) :: latt_vecs(3,3) !lattice vectors
    real(dp) :: r(3)
    complex(dp), allocatable :: exps_vec(:)
    complex(dp), allocatable  :: smat_t(:,:,:),srot(:,:), I_mat(:,:)

    n_wann = self%n_orb

    allocate(smat(n_wann,n_wann,3))
    smat(:,:,:) = 0._dp

    if ((.not. present(atom)) .or. (atom == 0)) then
        atom_index = 0
    else
        atom_index = atom
    end if

    if (atom_index /= 0) then
        if (.not. self%projs_defined) then
            stop 'projs must be defined to use projections'
        end if
        projs = self%projs
    end if
    
    if (.not. allocated(self%smat_is_calculated)) then
        allocate(self%smat_is_calculated(0:projs%n_atoms))
        self%smat_is_calculated = .false.
        allocate(self%smat_calc(self%n_orb,self%n_orb,3,0:projs%n_atoms))
        self%smat_calc(:,:,:,:) = 0_dp
    end if

    !checks if smat has already been calculated for this index so that it does not have to be calculated again
    !used only if smat is k-independent
    if ( self%smat_is_calculated(atom_index) ) then
        smat = self%smat_calc(:,:,:,atom_index)
    else

        if (self%s_type == 0) then 

            if ( atom_index == 0 ) then
            
                do i =1,n_wann/2
                    !sigma_x
                    smat(i,n_wann/2+i,1) = 1._dp
                    smat(n_wann/2+i,i,1) = 1._dp
                    !sigma_y
                    smat(i,n_wann/2+i,2) = (0._dp,-1._dp)
                    smat(n_wann/2+i,i,2) = (0._dp,1._dp)
                    !sigma_z
                    smat(i,i,3) = 1._dp 
                    smat(n_wann/2+i,n_wann/2+i,3) = -1._dp 
                end do
            
            else
                !sigma_x
                do i =1,projs%n_orbs(atom)/2
                    orb = projs%orbs(i,atom)
                    smat(orb,n_wann/2+orb,1) = 1._dp
                    smat(n_wann/2+orb,orb,1) = 1._dp
                end do

                !sigma_y
                do i =1,projs%n_orbs(atom)/2
                    orb = projs%orbs(i,atom)
                    smat(orb,n_wann/2+orb,2) = (0._dp,-1._dp)
                    smat(n_wann/2+orb,orb,2) = (0._dp,1._dp)
                end do

                !sigma_z
                do i =1,projs%n_orbs(atom)/2
                    orb = projs%orbs(i,atom)
                    smat(orb,orb,3) = 1._dp 
                    smat(n_wann/2+orb,n_wann/2+orb,3) = -1._dp 
                end do
            endif

        else if (self%s_type == 1) then
            
            if ( atom_index == 0 ) then
            
                do i =1,n_wann/2
                    !sigma_x
                    smat(2*i-1, 2*i, 1) = 1._dp
                    smat(2*i, 2*i-1, 1) = 1._dp
                    !sigma_y
                    smat(2*i-1, 2*i, 2) = (0._dp,-1._dp)
                    smat(2*i, 2*i-1, 2) = (0._dp,1._dp)
                    !sigma_z
                    smat(2*i-1,2*i-1,3) = 1._dp 
                    smat(2*i,2*i,3) = -1._dp 
                end do

            else
                do i =1,projs%n_orbs(atom)
                    orb = projs%orbs(i,atom)
                    if ( modulo(orb,2) == 0 ) then
                        smat(orb-1,orb,1) = 1._dp 
                        smat(orb-1,orb,2) = (0._dp,-1._dp)
                        smat(orb,orb,3) = -1._dp
                    else
                        smat(orb+1,orb,1) = 1._dp
                        smat(orb+1,orb,2) = (0._dp,1._dp) 
                        smat(orb,orb,3) = 1._dp
                    end if
                end do
            endif

        else if (self%s_type == -1) then

            latt_vecs = self%latt_vecs

            allocate(exps_vec(self%n_r))
            !to speed up the calculation we first calculate exp(i*k*R) and then calculate the fourrier
            !transform as a dot product
            do i=1,self%n_r
                !express R in cartesian coordinates in the units of Ang
                r = self%rpts(1,i) * latt_vecs(:,1) + self%rpts(2,i) * latt_vecs(:,2) + self%rpts(3,i) * latt_vecs(:,3) 
                !the conjugate is here becase dotc also contains a conjg and we dont want that
                !has to be divided by r_deg, don't really know why
                exps_vec(i) = conjg(exp(ii*dot_product(r,k)) / self%r_deg(i))
            end do

            do i = 1,3
                if ( atom_index == 0) then
                    !only the lower diagonal part is calculated
                    do m=1,n_wann
                        do n=m, n_wann
                            if (self%f_type == 1) then
                                smat(n,m,i) = exp(ii*dot_product(k,self%pos(:,m) - self%pos(:,n))) * &
                                    dotc(exps_vec,self%smat_r(:,n,m,i))
                            else 
                                smat(n,m,i) =  dotc(exps_vec,self%smat_r(:,n,m,i))
                            end if

                        end do
                    end do
                    do m=2,n_wann
                        do n=1,m-1
                            smat(n,m,i) = conjg(smat(m,n,i))
                        end do
                    end do
                else
                    do m=1,projs%n_orbs(atom_index)
                        do n=1, projs%n_orbs(atom_index)
                            n_orb = projs%orbs(n,atom_index)
                            m_orb = projs%orbs(m,atom_index)
                            if (self%f_type == 1) then
                                smat(n_orb,m_orb,i) = exp(ii*dot_product(k,self%pos(:,m_orb) - self%pos(:,n_orb))) * &
                                dotc(exps_vec,self%smat_r(:,n_orb,m_orb,i))
                            else 
                                smat(n_orb,m_orb,i) =  dotc(exps_vec,self%smat_r(:,n_orb,m_orb,i))
                            end if
                        end do
                    end do
                end if
            end do

        else
            stop 'wrong s_type'
        endif
    
        !this rotates the spin matrices
        !should probably also work for a k-dependent spin matrix, but I'm not completely sure about that
        if ( abs(self%s_rot_angle) > 1e-14 ) then
            allocate(srot(n_wann,n_wann))
            allocate(smat_t(n_wann,n_wann,3))
            allocate(I_mat(n_wann,n_wann))

            I_mat = 0._dp
            if (atom_index == 0) then
                do i=1,n_wann
                    I_mat(i,i) = (1._dp,0._dp)
                end do
            else
                do i =1,projs%n_orbs(atom)
                    orb = projs%orbs(i,atom)
                    I_mat(orb,orb) = (1._dp,0._dp)
                end do
            end if

            srot = cos(self%s_rot_angle/2) * I_mat - (0_dp,1_dp) * sin(self%s_rot_angle/2) * ( smat(:,:,1) * self%s_rot_axis(1) + &
                    smat(:,:,2) * self%s_rot_axis(2) + smat(:,:,3) * self%s_rot_axis(3) )
            
            do i=1,3
                call gemm(smat(:,:,i),srot,smat_t(:,:,i),transb='C')
                call gemm(srot,smat_t(:,:,i),smat(:,:,i))
            end do
        end if

        if (self%s_type == 0 .or. self%s_type == 1) then
            self%smat_is_calculated(atom_index) = .true.
            self%smat_calc(:,:,:,atom_index) = smat
        end if

    end if


end subroutine

subroutine def_projs(self,projs)
    !--------------------------------------------------------------------------
    !Defines projections on atoms
    !
    !Args:
    !   projs(projs_type): the projections definition as outputed by read_projs
    !
    !--------------------------------------------------------------------------
    class(wann_model) :: self
    type(projs_type), intent(in) :: projs

    integer :: i

    self%projs = projs
    self%projs_defined = .true.

    allocate(self%smat_is_calculated(0:projs%n_atoms))
    allocate(self%smat_calc(self%n_orb,self%n_orb,3,0:projs%n_atoms))
    do i=0,projs%n_atoms
        self%smat_is_calculated(i) = .false.
    end do
    self%smat_calc(:,:,:,:) = 0_dp

end subroutine 

end module

