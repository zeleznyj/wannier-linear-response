module response
!--------------------------------------------------------------------------
!This calculates various linear response quantities using either a wannier
!tb Hamiltonian or an empirical tight-binding Hamiltonian
!--------------------------------------------------------------------------

use mpi
use constants
use input, only: input_type, write_input, read_projs, get_tensor_rank, projs_type
use eval_k, only: response_k
use k_integrate, only: cross_product,integrate_sum_1uc_para,integrate_sum_1uc_boundaries
use generic_model, only: gen_model
implicit none

contains

function get_unit_conversion_factor(inp,model) result(f)
    type(input_type), intent(in) :: inp 
    class(gen_model), allocatable, intent(in) :: model
    real(dp) :: f

    real(dp) :: latt_vecs(3,3)
    real(dp) :: vol

    call model%get_latt_vecs(latt_vecs)
    vol = dot_product(latt_vecs(:,1),cross_product(latt_vecs(:,2),latt_vecs(:,3)))

    f = 1.0_dp
    !------------- UNITS ---------------

    !this is missing in the integral, it's a prefactor because we are actually calculating a sum
    !over k-points
    f = f / (2*pi)**3 
    !Since the integral returns result per unit cell volume, which is in angstrom we need to convert to m
    f = f / 1e-30

    if (inp%order == 1) then
        !convert to units of [A/vol]*m/V, where vol is the volume of unit cell
        !That is units of the observable A per unit cell volume per electric field
        f = f * 1e-10_dp / eV
    elseif (inp%order == 2) then
        !convert to units of [A]/m/V**2 == [A/vol] * m**2/V**2
        !That is units of the observable A per unit cell volume per electric field squared
        f = f * 1e-20_dp / eV**2
    endif

    !add correct prefactors for the units of the observable A
    !also other unit conversions specific to each calculation
    if (inp%linres == 'cisp') then
        !We want spin in a unit cell per electric field
        !So we need to multiply by vol to get spin per unit cell volume and divide by 2
        !since the spin matrix within the code is just the pauli matrix
        !the unit cell volume is in angstrom so this has to be converted to m
        f = f * vol * 1e-30_dp / 2 
        !X = X * vol * 1e-10_dp / 2  / eV
    elseif (inp%linres == 'cond') then
        !convert to units 1/Omega/cm
        !in the calculation A is just velocity operator so we need to add factor -e
        !v is in the units of A/s so we need to convert to m/s too
        f = f  * (-eele) * 1e-10_dp
        if (inp%order == 1) then
            !This would have the units of 1/Ohm/m, but we want 1/Ohm/cm so we need in the end:
            f = f / 1e2_dp
            !X = X  * (-eele)  * 1e-8_dp / eV / 1e-16_dp
        !for inp%order == 2 we don't have to do anything as the units are already what we want: 1/Ohm/V
        endif
            
    elseif (inp%linres == 'she' ) then
        !Convert the velocity from Ang/s to m/s and add factor of 2 because of spin
        f = f  * 1e-10_dp / 2
        !This gives the result in h/Ohm/m, but we want hbar/e/Ohm/cm
        !so we need to mulitply by e and convert m to cm
        !We want the result in the units of hbar/e / cm / Omega
        if (inp%order == 1) then
            f = f * eele / 1e2_dp
        else if (inp%order == 2) then
            f = f * eele
        endif
        !X = X * 10**8 /2 / (2*pi)**3
    !elseif (inp%linres == 'cond2') then
        !just need to include the 2pi^3 factor which is missing in the integral
        !X = X 
    endif

end function



subroutine calculate_response(inp,model)

    type(input_type), intent(in) :: inp 
    class(gen_model), allocatable, intent(in) :: model

    integer :: i,j,l,m,n,o,q,r,s,l2,par(5)
    real(dp), allocatable :: X(:,:), X_t(:,:)
    real(dp), allocatable :: Xtot(:,:), Xtot_t(:,:)
    real(dp) :: t1,t2,t3,t4
    real(dp) :: times(10,2)
    integer :: ierr,rank,ntasks
    integer :: nktot, n_fors
    integer :: n_gam, n_projs, nEf
    real(dp) :: latt_vecs(3,3)
    real(dp) :: vol
    real(dp) :: f
    type(projs_type) :: projs
    integer, allocatable :: atoms(:)
    real(dp), allocatable :: Xlinres(:,:)
    real(dp), allocatable :: Xcond2(:,:,:)
    integer :: X_rank
    real(dp), allocatable :: X2(:,:),X3(:,:,:),X4(:,:,:,:)

    times = 0_dp

    call mpi_comm_rank(mpi_comm_world,rank,ierr)
    if (rank == 0) then 
        t1 = mpi_wtime()
    endif
    call mpi_comm_size(mpi_comm_world,ntasks,ierr)

    !------------------------
    !mpi communication test
    allocate(X_t(9,10),Xtot_t(9,10))
    Xtot_t = rank
    call mpi_reduce(X_t,Xtot_t,9*10,mpi_double_precision,mpi_sum,0,mpi_comm_world,ierr)
    if (rank == 0) then 
        write(*,*) 'communication ok'
    endif
    !------------------------

    call model%get_latt_vecs(latt_vecs)

    if (rank == 0) then 
        t2 = mpi_wtime()
    endif

    !if there are projections read the projection definitions
    if (inp%n_projs_inp > 0) then
        call read_projs(projs)
        call model%def_projs(projs)
    endif

    nktot = inp%nk(1)*inp%nk(2)*inp%nk(3)
    !parallelization is the most efficient for commensurate number of kpoints and tasks, but 
    !this doesnt matter in most cases
    if (rank == 0) then
        if ((nktot/ntasks)*ntasks /= nktot) then
            write(*,*) 'Use commensurate number of kpoints and tasks for best result'
        endif
    endif

    allocate(X(inp%n_X,inp%n_params))
    allocate(Xtot(inp%n_X,inp%n_params))
    allocate(X2(3,3))
    allocate(X3(3,3,3))
    allocate(X4(3,3,3,3))

    if (rank == 0) then 
        t3 = mpi_wtime()
    endif

    !integrate the linear response formular over k
    X = reshape(integrate_sum_1uc_para(func,inp%n_X*inp%n_params,inp%nk,latt_vecs,rank),&
        [inp%n_X,inp%n_params])
    vol = dot_product(latt_vecs(:,1),cross_product(latt_vecs(:,2),latt_vecs(:,3)))
    
    f = get_unit_conversion_factor(inp,model)
    X = X * f

    call mpi_reduce(X,Xtot,inp%n_X*inp%n_params,mpi_double_precision,mpi_sum,0,mpi_comm_world,ierr)

    if (rank == 0) then 
        t4 = mpi_wtime()
    endif

    !prints the output
    if (rank == 0) then
        write(*,*) 'total number of k-points', inp%nk(1)*inp%nk(2)*inp%nk(3)
        write(*,*) 'number of processes', ntasks
        write(*,*) 'reading input took', t2-t1, 's'
        write(*,*) 'calculation took', t4-t3, 's'
        write(*,*) 'time per 1 process and 1000 k points', (t4-t3) * ntasks / (inp%nk(1)*inp%nk(2)*inp%nk(3))
        write(*,*) 
        write(*,*) 'Detailed timings:'
        write(*,*) 'constructing the velocity operator took:', times(1,2)-times(1,1), 's'
        write(*,*) 'constructing Hk took:', times(7,2)-times(7,1), 's'
        write(*,*) 'diagonalizing the Hamiltonian took:', times(8,2)-times(8,1), 's'
        write(*,*) 'constructing the spin-operator (for all projections) took:', times(3,2)-times(3,1), 's'
        write(*,*) 'matrix elements of velocity took:', times(2,2)-times(2,1), 's'
        write(*,*) 'matrix elements of spin(for all projections) took:', times(4,2)-times(4,1), 's'
        write(*,*) 'evaluating all the linear response formulas for all gammas took:', times(5,2)-times(5,1), 's'
        write(*,*) 'response_k total:', times(6,2)-times(6,1), 's'
        write(*,*) 
        if (inp%order == 1) then
            if (inp%linres == 'cisp') then
                write(*,*) 'CISP in units of hbar*m/V'
            elseif (inp%linres == 'cond') then
                write(*,*) 'Conductivity in units of 1/(Ohm*cm)'
            elseif (inp%linres == 'she' .or. inp%linres == 'she2') then
                write(*,*) 'SHE in units of hbar/e/Omega/cm'
            endif
        elseif (inp%order == 2) then
            if (inp%linres == 'cond') then
                write(*,*) 'Second-order conductivity in units of 1/(Omega*V)'
            endif
            if (inp%linres == 'she') then
                write(*,*) 'Second-order spin conductivity in units of hbar/e/(Omega*V)'
            endif
        endif

        
        do l=1,inp%params%n_gammas
            write(*,*) '*****************************'
            write(*,*) 'Gamma: ', inp%params%gammas(l)
            do q=1,inp%params%n_Efs
                if ( inp%params%n_Efs > 1) then
                    write(*,*)
                    write(*,*) 'fermi level:', inp%params%Efs(q)
                endif
                do n=1,inp%params%n_projs
                    if (inp%n_projs_inp > 0 ) then
                        if (inp%params%projs(n)==0) then
                            write(*,*) 'Total:'
                        else
                            write(*,*) 'Projection on atom', inp%params%projs(n)
                        endif
                    endif
                    do m=1,inp%params%n_formulas
                        write(*,*) 'Formula: ', inp%params%formulas(m)
                        do s = 1, inp%params%n_omegas
                            if ( inp%params%n_omegas > 1) then
                                write(*,*)
                                write(*,*) 'Omega:', inp%params%omegas(s)
                            endif
                            l2 = inp%params%get_r([m,q,l,n,s])
                            if (inp%linres == 'cond' .or. inp%linres == 'cisp') then
                                if (inp%order == 1) then
                                    X2 = reshape(Xtot(:,l2),[3,3])
                                    do i =1,3
                                        !write(*,*) (Xlinres((n-1)*3+i,j),j=1,3)
                                        write(*,*) X2(i,:)
                                    end do
                                elseif (inp%order == 2) then
                                    X3 = reshape(Xtot(:,l2),[3,3,3])
                                    do i=1,3
                                        do j=1,3
                                            do o=1,3
                                                write(*,*) i,j,o, X3(i,j,o)
                                            enddo
                                        enddo
                                    end do
                                endif
                            else if (inp%linres == 'she') then
                                if (inp%order == 1) then
                                    X3 = reshape(Xtot(:,l2),[3,3,3])
                                    do i=1,3
                                        write(*,*) 'spin_component', i
                                        do j=1,3
                                            write(*,*) X3(i,j,:)
                                        end do
                                        write(*,*)
                                    end do
                                elseif (inp%order == 2) then
                                    X4 = reshape(Xtot(:,l2),[3,3,3,3])
                                    do i=1,3
                                        write(*,*) 'spin_component', i
                                        do j=1,3
                                            do o=1,3
                                                do r=1,3
                                                    write(*,*) j,o,r,X4(i,j,o,r)
                                                end do
                                            end do
                                        end do
                                    end do
                                endif
                            end if
                            write(*,*)
                        end do
                    end do
                end do
            end do
        end do
    endif

    if ( rank == 0 ) then
        write(*,*) 
        write(*,*) '*****************************'
        write(*,*) 'Input parameters'
        write(*,*) 'Calculation:', inp%linres
        write(*,*) 'Order:', inp%order
        write(*,*) 'Model:', inp%model
        write(*,*) 'nk:', inp%nk(:)
        write(*,*) 'gamma:', inp%params%gammas(:)
        write(*,*) 'Ef:', inp%params%Efs(:)
        write(*,*) 'Formulas:', inp%params%formulas(:)
        write(*,*) 'Omegas:', inp%params%omegas(:)
        write(*,*) 'Epsilon:', inp%eps
        write(*,*) 'Temperature:', inp%T
        if (inp%n_projs_inp > 0) then
            write(*,*) 'Projections:', inp%params%projs(:)
        end if
        if (inp%model == 'wann') then
            write(*,*) 'f_type:', model%f_type
        end if
    endif

    !This just dumps the Xtot array without any formatting (though it is a formatted file in the fortran
    !terminology)
    if (rank == 0 ) then
        write(*,*) 
        write(*,*) '*****************************'
        write(*,*) 'Unformatted output:'

        write(*,*) 'rank: '
        X_rank = get_tensor_rank(inp)
        write(*,*) X_rank

        write(*,*) 'Parameter ranges:'

        write(*,*) 'Formula: '
        do i=1,inp%params%n_formulas
            write(*,*) inp%params%formulas(i)
        end do

        write(*,*) 'Gamma: '
        do i=1,inp%params%n_gammas
            write(*,*) inp%params%gammas(i)
        end do

        write(*,*) 'Fermi: '
        do i=1,inp%params%n_Efs
            write(*,*) inp%params%Efs(i)
        end do

        write(*,*) 'Projection: '
        do i=1,inp%params%n_projs
            write(*,*) inp%params%projs(i)
        end do

        write(*,*) 'Omega: '
        do i=1,inp%params%n_omegas
            write(*,*) inp%params%omegas(i)
        end do

        do l=1,inp%n_params
            par = inp%params%get(l)
            write(*,*) 'Param: ', par(1),par(3),par(2),par(4),par(5)
            do i=1,inp%n_X
                write(*,*) Xtot(i,l)
            end do
        end do
    end if


    contains 

        !this is a wrapper for the response_k function so that this function can be sent to k_integrate
        function func(k,n)
            real(dp), intent(in) :: k(3)
            integer, intent(in) :: n
            real(dp) :: func(n)

            real(dp), allocatable :: Xo(:,:)

            call response_k(k,model,inp,Xo,times=times)
            func = reshape(Xo,[inp%n_X*inp%n_params])

        end function

end subroutine

end module
