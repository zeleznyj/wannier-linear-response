
#This read the centers of wannier orbitals in the final state
with open('wannier90.wout') as f:
    wout = f.readlines()

for i in range(len(wout)):
    if 'Final State' in wout[i]:
        final_start = i

pos = []
i = final_start+1
while True:
    if 'Sum of centres and spreads' in wout[i]:
        break
    pos.append(wout[i].split()[6:9])
    i += 1

nwan = i - final_start-1

for p in pos:
    p[0] = p[0].replace(',','')
    p[1] = p[1].replace(',','')
    for i in range(3):
        p[i] = float(p[i])

with open("xmat_wann.dat") as infile:
    even = False
    i = 0
    for line in infile:
        i += 1
        if i <1e8:
            if not even:
                ls = line.split()
                even = True
            else:
                val = line.split(',')
                val[0] = val[0][2:]
                val[1] = val[1][:-2]
                v = float(val[0])+1j*float(val[1])
                even = False
                if abs(v) > 1e-1:
                    print ls, v
                    if int(ls[1]) == 380 and ls[2] == ls[3]:
                        print pos[int(ls[2])-1][int(ls[0])-1], abs(pos[int(ls[2])-1][int(ls[0])-1]-v)
                    #if int(ls[1]) == 365:
                        #print pos[int(ls[2])-1][int(ls[0])-1],pos[int(ls[3])-1][int(ls[0])-1]

