"""
This is used for reading the output from the calculation in a nympy format.

Can be used interactively in ipython, allows for easy analysis and plotting...

The main subroutine is read_result, which reads the results and stores it as 
a class result.

Examples:

    res = read_result()

    To get a tensor with gamma=0.001, formula number 2, projection 1 and nk=100**3 do:

        res.get_result((0.001,2,1,100**3))

    For a spin-Hall effect there are no projections so do:

        res.get_result((0.001,2,100**3))
"""
import numpy as np
import xarray as xr
import itertools
from collections import OrderedDict

def read_result(dire='.',fil='linres_out'):
    dire + '/' + fil
    with open(dire + '/' + fil) as f:
        lines = f.readlines()

    for i,line in enumerate(lines):
        if 'Version' in line:
            version = line.split(':')[1].strip()
            if len(version.split('.')) != 3:
                version = lines[i+1].strip()


    vs = version.split('.')
    for i,v in enumerate(vs):
        vs[i] = int(v)
    if version == '0.1.0':
        return result_0_1_0(dire,fil)
    elif vs[0] == 0 and vs[1] == 2 and vs[2] == 0:
        return result_0_2_0(dire,fil)
    elif vs[0] == 0 and vs[1] >= 2 and vs[1] <= 3:
        return result_0_3(dire,fil)
    elif vs[0] == 0 and vs[1] >= 2 and vs[1] <= 4 and vs[2] <= 12:
        res = result(dire,fil)
        return res.get_xarray()
    else:
        return read_new(dire=dire,fil=fil)

def read_new(content=None,dire='.',fil='linres_out',empty_projs=False):
    if content is not None:
        lines = content
    else:
        with open(dire + '/' + fil) as f:
            lines = f.readlines()

    params = OrderedDict()
    start = False
    start_pranges = False
    start_X = False
    popped = []
    for i,line in enumerate(lines):
        if 'Unformatted output' in line:
            start = True
            continue
        if start:
            if 'rank' in line:
                rank = int(lines[i+1])
            if 'Parameter ranges' in line:
                start_pranges = True
                continue
            if start_pranges:
                if ':' in line:
                    if 'Param:' in line:
                        start_pranges = False
                        start_X = True
                        if len(params['Projection']) == 1:
                            if not empty_projs:
                                for i,p in enumerate(params):
                                    if p == 'Projection':
                                        popped.append(i)
                                params.pop('Projection')
                        n_params = len(params)
                        param_lens = [len(params[param]) for param in params]
                        param_lens += [3]*rank
                        X = np.zeros(param_lens)
                    else:
                        params[line.strip().rstrip(':')] = []
                else:
                    ls = line.strip()
                    try:
                        val = int(ls)
                    except:
                        val = float(ls)
                    params[next(reversed(params))].append(val)
            if start_X:
                if 'Param:' in line:
                    X_p = np.zeros(3**rank)
                    ind = line.strip().lstrip('Param:').split()
                    ind = [int(i)-1 for i in ind]
                    for i in popped:
                        ind.pop(i)
                    ind2 = 0
                else:
                    try:
                        X_p[ind2] = float(line.strip())
                    except:
                        break
                    if ind2 == 3**rank - 1: 
                        ind3 = tuple(ind)+(Ellipsis,)
                        X[ind3] = np.reshape(X_p,(3,)*rank,order='F')
                    ind2 += 1

    coords = params.values()+[range(3)]*rank
    dims = params.keys() + ['dim'+str(i+1) for i in range(rank)]
    Xa = xr.DataArray(X,coords=coords,dims=dims)

    return Xa

def read_k_resolved(path):
 
    spec_path = path.replace('.out','.spec')
    with open(spec_path) as f:
        lines = f.readlines()

    mode = None
    for line in lines:
        if 'Mode:' in line:
            mode = line.split()[1]
        if 'Version' in line:
            version = line.split(':')[1].strip()
            if len(version.split('.')) != 3:
                version = lines[i+1].strip()
        if 'Shape:' in line:
            shape = line.split()[1:]
            shape = [int(x) for x in shape]
        if 'Tensor rank:' in line:
            rank = int(line.split(':')[1])
        if 'k-dimension:' in line:
            kdim = int(line.split(':')[1])

    if mode is None:
        mode = 'k_resolved'

    if mode == 'k_resolved':
        shape2 = list(shape)
        shape2[-2:-2] = [3]*rank
        shape2.pop(-2)

    if version == '0.4.15':
        X = np.fromfile(path,np.float64)
        Xr = np.reshape(X,shape2,order='F')
    else:
        X = np.fromfile(path,np.float64)
        if mode == 'k_resolved':
            Xr1 = np.reshape(X,shape)
            Xr = np.reshape(Xr1,shape2,order='F')
        elif mode == 'bands':
            Xr = np.reshape(X,shape)
        else:
            raise Exception('Unknown mode')

    ks =['k1','k2','k3']
    ks = ks[0:kdim]

    if mode == 'k_resolved':
        dims = ks + ['dim'+str(i+1) for i in range(rank)] + ['param']
        coords = []
        for i in range(len(shape2)):
            coords.append((dims[i],range(shape2[i])))
    elif mode == 'bands':
        dims = ks + ['orb']
        coords = []
        for i in range(len(shape)):
            coords.append((dims[i],range(shape[i])))

    Xa = xr.DataArray(Xr,dims=dims,coords=coords)
    return Xa

class result:
    def __init__(self,dire='.',fil='linres_out'):
        """
        Reads the result.

        Args:
            dire(string): the directory where the result is located
        """

        self.dat = {}
        self.pars = []
        self.npars = 3

        self.file = dire + '/' + fil

        self.output = self.read_output()

        self.full_output = self.read_full_output()

        self.gams,self.fors,self.Efs,self.projs = self.find_pars()
        
        if len(self.projs) > 0:
            self.projections = True
            self.npars = 4
        else:
            self.projections = False
            self.nparts = 3

        self.calc,self.nks = self.read_input()

        if self.calc == 'cond' or self.calc == 'cisp':
            self.rank = 2
        elif self.calc == 'she' or self.calc == 'cond2':
            self.rank = 3
        else:
            raise Exception('Unknown calc')

        for i,gam in enumerate(self.gams):
            for j,Ef in enumerate(self.Efs):
                for k,f in enumerate(self.fors):
                    if self.projections:
                        for l in range(len(self.projs)):
                            X = self.read_tensor(gam,Ef,k+1,l+1)
                            self.add_tensor((i,j,k,l),X)
                    else:
                        X = self.read_tensor(gam,Ef,k+1)
                        self.add_tensor((i,j,k),X)

    def add_tensor(self,pars,tensor):
        if  (type(pars) != tuple) or (len(pars) != self.npars):
            raise TypeError
        self.dat[pars] = tensor
        if pars in self.pars:
            print 'Tensor for this pars is already present!'
            print 'pars:', pars
        else:
            self.pars.append(pars)

    def get_tensor(self,pars):
        if pars in self.dat:
            return self.dat[pars]
        else:
            if pars[0] in self.gams:
                found_g = True
            else:
                found_g = False
            if pars[1] in self.Efs:
                found_Ef = True
            else:
                found_Ef = False
            if pars[2] in self.fors:
                found_for = True
            else:
                found_Ef = False
        if found_g and found_Ef and found_for:
            return self.dat[(self.gams.index(pars[0]),self.Efs.index(pars[1]),pars[2])]
        else:
            print 'wrong parameters'
            raise KeyError

    def read_output(self):

        with open(self.file) as f:
            lines = f.readlines()

        for i in range(len(lines)):
            if 'Unformatted output:' in lines[i]:
                start = i+1

        if len(lines[start].split()) == 4:
            lines_new = []
            odd = True
            for i in range(start,len(lines)):
                if odd:
                    lines_new.append(lines[i].strip()+ '  ' + lines[i+1].strip())
                    odd = False
                else:
                    odd = True
        elif len(lines[start].split()) == 6:
            lines_new = lines[start:-1]
        else:
            raise Exception('Wrong output format, unexpected number of items at unformatted output line.')

        for i in range(len(lines_new)):
            linc = lines_new[i].split()
             
            for j in [0,1,4]:
                linc[j] = int(linc[j])
            for j in [2,3,5]:
                linc[j] = float(linc[j])

            lines_new[i]=linc

        return lines_new

    def read_full_output(self):

        with open(self.file) as f:
            lines = f.readlines()

        return lines

    def read_input(self):

        with open(self.file) as f:
            lines = f.readlines()

        for i in range(len(lines)):
            if 'Input parameters:' in lines[i]:
                start = i+1

        for line in lines:
            if 'Calculation' in line:
                calc = line.split(':')[1].rstrip()
            if 'Order' in line:
                order = int(line.split(':')[1].rstrip())
            if 'nk:' in line:
                nks = line.split(':')[1].split()
                for nk in nks:
                    nk = int(nk)

        if calc == 'cond' and order == 2:
            calc = 'cond2'

        return calc,nks

    def find_pars(self):

        lines = self.output
        lines_full = self.full_output

        gams = []
        Efs = []

        for i in range(len(lines)):
            #I believe there should be no problems with rounding errors here
            if lines[i][2] not in gams:
                gams.append(lines[i][2])
            if lines[i][3] not in Efs:
                Efs.append(lines[i][3])

        projections = False
        for i in range(len(lines_full)):
            if 'Formulas:' in lines_full[i]:
                fors = lines_full[i].split(':')[1].split()
                for j,f in enumerate(fors):
                    fors[j] = int(fors[j])
            if 'Projections:' in lines_full[i]:
                projections = True
                projs = lines_full[i].split(':')[1].split()
                for j,proj in enumerate(projs):
                    projs[j] = int(proj)

        if not projections:
            projs = []

        return gams,fors,Efs,projs

    def read_tensor(self,gamma,Ef,nfor,proj=None):

        if self.rank == 2:
            X = np.zeros((3,3))
        elif self.rank == 3:
            X = np.zeros((3,3,3))

        if proj is not None and self.calc != 'cisp':
            raise Exception('Projections only allowed for cisp')

        lines = self.output
        found = False
        for i in range(len(lines)):
            if abs(lines[i][2]-gamma) < 1e-10 and abs(lines[i][3]-Ef) < 1e-10 and lines[i][4] == nfor:
                found = True
                if self.rank == 2:
                    if proj is not None:
                        if (lines[i][0]-1)/3 == proj-1:
                            X[lines[i][0]-(proj-1)*3-1,lines[i][1]-1] = lines[i][5]
                    else:
                        X[lines[i][0]-1,lines[i][1]-1] = lines[i][5]
                elif self.rank == 3:
                    ind1 = (lines[i][0]-1) / 3
                    ind2 = lines[i][0]-1 - ind1*3
                    X[ind1,ind2,lines[i][1]-1] = lines[i][5]
        if not found:
            raise Exception('Tensor not found')

        return X

    def get_xarray(self):
        if self.rank == 2:
            ten_dim = (3,3)
        elif self.rank == 3:
            ten_dim = (3,3,3)
        n_fors = len(self.fors)
        n_gams = len(self.gams)
        n_Efs = len(self.Efs)
        n_projs = len(self.projs)
        if self.projections:
            dims = (n_fors,n_gams,n_Efs,n_projs) + ten_dim
        else:
            dims = (n_fors,n_gams,n_Efs) + ten_dim
        X = np.zeros(dims)
        for i in range(n_gams):
            for j in range(n_Efs):
                for k in range(n_fors):
                    if self.projections:
                        for l in range(n_projs):
                            if self.rank == 2:
                                X[k,i,j,l,:,:] = self.get_tensor((i,j,k,l))
                            elif self.rank == 3:
                                X[k,i,j,l:,:,:] = self.get_tensor((i,j,k))
                    else:
                        if self.rank == 2:
                            X[k,i,j,:,:] = self.get_tensor((i,j,k))
                        elif self.rank == 3:
                            X[k,i,j,:,:,:] = self.get_tensor((i,j,k))

        ranges = [range(3)]*self.rank
        if self.projections:
            coords = [self.fors,self.gams,self.Efs,self.projs] + ranges
        else:
            coords = [self.fors,self.gams,self.Efs] + ranges
        if self.rank == 2:
            dims = ['dim1','dim2']
        elif self.rank == 3:
            dims = ['dim1','dim2','dim3']
        if self.projections:
            dims_all = ['formula','Gamma','Ef','Projections'] + dims
        else:
            dims_all = ['formula','Gamma','Ef'] + dims
        out = xr.DataArray(X,coords=coords,dims=dims_all)
        return out

class result_old:
    def __init__(self,dire='.',fil='linres_out'):
        """
        Reads the result.

        Args:
            dire(string): the directory where the result is located
        """

        self.dat = {}
        self.pars = []
        self.npars = 3

        self.file = dire + '/' + fil

        self.output = self.read_output()

        self.gams,self.fors,self.Efs = self.find_pars()

        self.calc,self.nks = self.read_input()


        for i,gam in enumerate(self.gams):
            for j,Ef in enumerate(self.Efs):
                for f in self.fors:
                    X = self.read_tensor(self.calc,gam,Ef,f)
                    self.add_tensor((i,j,f),X)

    def add_tensor(self,pars,tensor):
        if  (type(pars) != tuple) or (len(pars) != self.npars):
            raise TypeError
        self.dat[pars] = tensor
        if pars in self.pars:
            print 'Tensor for this pars is already present!'
            print 'pars:', pars
        else:
            self.pars.append(pars)

    def get_tensor(self,pars):
        if pars in self.dat:
            return self.dat[pars]
        else:
            if pars[0] in self.gams:
                found_g = True
            else:
                found_g = False
            if pars[1] in self.Efs:
                found_Ef = True
            else:
                found_Ef = False
            if pars[2] in self.fors:
                found_for = True
            else:
                found_Ef = False
        if found_g and found_Ef and found_for:
            return self.dat[(self.gams.index(pars[0]),self.Efs.index(pars[1]),pars[2])]
        else:
            print 'wrong parameters'
            raise KeyError

    def read_output(self):

        with open(self.file) as f:
            lines = f.readlines()

        for i in range(len(lines)):
            if 'Unformatted output:' in lines[i]:
                start = i+1

        if len(lines[start].split()) < 6:
            lines2 = []
            for i in range(start,len(lines)):
                if (i-start) % 2 == 0:
                    lines2.append(lines[i]) 
                else:
                    lines2[-1] = lines2[-1].split() + lines[i].split()
        else:
            lines2 = []
            for i in range(start,len(lines)):
                lines2.append(lines[start:-1].split())

        for i in range(len(lines2)):
            linc = lines2[i]
            
             
            for j in [0,1,4]:
                linc[j] = int(linc[j])
            for j in [2,3,5]:
                linc[j] = float(linc[j])

            lines2[i]=linc

        return lines2

    def read_input(self):

        with open(self.file) as f:
            lines = f.readlines()

        for i in range(len(lines)):
            if 'Input parameters' in lines[i]:
                start = i+1

        for i in range(start,len(lines)):
            line = lines[i]
            if 'Calculation' in line:
                calc = line.split(':')[1].rstrip()
            if 'nk' in line:
                nks = line.split(':')[1].split()
                for nk in nks:
                    print nk
                    nk = int(nk)

        return calc,nks

    def find_pars(self):

        lines = self.output

        gams = []
        fors = []
        Efs = []

        for i in range(len(lines)):
            #I believe there should be no problems with rounding errors here
            if lines[i][2] not in gams:
                gams.append(lines[i][2])
            if lines[i][3] not in Efs:
                Efs.append(lines[i][3])
            if lines[i][4] not in fors:
                fors.append(lines[i][4])


        return gams,fors,Efs

    def read_tensor(self,calc,gamma,Ef,nfor,dire='.'):

        if calc == 'cisp' or calc == 'cond':
            X = np.zeros((3,3))
        if calc == 'she' or calc == 'cond2':
            X = np.zeros((3,3,3))

        lines = self.output
        for i in range(len(lines)):
            if abs(lines[i][2]-gamma) < 1e-10 and abs(lines[i][3]-Ef) < 1e-10 and lines[i][4] == nfor:
                if calc == 'cisp' or calc == 'cond':
                    X[lines[i][0]-1,lines[i][1]-1] = lines[i][5]
                if calc == 'she':
                    ind1 = (lines[i][0]-1) / 3
                    ind2 = lines[i][0]-1 - ind1*3
                    X[ind1,ind2,lines[i][1]-1] = lines[i][5]

        return X

class result_0_3:
    def __init__(self,dire='.',fil='linres_out'):
        """
        Reads the result.

        Args:
            dire(string): the directory where the result is located
        """

        self.dat = {}
        self.pars = []
        self.npars = 3

        self.file = dire + '/' + fil

        self.output = self.read_output()

        self.gams,self.fors,self.Efs = self.find_pars()

        self.calc,self.nks = self.read_input()


        for i,gam in enumerate(self.gams):
            for j,Ef in enumerate(self.Efs):
                for f in self.fors:
                    X = self.read_tensor(self.calc,gam,Ef,f)
                    self.add_tensor((i,j,f),X)

    def add_tensor(self,pars,tensor):
        if  (type(pars) != tuple) or (len(pars) != self.npars):
            raise TypeError
        self.dat[pars] = tensor
        if pars in self.pars:
            print 'Tensor for this pars is already present!'
            print 'pars:', pars
        else:
            self.pars.append(pars)

    def get_tensor(self,pars):
        if pars in self.dat:
            return self.dat[pars]
        else:
            if pars[0] in self.gams:
                found_g = True
            else:
                found_g = False
            if pars[1] in self.Efs:
                found_Ef = True
            else:
                found_Ef = False
            if pars[2] in self.fors:
                found_for = True
            else:
                found_Ef = False
        if found_g and found_Ef and found_for:
            return self.dat[(self.gams.index(pars[0]),self.Efs.index(pars[1]),pars[2])]
        else:
            print 'wrong parameters'
            raise KeyError

    def read_output(self):

        with open(self.file) as f:
            lines = f.readlines()

        for i in range(len(lines)):
            if 'Unformatted output:' in lines[i]:
                start = i+1

        for i in range(start,len(lines)):
            linc = lines[i].split()
             
            for j in [0,1,4]:
                linc[j] = int(linc[j])
            for j in [2,3,5]:
                linc[j] = float(linc[j])

            lines[i]=linc

        return lines[start:]

    def read_input(self):

        with open(self.file) as f:
            lines = f.readlines()

        for i in range(len(lines)):
            if 'Input parameters:' in lines[i]:
                start = i+1

        for line in lines:
            if 'Calculation' in line:
                calc = line.split(':')[1].rstrip()
            if 'nk:' in line:
                nks = line.split(':')[1].split()
                for nk in nks:
                    nk = int(nk)

        return calc,nks

    def find_pars(self):

        lines = self.output

        gams = []
        fors = []
        Efs = []

        for i in range(len(lines)):
            #I believe there should be no problems with rounding errors here
            if lines[i][2] not in gams:
                gams.append(lines[i][2])
            if lines[i][3] not in Efs:
                Efs.append(lines[i][3])
            if lines[i][4] not in fors:
                fors.append(lines[i][4])


        return gams,fors,Efs

    def read_tensor(self,calc,gamma,Ef,nfor,dire='.'):

        if calc == 'cisp' or calc == 'cond':
            X = np.zeros((3,3))
        if calc == 'she':
            X = np.zeros((3,3,3))

        lines = self.output
        for i in range(len(lines)):
            if abs(lines[i][2]-gamma) < 1e-10 and abs(lines[i][3]-Ef) < 1e-10 and lines[i][4] == nfor:
                if calc == 'cisp' or calc == 'cond':
                    X[lines[i][0]-1,lines[i][1]-1] = lines[i][5]
                if calc == 'she':
                    ind1 = (lines[i][0]-1) / 3
                    ind2 = lines[i][0]-1 - ind1*3
                    X[ind1,ind2,lines[i][1]-1] = lines[i][5]

        return X

class result_0_1_0:
    def __init__(self,dire='.',fil='linres_out'):
        """
        Reads the result.

        Args:
            dire(string): the directory where the result is located
        """

        self.dat = {}
        self.pars = []
        self.npars = 2

        self.file = dire + '/' + fil

        self.output = self.read_output()

        self.gams,self.fors = self.find_pars()

        self.calc,self.nks,self.Ef = self.read_input()


        for gam in self.gams:
            for f in self.fors:
                X = self.read_tensor(self.calc,gam,f)
                self.add_tensor((gam,f),X)

    def add_tensor(self,pars,tensor):
        if  (type(pars) != tuple) or (len(pars) != self.npars):
            raise TypeError
        self.dat[pars] = tensor
        if pars in self.pars:
            print 'Tensor for this pars is already present!'
        else:
            self.pars.append(pars)

    def get_tensor(self,pars):
        return self.dat[pars]

    def read_output(self):

        with open(self.file) as f:
            lines = f.readlines()

        for i in range(len(lines)):
            if 'Unformatted output:' in lines[i]:
                start = i+1

        for i in range(start,len(lines)):
            linc = lines[i].split()
             
            for j in [0,1,3]:
                linc[j] = int(linc[j])
            for j in [2,4]:
                linc[j] = float(linc[j])

            lines[i]=linc

        return lines[start:]

    def read_input(self):

        with open(self.file) as f:
            lines = f.readlines()

        for i in range(len(lines)):
            if 'Input parameters:' in lines[i]:
                start = i+1

        for line in lines:
            if 'Calculation' in line:
                calc = line.split(':')[1].rstrip()
            if 'nk' in line:
                nks = line.split(':')[1].split()
                for nk in nks:
                    nk = int(nk)
            if 'Ef' in line:
                Ef = float(line.split(':')[1])

        return calc,nks,Ef

    def find_pars(self):

        lines = self.output

        gams = []
        fors = []

        for i in range(len(lines)):
            #I believe there should be no problems with rounding errors here
            if lines[i][2] not in gams:
                gams.append(lines[i][2])
            if lines[i][3] not in fors:
                fors.append(lines[i][3])

        return gams,fors

    def read_tensor(self,calc,gamma,nfor,dire='.'):

        if calc == 'cisp' or calc == 'cond':
            X = np.zeros((3,3))
        if calc == 'she':
            X = np.zeros((3,3,3))

        lines = self.output
        for i in range(len(lines)):
            if abs(lines[i][2]-gamma) < 1e-10 and lines[i][3] == nfor:
                if calc == 'cisp' or calc == 'cond':
                    X[lines[i][0]-1,lines[i][1]-1] = lines[i][4]
                if calc == 'she':
                    ind1 = (lines[i][0]-1) / 3
                    ind2 = lines[i][0]-1 - ind1*3
                    X[ind1,ind2,lines[i][1]-1] = lines[i][4]

        return X

def convert_tensor_3op(ten,T):
    """
    Converts a tensor, which describes a 3 operator linear response.
    Args:
        tensor: The tensor to be transformed.
        T (matrix): Coordinate transformation matrix. If it is set, the symmetry operations will be transformed by this matrix.
            Symmetry operations are given in basis A. T transforms from A to B, ie Tx_A = x_B.
    Returns:
        ten_T: The transformed tensor.
    """


    mat1 = T
    mat2 = T.inv().T
    ten_T = np.zeros((3,3,3))
 
    for ind1 in itertools.product(range(3),range(3),range(3)):
        for ind2 in itertools.product(range(3),range(3),range(3)):
            factor = 1
            for i in range(3):
                if i == 0:
                    factor *= mat1[ind1[i],ind2[i]]
                elif i == 1:
                    factor *= mat1[ind1[i],ind2[i]]
                else:
                    factor *= mat2[ind1[i],ind2[i]]
            print ind1,ind2,factor
            ten_T[ind1] += factor*ten[ind2]
    
    return ten_T
