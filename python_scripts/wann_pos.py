"""
Creates a file which defines on which atoms are the wannier orbitals centered

Takes as an input the wannier90.wout file

Produces some debug output and wann_projs file, which is then read by the fortran code
"""

import argparse
import numpy as np
import json
from collections import OrderedDict

parser = argparse.ArgumentParser()
parser.add_argument('--print-pos',action='store_true')
parser.add_argument('--print-projs',action='store_true')
parser.add_argument('--input-file',default=None)
parser.add_argument('--output-file',default=None)
parser.add_argument('wann_prefix',type=str)
args = parser.parse_args()

print_pos = args.print_pos
print_projs = args.print_projs
wann_prefix = args.wann_prefix
input_file = args.input_file
output_file = args.output_file
if input_file is not None and output_file is not None:
    raise Exception('You cannot provide both input_file and output_file')

with open(wann_prefix+'.wout') as f:
    wout = f.readlines()

for i in range(len(wout)):
    if 'Cartesian Coordinate' in wout[i]:
        struct_start = i
        break

#This read the atomic positions
atoms = []
i = struct_start+2
while True:
    if '---------------------' in wout[i]:
        break
    atoms.append([float(wout[i].split()[j]) for j in range(7,10)])
    i += 1
natoms = i-struct_start-2
atoms = np.array(atoms)

for i in range(len(wout)):
    if 'Final State' in wout[i]:
        final_start = i

#This read the centers of wannier orbitals in the final state
pos = []
i = final_start+1
while True:
    if 'Sum of centres and spreads' in wout[i]:
        break
    pos.append(wout[i].split()[6:9])
    i += 1


nwan = i - final_start-1

for p in pos:
    p[0] = p[0].replace(',','')
    p[1] = p[1].replace(',','')
    for i in range(3):
        p[i] = float(p[i])

if print_pos:

    if input_file is not None:
        with open(input_file,'r') as f:
            input_json = json.load(f,object_pairs_hook=OrderedDict)
        with open(input_file,'w') as f:
            input_json['wann']['centers'] = pos
            json.dump(input_json,f,indent=4)
    else:
        if output_file is None:
            output_file = 'wann_pos'
        with open(output_file,'w') as f:
            f.write('[',)
            for i in range(len(pos)):
                if i != len(pos)-1:
                    f.write(str(pos[i])+',\n')
                else:
                    f.write(str(pos[i])+']')





if print_projs:
    pos = np.array(pos)

    #for a wannier orbital it finds the nearest atom
    #returns(number of the atom, distance)
    def find_closest(pos,atoms):
        for i in range(natoms):
            dist = np.linalg.norm(pos-atoms[i])
            if i == 0:
                minr = (0,dist)
            if dist < minr[1]:
                minr = (i,dist)
        return minr

    #for a number of atom, it finds the distance to nearest neighbour
    def find_nn(iat,atoms):
        #looking for nearest neighbours is only implemented in the first unit cell,
        #so it doesn't work when there is only one atom
        #this is only for informatin purposes so it doesnt really matter
        if len(atoms) == 1:
            return (0,-1)
        for i in range(natoms):
            dist = np.linalg.norm(atoms[iat]-atoms[i])
            if i == iat:
                continue
            if i == 0 or (iat == 0 and i == 1):
                minr = (0,dist)
            if dist < minr[1]:
                minr = (i,dist)
        return minr

    #finds how many orbitals are on each atom
    counts = np.zeros((natoms))
    for p in pos:
        for i in range(natoms):
            if find_closest(p,atoms)[0] == i:
                counts[i] += 1

    #finds average and maximum distances of wann orbitals for each atom
    projs = [[counts[j]] for j in range(natoms)]
    dists = [[0,float(0)] for j in range(natoms)]
    for i in range(len(pos)):
        minr = find_closest(pos[i],atoms)
        projs[minr[0]].append(i+1)
        dists[minr[0]][0] += minr[1]/counts[minr[0]]
        if minr[1] > dists[minr[0]][1]:
            dists[minr[0]][1] = minr[1]

    print('Atomic positions:')
    at = 0
    for atom in atoms:
        at += 1
        print(at, " ".join("%9.5f" % f for f in atom))
    print('')

    print('Nearest neighbour distance for each atom:')
    at = 0
    for i in range(natoms):
        at += 1
        print(at, "%9.5f" % find_nn(i,atoms)[1])
    print('')

    print('Average distance of orbitals from the atom center and the maximum distance:')
    at = 0
    for dist in dists:
        at = at+1
        print(at, " ".join("%10.6f" % f for f in dist))
    print('')

    print('Number of orbitals for each atom and orbital numbers:')
    at = 0
    for proj in projs:
        at = at+1
        print(at, proj[0], proj[1:])

    maxnorb = int(max(counts))
    #creates the output file
    with open('input_projs','w') as f:
        f.write(str(natoms)+'\n')
        for proj in projs:
            f.write(str(int(proj[0]))+'\n')
            for p in range(1,len(proj)):
                f.write(str(proj[p])+' ')
            for i in range(len(proj),maxnorb+1):
                f.write('0 ')
            f.write('\n')

    if input_file is not None:
        with open(input_file,'r') as f:
            input_json = json.load(f,object_pairs_hook=OrderedDict)
        with open(input_file,'w') as f:
            for proj in projs:
                proj.pop(0)
            input_json['def_projections'] = projs
            json.dump(input_json,f,indent=4)
    else:
        if output_file is None:
            output_file = 'input_projs'
        with open(output_file,'w') as f:
            f.write('[',)
            for i in range(len(projs)):
                if i != len(projs)-1:
                    f.write(str(projs[i][1:])+',\n')
                else:
                    f.write(str(projs[i][1:])+']')



