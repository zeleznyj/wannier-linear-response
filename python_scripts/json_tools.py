import json
import re
import collections

def array_rank(array,current_rank=None):
    if current_rank is None:
        current_rank = 0
    all_list = True
    all_number = True
    for a in array:
        if not (isinstance(a,int) or isinstance(a,float)):
            all_number = False
        if not isinstance(a,list):
            all_list = False
    if not (all_list or all_number):
        return None
    if all_number:
        rank = current_rank + 1
        return rank
    if all_list:
        current_rank += 1
        ranks = []
        for a in array:
            ranks.append(array_rank(a,current_rank))
        all_list = True
        all_same_numbers = True
        if isinstance(ranks[0],int):
            for rank in ranks:
                if not isinstance(rank,int):
                    all_same_numbers = False
                elif rank != ranks[0]:
                    all_same_numbers = False
        for rank in ranks:
            if not isinstance(rank,list):
                all_list = False
        if not (all_same_numbers or all_list):
            well_formed = False
            return None
        if all_same_numbers:
            return ranks[0]

def find_arrays(inp,found_arrays=None):
    if found_arrays is None:
        found_arrays = []
    for key in inp:
        if isinstance(inp[key],list):
            found_arrays.append((key,inp[key]))
        elif isinstance(inp[key],dict):
            find_arrays(inp[key],found_arrays)
    return found_arrays

def join_array1(array):
    string = ''.join(array)
    string_split = string.split(':')
    string_split[1] = re.sub( '\s+', ' ', string_split[1]).strip()
    string = ': '.join(string_split)
    return string

def join_array2(array):
    for i,a in enumerate(array):
        if i > 0:
            array[i] = re.sub( '\s+', ' ', a)
    string = ''.join(array)
    return string

def reformat_arrays(inp,indent=2):
    """
    Reformats arrays in a json object so that the output strings is nicer.
    """

    inp_str = json.dumps(inp,indent=indent).splitlines()
    found_arrays =  find_arrays(inp)

    for (key,array) in found_arrays:
        i = 0
        found_key = False
        rank = array_rank(array)
        if rank == 1:
            while not found_key: 
                line = inp_str[i]
                if key in line:
                    found_key = True
                    found = False
                    j = 0
                    while not found:
                        if ']' in inp_str[i+j]:
                            dim = j
                            found = True
                        else:
                            j = j+1
                    #inp_str[i:i+j+1] = [''.join(inp_str[i:i+j+1])]
                    inp_str[i:i+j+1] = [join_array1(inp_str[i:i+j+1])]
                else:
                    i += 1

        elif rank == 2:
            end1 = False
            n_done = 0
            while not end1:
                i = 0
                found_key = False
                end2 = False
                while not found_key: 
                    line = inp_str[i]
                    if key in line:
                        found_key = True
                        j_start = i+n_done+1
                        j = j_start
                        if ']' in inp_str[j]:
                            end1 = True
                            break
                        else:
                            j +=1
                            while not end2:
                                if ']' in inp_str[j]:
                                    j_end = j
                                    end2 = True
                                else:
                                    j += 1
                            inp_str[j_start:j_end+1] = [join_array2(inp_str[j_start:j_end+1])]
                            n_done += 1
                            break
                    else:
                        i += 1

    return "\n".join(inp_str)        

def json_cp(key,origin,dest):
    try:
        with open(origin) as f:
            origin_json = json.load(f,object_pairs_hook=collections.OrderedDict)
    except:
        raise Exception('Problem loading origin file')

    try:
        with open(dest) as f:
            dest_json = json.load(f,object_pairs_hook=collections.OrderedDict)
    except:
        dest_json = {}

    dest_json2 = dest_json 
    if '.' in key:
        keys = key.split('.')
        val = origin_json
        for k in keys:
            val = val[k]
        for k in keys[:-1]:
            dest_json = dest_json.setdefault(k, {})
        dest_json[keys[-1]] = val

    else:
        dest_json[key] = origin_json[key]
    dest_string = reformat_arrays(dest_json2)

    with open(dest,'w') as f:
        f.write(dest_string)

def json_modify(loc,key,val):
    try:
        with open(loc) as f:
            loc_json = json.load(f,object_pairs_hook=collections.OrderedDict)
    except:
        raise Exception('Problem loading origin file')

    if '.' in key:
        keys = key.split('.')
        loc_json2 = loc_json
        for k in keys[:-1]:
            loc_json = loc_json.setdefault(k, {})
        loc_json[keys[-1]] = val

    else:
        loc_json[key] = val

    loc_string = reformat_arrays(loc_json2)

    with open(loc,'w') as f:
        f.write(loc_string)
