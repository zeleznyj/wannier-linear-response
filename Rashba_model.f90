module Rashba_model_mod
!--------------------------------------------------------------------------
!This module reads a tight-binding Hamiltonian produced by python sd_model.py
!script and provides routines for linear response calculation. The routines
!are similar to the wann_model and have the same output but work a little 
!differently.
!--------------------------------------------------------------------------

use generic_model
use constants
use input
use blas95
use json_module
use k_integrate, only: find_recvecs

implicit none

type, extends (gen_model) :: Rashba_model

    type(projs_type) :: projs
    logical :: projs_defined

    !Model parameters
    real(dp) :: J_sd
    real(dp), allocatable :: M(:)
    real(dp) :: t
    real(dp) :: a_R, a_D
    
    !Pauli matrices
    complex(dp) :: sigma_0(2,2),sigma_x(2,2),sigma_y(2,2),sigma_z(2,2)
    complex(dp) :: smat(2,2,3)

    !other
    real(dp) :: k_shift(2)
    logical :: shift_k

    contains

        !generic subroutines
        procedure :: init
        procedure :: create_smat
        procedure :: create_Hk
        procedure :: create_vk
        procedure :: create_vk_dk
        procedure :: def_projs

        !internal subroutines
        procedure :: read_Rashba_input
        procedure :: create_S
        procedure :: get_k_shift

end type

contains

subroutine read_Rashba_input(self)
    class(Rashba_model) :: self
    type(json_file) :: json
    logical :: found

    json = parse_input()

    call json%get('model.Rashba.t',self%t,found)
    if (.not. found) then
        stop 'model.Rashba.t must be specified'
    endif

    call json%get('model.Rashba.J_sd',self%J_sd,found)
    if (.not. found) then
        stop 'model.Rashba.J_sd must be specified'
    endif

    call json%get('model.Rashba.M',self%M,found)
    if (.not. found) then
        stop 'model.Rashba.M must be specified'
    endif
    self%M = self%m/norm2(self%m)

    call json%get('model.Rashba.a_R',self%a_R,found)
    if (.not. found) then
        stop 'model.Rashba.a_R must be specified'
    endif

    call json%get('model.Rashba.a_D',self%a_D,found)
    if (.not. found) then
        stop 'model.Rashba.a_R must be specified'
    endif

    call json%get('model.Rashba.shift_k',self%shift_k,found)
    if (.not. found) then
        self%shift_k = .true.
    endif

end subroutine


subroutine init(self)
    !--------------------------------------------------------------------------
    !Reads the hamiltonian matrix elements in the tight-binding basis outputted by
    !sd_model.py.
    !
    !Returns (module variables):
    !   n_orb (integer): number of basis functions
    !   n_Hrs (integer): number of Hamiltonian matrix elements
    !   rhos (real(3,n_orb)): rhos that appear in the exponential for each matrix element
    !       rho is given in fractional coordinates and is defined as:
    !   orbs (integer(3,n_orb): the orbitals of each matrix element
    !   Hr (complex(:)): the Hamiltonian matrix elements stored as:
    !       Hr(i) = < orbs(1,i)|Hr|orbs(2,i) >
    !--------------------------------------------------------------------------

    class(Rashba_model) :: self

    self%n_orb = 2

    call self%read_Rashba_input()
    
    self%sigma_0 = 0_dp
    self%sigma_x = 0_dp
    self%sigma_y = 0_dp
    self%sigma_z = 0_dp

    self%sigma_0(1,1) = 1._dp
    self%sigma_0(2,2) = 1._dp

    self%sigma_x(1,2) = 1._dp
    self%sigma_x(2,1) = 1._dp

    self%sigma_y(1,2) = -ii
    self%sigma_y(2,1) = ii

    self%sigma_z(1,1) = 1._dp
    self%sigma_z(2,2) = -1._dp
    
    self%smat(:,:,1) = self%sigma_x
    self%smat(:,:,2) = self%sigma_y
    self%smat(:,:,3) = self%sigma_z

    if (self%shift_k) then
        self%k_shift = self%get_k_shift()
    else
        self%k_shift = [0.0_dp,0.0_dp]
    endif
    write(*,*) 'Rashba model: Shifting k by: ', self%k_shift
    write(*,*) ''


end subroutine

subroutine create_S(self,k,S)
    class(Rashba_model) :: self
    real(dp), intent(in) :: k(2)
    real(dp), intent(out) :: S(3)

    S(1) = -self%J_sd * self%M(1) - self%a_R * k(2) - self%a_D * k(1)
    S(2) = -self%J_sd * self%M(2) + self%a_R * k(1) + self%a_D * k(2)
    S(3) = -self%J_Sd * self%M(3)

end subroutine

subroutine create_Hk(self,k,Hk)
    !--------------------------------------------------------------------------
    !--------------------------------------------------------------------------

    class(Rashba_model) :: self
    real(dp), intent(in) :: k(3)
    complex(dp), allocatable, intent(out) :: Hk(:,:)

    real(dp) :: ks(2)
    integer :: n_wann
    integer :: i,n,m
    real(dp) :: r(3)
    real(dp) :: S(3)

    ks = k(1:2) + self%k_shift
    allocate(Hk(2,2))
    Hk(:,:) = 0_dp

    call self%create_S(ks,S)

    Hk = Hk + self%t * (ks(1)**2 + ks(2)**2) * self%sigma_0
    Hk = Hk + S(1) * self%sigma_x
    Hk = Hk + S(2) * self%sigma_y
    Hk = Hk + S(3) * self%sigma_z

end subroutine

subroutine create_vk(self,k,vk,ind)
    !--------------------------------------------------------------------------
    !--------------------------------------------------------------------------

    class(Rashba_model) :: self
    real(dp), intent(in) :: k(3)
    complex(dp), allocatable, intent(out) :: vk(:,:,:)
    integer, intent(in), optional :: ind

    real(dp) :: ks(2)

    ks = k(1:2) + self%k_shift

    allocate(vk(2,2,3))
    vk(:,:,:) = 0_dp

    vk(:,:,1) = 2 * self%t * ks(1) * self%sigma_0 - self%a_D * self%sigma_x + self%a_R * self%sigma_y
    vk(:,:,2) = 2 * self%t * ks(2) * self%sigma_0 - self%a_R * self%sigma_x + self%a_D * self%sigma_y
    
    vk = vk / hbar

end subroutine

subroutine create_smat(self,k,smat,atom)
    !--------------------------------------------------------------------------
    !Defines the matrix of the spin-operator
    !
    !Notes: 
    !   Assumes that first half othe basis functions are spin-up and the other half
    !   are spin-down.
    !   dimensionless, i.e. S=sigma. Divide by 2 to get spin in the units of hbar
    !
    !Args:
    !   n_wann: magnitude of the Hamiltonian
    !   atom(optional): number of atom on which the spin-operator is projected
    !       if atom is not present, no projection is done
    !   projs(optional): must be present if atom is present. defines how the 
    !       wannier orbitals are localized, which defines how the projections are
    !       done. This is outputed by read_projs
    !
    !Returns:
    !   smat: all three components of the Hamiltonian
    !--------------------------------------------------------------------------
    class(Rashba_model) :: self
    real(dp), intent(in) :: k(3)
    complex(dp), allocatable, intent(out) :: smat(:,:,:)
    integer, optional, intent(in) :: atom

    if ( present(atom) ) then
        stop 'Projections not possible for the Rashba model'
    end if

    allocate(smat(2,2,3))
    smat = self%smat

end subroutine

subroutine create_vk_dk(self,k,vk_dk)

    class(Rashba_model) :: self
    real(dp), intent(in) :: k(3)
    complex(dp), allocatable, intent(out) :: vk_dk(:,:,:,:)

    real(dp) :: ks(2)

    ks = k(1:2) + self%k_shift

    allocate(vk_dk(2,2,2,3))
    vk_dk(:,:,:,:) = 0_dp

    vk_dk(:,:,1,1) = 2 * self%t
    vk_dk(:,:,2,2) = 2 * self%t
    
    vk_dk = vk_dk / hbar

end subroutine

subroutine def_projs(self,projs)
    class(Rashba_model) :: self
    type(projs_type), intent(in) :: projs

    stop 'Projections not possible for the Rashba model'
 
end subroutine

function get_k_shift(self) result(k_shift)
    class(Rashba_model) :: self
    real(dp) :: k_shift(2)

    real(dp) :: shift(3)
    real(dp) :: latt_vecs(3,3)
    real(dp) :: rec_vecs(3,3)
    real(dp) :: rec_vol

    call self%get_latt_vecs(latt_vecs)
    call find_recvecs(latt_vecs,rec_vecs,rec_vol)
    
    shift = -rec_vecs(:,1)/2 - rec_vecs(:,2)/2
    if ( abs(shift(3)) > 1e-14) stop 'shift(3) not equal to zero, something is wrong'
    k_shift = shift(1:2)

end function





end module

