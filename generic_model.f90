module generic_model
use input, only: read_structure

integer, parameter,private :: dp=kind(0.d0)

type, abstract :: gen_model
    integer :: n_orb
    integer :: f_type
    contains
        procedure(init_Int), deferred :: init
        procedure(create_smat_Int), deferred :: create_smat
        procedure(create_Hk_Int), deferred :: create_Hk
        procedure(create_vk_Int), deferred :: create_vk
        procedure(create_vk_dk_Int), deferred :: create_vk_dk
        procedure(def_projs_Int), deferred :: def_projs
        procedure :: get_latt_vecs
end type

abstract interface 

    subroutine init_Int(self)
        import gen_model
        class(gen_model) :: self
    end subroutine

    subroutine create_smat_Int(self,k,smat,atom)
        import gen_model
        import dp
        class(gen_model) :: self
        real(dp), intent(in) :: k(3)
        complex(dp), allocatable, intent(out) :: smat(:,:,:)
        integer, optional, intent(in) :: atom
    end subroutine

    subroutine create_Hk_Int(self,k,Hk)
        import gen_model
        import dp
        class(gen_model) :: self
        real(dp), intent(in) :: k(3)
        complex(dp), allocatable, intent(out) :: Hk(:,:)
    end subroutine

    subroutine create_vk_Int(self,k,vk,ind)
        import gen_model
        import dp
        class(gen_model) :: self
        real(dp), intent(in) :: k(3)
        complex(dp), allocatable, intent(out) :: vk(:,:,:)
        integer, intent(in), optional :: ind
    end subroutine

    subroutine create_vk_dk_Int(self,k,vk_dk)
        import gen_model
        import dp
        class(gen_model) :: self
        real(dp), intent(in) :: k(3)
        complex(dp), allocatable, intent(out) :: vk_dk(:,:,:,:)
    end subroutine

    subroutine def_projs_Int(self,projs)
        use input, only: projs_type
        import gen_model
        class(gen_model) :: self
        type(projs_type), intent(in) :: projs
    end subroutine

end interface

contains
    subroutine get_latt_vecs(self,latt_vecs)
        class(gen_model) :: self
        real(dp) :: latt_vecs(3,3)

        call read_structure(latt_vecs)
    end subroutine

end module
