"""
This is a program for constructing an sd tight-binding models.

The model is stored in class Model, which also provides function
for writing the Hamiltonian to a tb_hr.dat file in a format that
can be read by the fortran linear response code.

The model is defined in input file model.in, which has an .ini
syntax.

This program when it is run reads the model.in file and writes the
tb_hr.dat file, but it can also be used as a library.
"""


import sys
from datetime import datetime
from backports import configparser
import argparse
import itertools
import sympy as sp
from sympy import sympify as spf
from sympy import I,N
from sympy.physics.matrices import msigma
from sympy.physics.quantum import TensorProduct
from sympy.functions import re,im,exp,Abs,cos,sin

import numpy as np
from scipy.constants import hbar


parser = argparse.ArgumentParser()
parser.add_argument('-sc2',default=-1000)
args = parser.parse_args()

sc2 = int(args.sc2)

def sym2num(mat):
    return np.array(mat).astype(np.complex_)

class Model:
    """
    This class stores the model.

    Parameters of the model are read from an input file.
    """

    def __init__(self,inp_file):
        """
        The parameters of the model are read from inp_file.

        The input file has an .ini like syntax.
        """
        config = configparser.ConfigParser()
        config.read(inp_file)

        dim = int(config['structure']['dim'])

        self.dim = dim

        latt = config['structure']['lattice'].split('\n')
        B = sp.zeros(dim)
        for i in range(dim):
            for j in range(dim):
                B[i,j] = spf(latt[i+1].split()[j])

        B = B * spf(config['structure']['lattice_scale'])

        self.B = B

        n_atoms = int(config['structure']['n_atoms'])
        atoms = sp.zeros(n_atoms,dim)
        at = config['structure']['atoms'].split('\n')
        for i in range(n_atoms):
            for j in range(dim):
                atoms[i,j] = spf(at[i+1].split()[j])

        self.n_atoms = atoms.shape[0]
        self.n_orbs = self.n_atoms*2
        self.atoms = atoms

        t = spf(config['tb']['hopping'])
        J = spf(config['tb']['exchange'])
        self.t = t
        self.J = J
        mags = sp.zeros(n_atoms,3)
        ms = config['tb']['mag_moms'].split('\n')
        for i in range(n_atoms):
            for j in range(3):
                mags[i,j] = spf(ms[i+1].split()[j])

        #we normalize the magnetic moments
        for at in range(n_atoms):
            if mags[at,:].norm() > 1e-6:
                mags[at,:] = mags[at,:] / mags[at,:].norm()

        self.mags = mags

        if config.has_option('tb','nns'):
            self.nns = spf(config['tb']['nns'])
        else:
            self.nns = 1

        if self.nns > 1:
            self.nn_scaling = config['tb']['nn_scaling']

        self.debug = True
        
        print('{} starting to create Hr'.format(datetime.now().strftime("%H:%M:%S")))
        self.Hr = self.create_Hr()

    def t_scale(self,dist,dist_nn):
        """
        If we consider also neighbors beyond the nearest neighbor than we scale the hopping for these.

        """

        if self.nns == 1:
            return self.t
        else:
            if self.nn_scaling == '2':
                return self.t*dist_nn**2/dist**2
            elif self.nn_scaling == '1': 
                return self.t

    def find_nn(self):
        """
        Finds nearest neighbours.

        Only finds the nearest neighbourse, but could be easily modified for
        next to nearest etc.

        Returns:
            nn: A list, which for each atom contains a list of all nearest neighbours
                up to nth-nearest neighbors, where n=self.nns.
                Nearest neighbours have format:
                [atom_index,unit cell coordinates of the neighbor,distance,n],
                where n means order of the neighbor, i.e., it's nth nearest neighbor.
        """

        dim = self.dim

        ncells = 2 #ncells determines how many cells we use to look for nearest neighbors

        B = self.B
        atoms = self.atoms

        prec = 1e-5

        def find_position(n,atom_n):
            """
            Finds position of atom with index atom_n in unit cell with coordinates given by n.
            n is given in fractional coordinates.
            """

            #first find position of the atom in the 1. unit cell
            position = sp.zeros(1,dim)
            for i in range(dim):
                for j in range(dim):
                    position[j] += B[i,j]*atoms[atom_n,i]
            #now move to correct unit cell:
            for j in range(dim):
                for i in range(dim):
                    position[j] += B[i,j]*n[i]
            return position


        #this builds a list, which runs over nearest cells 
        iter1 = range(-ncells,ncells+1,1)
        iterlist = []
        for i in range(dim):
            iterlist.append(iter1)
        cells_iter = list(itertools.product(*iterlist))

        nn = []
        #loop over all atoms
        for a in range(self.n_atoms):
            neighbs = [] #neighbs contains all neighbors in the cells we consider for each atom
            for n in cells_iter: #loop over nearest cells
                for a2 in range(self.n_atoms): #loop over all atoms in given cell
                    dist = find_position(n,a2)-find_position([0]*dim,a) 
                    neighbs.append([a2,n,dist.norm()])

            #we sort the neighbors by distance
            neighbs = sorted(neighbs, key=lambda tup:tup[2])
            neighbs.pop(0) #remove the first one since it is the atom itself
            #count the order of each neighbor
            for i,ne in enumerate(neighbs):
                if i == 0:
                    n_nn = 1
                else:
                    if Abs(ne[2]-neighbs[i-1][2]) > 1e-5:
                        n_nn += 1
                ne.append(n_nn)
                
            #now we select only the nearest neighbors up to order self.nns
            nn_a = []    
            for ne in neighbs:
                #if self.debug:
                    #if ne[3] < 10:
                    #    print('printing ne')
                    #    print ne
                if ne[3] <= self.nns:
                    nn_a.append(ne)
            nn.append(nn_a)

        return nn

    def create_Hr(self):
        """
        This creates a tight-binding Hamiltonian in a similar format to that of wannier90.

        Returns: Hr: format is a dictionary, where each key has a format:
            (n,i,j), where are cell fractional coordinates and i,j are orbital numbers
            in this case, there are only two orbitals per atom, spin-up orbital on atom i
            has orbital number i+1, spin-down i+1+n_atoms (we start the counting from 1)
            Hr[n0,n1,i,j] is <0,0,i|H|n0,n1,j>
        """

        print('{} finding nearest neighbours'.format(datetime.now().strftime("%H:%M:%S")))
        nn = self.find_nn()
        print('{} nearest neighbors finished'.format(datetime.now().strftime("%H:%M:%S")))
        Hr = {}
        #first we add the hoppings
        for i in range(self.n_atoms):
            #finds the nearest neighbor distance
            dist_nn = nn[i][0][3]
            for neg in nn[i]:
                #construct the key
                cell = [0]*self.dim
                for j in range(self.dim):
                    cell[j] = neg[1][j]
                cell = tuple(cell)
                key = (cell,i+1,neg[0]+1)
                #add the hopping
                #if we consider more than nearest neighbors than we scale the hopping according
                #to the distance
                if key in Hr:
                    Hr[key] += -self.t_scale(neg[2],dist_nn)
                else:
                    Hr[key] = -self.t_scale(neg[2],dist_nn)
                #the Hamiltonian has to be Hermitian
                key2 = (cell,i+1+self.n_atoms,neg[0]+1+self.n_atoms)
                if key2 in Hr:
                    Hr[key2] += -self.t_scale(neg[2],dist_nn)
                else:
                    Hr[key2] = -self.t_scale(neg[2],dist_nn)

        #this constructs the spin-operators projected on individual atoms
        ms = sp.zeros(self.n_atoms*2)
        for at in range(self.n_atoms):
            proj = sp.zeros(self.n_atoms)
            proj[at,at] = 1
            for i in range(3):
                ms += self.mags[at,i]*TensorProduct(msigma(i+1),proj)

        #add the exchange part
        for i in range(self.n_atoms*2):
            for j in range(self.n_atoms*2):
                cell = (0,)*self.dim
                key = (cell,i+1,j+1)
                if key in Hr:
                    Hr[key] += self.J*ms[i,j]
                else:
                    Hr[key] = self.J*ms[i,j]

        print('Hr-----')
        for key in Hr:
            print(key,Hr[key])

        return Hr

    def write_wann_Hr(self):
        """
        This prints the Hr Hamiltonian in a file wannier90_hr.dat in the same format
        as wannier90 uses.

        !!!This is not the correct way how to do it and is left here only as a reference.!!!
        """

        Hr = self.create_Hr()
        rs = []
        for key in Hr:
            r = (key[0]) 
            if r not in rs: 
                rs.append(r)
        n_rs = len(rs)

        with open('wannier90_hr.dat','w') as f:
            f.write('\n')
            f.write(str(self.n_atoms*2)+'\n')
            f.write(str(n_rs)+'\n')
            nrlines = n_rs/15+1
            for l in range(nrlines):
                for i in range(15):
                    if l*15 + i < n_rs:
                        f.write('1  ')
                f.write('\n')
            for key in sorted(Hr):
                f.write(str(key[0][0])+'  ')
                try:
                    f.write(str(key[0][1])+'  ')
                except:
                    f.write(str(0)+'  ')
                try:
                    f.write(str(key[0][2])+'  ')
                except:
                    f.write(str(0)+'  ')

                f.write(str(key[1])+'  ')
                f.write(str(key[2])+'  ')

                f.write(str(float(re(Hr[key])))+ '  ')
                f.write(str(float(im(Hr[key]))))
                
                f.write('\n')

        with open('POSCAR','w') as f:
            f.write('\n')
            f.write('1\n')
            Bout = sp.eye(3)
            Bout[0:self.dim,0:self.dim] = self.B[:,:]
            for i in range(3):
                for j in range(3):
                    f.write(str(float(Bout[i,j]))+'  ')
                f.write('\n')

    def write_Hr(self):
        """
        This prints the Hamiltonian in the tight-binding basis in a format
        that can be easily used for transforming to a k-dependent Hamiltonian.
        It writes a tb_hr.dat file.
        """

        Hr = self.Hr

        with open('tb_hr.dat','w') as f:
            f.write('\n')
            f.write(str(self.n_atoms*2)+'\n')
            f.write(str(len(Hr))+'\n')
            for key in sorted(Hr):
                R = sp.zeros(1,3)
                at_a = (key[2]-1) % self.n_atoms
                at_b = (key[1]-1) % self.n_atoms
                for i in range(self.dim):
                    R[i] += key[0][i] - self.atoms[at_b,i]  + self.atoms[at_a,i]
                    f.write(str(float(R[i]))+'  ')
                for i in range(self.dim,3):
                    f.write('0  ')
                
                f.write(str(key[1])+'  ')
                f.write(str(key[2])+'  ')

                f.write(str(float(re(Hr[key])))+ '  ')
                f.write(str(float(im(Hr[key]))))
                
                f.write('\n')

        with open('POSCAR','w') as f:
            f.write('\n')
            f.write('1\n')
            Bout = sp.eye(3)
            Bout[0:self.dim,0:self.dim] = self.B[:,:]
            for i in range(3):
                for j in range(3):
                    f.write(str(float(Bout[i,j]))+'  ')
                f.write('\n')

    def create_Hr2(self):
        Hr = self.Hr
        Hr2 = {}
        r = (1000,1000,1000)
        for key in sorted(Hr):
            if key[0] != r:
                r = key[0]
                Hr2[r] = np.zeros((self.n_orbs,self.n_orbs),dtype=np.complex_)

        for key in sorted(Hr2):
            for a in range(self.n_atoms*2):
                for b in range(self.n_atoms*2):
                    key2=(key,a+1,b+1)
                    if key2 in Hr:
                        Hr2[key][a,b] = complex(Hr[key2])
            #print key
            #sp.pprint(Hr2[key])

        return Hr2

    def create_sc2(self,f):
        print 'Using sc2 switch', f
        Hr2 = self.create_Hr2()
        sc2 = {}
        sigma_np = []
        for i in range(3):
            sigma_np.append(sym2num(TensorProduct(msigma(i+1),sp.eye(self.n_atoms))))
        for i in range(3):#spin index
            for j in range(3):#the position index
                sc2[(i,j)] = {}
                for r in sorted(Hr2):
                    sc2_1 = np.dot(Hr2[r],sigma_np[i])
                    sc2_2 = np.dot(sigma_np[i],Hr2[r])
                    r_c = self.lat2cart(r)
                    for a in range(self.n_atoms*2):
                        for b in range(self.n_atoms*2):
                            at_a = a % self.n_atoms
                            at_b = b % self.n_atoms
                            ra = self.lat2cart(self.atoms[at_a,:])
                            rb = self.lat2cart(self.atoms[at_b,:])
                            if f == 0:
                                sc2_1[a,b] = sc2_1[a,b] * (rb[j]+r_c[j]-ra[j])
                                sc2_2[a,b] = sc2_2[a,b] * (rb[j]+r_c[j]-ra[j])
                            elif f == 1:
                                sc2_1[a,b] = sc2_1[a,b] * (rb[j]+r_c[j])
                                sc2_2[a,b] = sc2_2[a,b] * ra[j]
                            elif f == 2:
                                sc2_1[a,b] = sc2_1[a,b] * (rb[j]+r_c[j]-ra[j])
                            elif f == 3:
                                sc2_1[a,b] = sc2_1[a,b] * ra[j]
                                sc2_2[a,b] = sc2_2[a,b] * ra[j]
                            elif f == 4:
                                sc2_1[a,b] = sc2_1[a,b] * (ra[j] + rb[j])
                                sc2_2[a,b] = sc2_2[a,b] * (ra[j] + rb[j])
                            else:
                                sys.exit('wrong sc2')
                    if f == 0:
                        sc2[(i,j)][r] = 0.5 * (sc2_1+sc2_2)
                    elif f == 1:
                        sc2[(i,j)][r] = sc2_1 - sc2_2
                    elif f == 2:
                        sc2[(i,j)][r] = sc2_1
                    elif f == 3:
                        sc2[(i,j)][r] = sc2_1 - sc2_2
                    elif f == 4:
                        sc2[(i,j)][r] = sc2_1 - sc2_2

        return sc2

    def write_sc2(self,ff):
        sc2 = self.create_sc2(ff)
        nnz = {}
        for i in range(3):
            for j in range(3):
                nnz[(i,j)] = 0
                for r in sorted(sc2[(i,j)]):
                    for a in range(self.n_orbs):
                        for b in range(self.n_orbs):
                            if np.absolute(sc2[(i,j)][r][a,b]) > 1e-10:
                                nnz[(i,j)] += 1
        #finds maximum nnz
        nnz_max = max(nnz.iteritems(),key=(lambda key: key[1]))[1]
        with open('tb_sc2.dat','w') as f:
            f.write(str(ff)+'\n')
            f.write(str(nnz_max)+'\n')
            for i in range(3):
                for j in range(3):
                    f.write(str(i)+'  ' + str(j)+'\n')
                    f.write(str(nnz[(i,j)])+'\n')
                    for r in sorted(sc2[(i,j)]):
                        for a in range(self.n_orbs):
                            for b in range(self.n_orbs):
                                if np.absolute(sc2[(i,j)][r][a,b]) > 1e-10:
                                    at_a = a % self.n_atoms
                                    at_b = b % self.n_atoms
                                    R = sp.zeros(1,3)
                                    for k in range(self.dim):
                                        R[k] += r[k] - self.atoms[at_a,k]  + self.atoms[at_b,k]
                                        f.write(str(float(R[k]))+'  ')
                                    for k in range(self.dim,3):
                                        f.write('0  ')
                                    f.write(str(a+1) + '  ' + str(b+1) + '  ')
                                    f.write(str(sc2[(i,j)][r][a,b].real)+ '  ')
                                    f.write(str(sc2[(i,j)][r][a,b].imag))
                                    f.write('\n')

    def create_Hk(self,k):

        Hr = self.Hr
        Hk = sp.zeros(self.n_atoms*2)

        for key in Hr:

            R = sp.zeros(1,self.dim)
            for i in range(self.dim):
                R += self.B[i,:]*key[0][i] 

            pa = sp.zeros(1,self.dim)
            pb = sp.zeros(1,self.dim)
            for i in range(self.dim):
                at_a = (key[2]-1) % self.n_atoms
                at_b = (key[1]-1) % self.n_atoms
                pa += self.B[i,:]*self.atoms[at_a,i] 
                pb += self.B[i,:]*self.atoms[at_b,i] 

            rho = R + pa - pb
            rho2 = sp.zeros(1,3)
            for i in range(self.dim):
                rho2[i] = rho[i]
            Hk[key[1]-1,key[2]-1] += exp(I*k.dot(rho2))*Hr[key]

        return Hk

    def spin_rot(self,atom,n,phi):

        n = n / n.norm()

        #creates the projected pauli matrices
        proj = sp.zeros(self.n_atoms)
        proj[atom,atom] = 1
        sigma_n = sp.zeros(self.n_atoms*2,self.n_atoms*2)
        for i in range(3):
            sigma_n += n[i]*TensorProduct(msigma(i+1),proj)

        s_rot = cos(phi/2)*TensorProduct(sp.eye(2),proj)+ I*sigma_n*sin(phi/2)

        return s_rot

    def lat2cart(self,vec):
        """converts a vector in lattice coordinates to the cartesian coordinates
        """
        vec_c = sp.zeros(1,self.dim)
        
        for i in range(self.dim):
            vec_c += self.B[i,:]*vec[i]

        return vec_c

if __name__ == '__main__':
    print('{} starting'.format(datetime.now().strftime("%H:%M:%S")))
    model = Model('model.in')

    print('{} Model Initialized'.format(datetime.now().strftime("%H:%M:%S")))
    model.write_Hr()

    print('{} Hamiltonain written'.format(datetime.now().strftime("%H:%M:%S")))

    #if sc2 > -1000:
    #    print 'writting sc2'
    #    model.write_sc2(sc2)
