SHELL = /bin/sh

json_fortran_path = /home/kuba/bin/json-fortran/lib/
F95ROOT = /home/kuba/bin/mkl_95_interfaces/compiled
MKLROOT = /opt/intel/compilers_and_libraries_2018.2.199/linux/mkl
link = ${F95ROOT}/lib/intel64/libmkl_blas95_lp64.a ${F95ROOT}/lib/intel64/libmkl_lapack95_lp64.a -Wl,--start-group ${MKLROOT}/lib/intel64/libmkl_gf_lp64.a ${MKLROOT}/lib/intel64/libmkl_sequential.a ${MKLROOT}/lib/intel64/libmkl_core.a -Wl,--end-group -lpthread -lm -ldl $(json_fortran_path)/libjsonfortran.a
compile = -I${F95ROOT}/include/intel64/lp64 -m64 -I${MKLROOT}/include -I$(json_fortran_path)/mod

objects = constants.o input.o generic_model.o wann_model.o tb_model.o eval_k.o k_integrate.o 

DEBUG ?= 0
ifeq ($(DEBUG),1)
    opt = -fcheck=all -fbacktrace
else
    opt = -O3 
endif

f90comp = mpif90

linres.x: $(objects) linres.o
	$(f90comp) -o linres.x $(objects) linres.o $(opt) $(link)

bands.x: $(objects) bands.o
	$(f90comp) -o bands.x $(objects) bands.o $(opt) $(link)

mag_moms.x: $(objects) mag_moms.o
	$(f90comp) -o mag_moms.x $(objects) mag_moms.o $(opt) $(link)

test.x: $(objects) test.o
	$(f90comp) -o test.x $(objects) test.o $(opt) $(link)

%.o: %.f90
	$(f90comp) -c $< $(compile) $(opt)

.PHONY: clean
clean:
	rm *.o *.mod
