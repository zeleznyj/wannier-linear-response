module constants
implicit none

character(len=10), parameter :: version = '0.4.25'

!constants, units, etc
integer, parameter :: dp=kind(0.d0)
real(dp),  parameter :: pi  = 4 * atan (1.0_dp)
complex(dp), parameter :: ii = dcmplx(0_dp,1_dp)
real(dp), parameter :: bohr = 0.52917721067121_dp ![Ang] atomic unit of length in Ansgstrom
real(dp), parameter :: hbar = 6.582119514e-16_dp ![eVs] Reduced Planck's constants
real(dp), parameter :: eele = 1.6021766208e-19_dp ![C] Elementary charge
real(dp), parameter :: eV = 1.602176565e-19_dp !eV
real(dp), parameter :: kB = 8.6173303e-5_dp ! [ev/K] Boltzmann constant

!some parameters
character(len=100), parameter :: input_file = 'input'
character(len=100), parameter :: struct_file = 'input_latt'
character(len=100), parameter :: projs_file = 'input_projs'
character(len=100), parameter :: bands_input_file = 'input_bands'


end module
