module k_resolved
    
use mpi
use constants
use input, only: input_type, write_input, get_tensor_rank
use k_integrate, only: find_recvecs, distribute_k,kindex2k
use eval_k, only: eigen,response_k
use generic_model
use response, only: get_unit_conversion_factor
implicit none

contains

function kindex2k_2(i,nk) result(k)
    integer, intent(in) :: i
    integer, intent(in) :: nk(3)
    integer :: k(3)

    k(3) = (i-1)/(nk(2)*nk(1))
    k(2) = (i-1-k(3)*nk(2)*nk(1))/nk(1)
    k(1) = i-1-k(3)*nk(2)*nk(1)-k(2)*nk(1)

end function

subroutine calculate_k_resolved_para(inp,model)

    type(input_type), intent(in) :: inp 
    class(gen_model), allocatable, intent(in) :: model

    integer :: k1,k2,k3
    integer :: nk(3),nk1,nk2,normal,n_k_local,ki(3),nk_tot
    integer :: i,p,l,ls,lf,i_l
    real(dp) :: kf(3),k(3)
    real(dp) :: latt_vecs(3,3)
    real(dp) :: rec_vecs(3,3)
    real(dp) :: vol
    real(dp), allocatable :: X(:,:),X_all3(:,:,:),X_all2(:,:,:,:),X3_local(:,:,:)
    real(dp), allocatable :: X_bands2(:,:,:),X_bands3_local(:,:)
    complex(dp), allocatable :: Hk(:,:)
    real(dp), allocatable :: w(:)
    real(dp) :: f
    integer :: rank,ierr,ntasks
    type(MPI_File) :: fh
    type(MPI_Status) :: statuss
    integer (kind = MPI_OFFSET_KIND) :: offset,error
    

    call mpi_comm_rank(mpi_comm_world,rank,ierr)

    call model%get_latt_vecs(latt_vecs)
    call find_recvecs(latt_vecs,rec_vecs,vol)

    nk = inp%nk

    f = get_unit_conversion_factor(inp,model)

    allocate(X(inp%n_X,inp%n_params))

    open(unit=101,file=inp%k_resolved_input%output_file//'.spec',status='replace')

    write(101,*) 'Version: ', version
    write(101,*) 'Mode: ', inp%mode

    if (inp%k_resolved_input%d == 2) then

        if (rank == 0) then

            open(unit=100,file=inp%k_resolved_input%output_file//'.out',status='replace',access='stream')

            normal = inp%k_resolved_input%normal

            if (normal == 1) then
                nk1 = nk(2)
                nk2 = nk(3)
            elseif (normal == 2) then
                nk1 = nk(1)
                nk2 = nk(3)
            elseif (normal == 3) then
                nk1 = nk(1)
                nk2 = nk(2)
            endif

            if ( inp%mode == 'k_resolved' ) then
                allocate(X_all2(nk1,nk2,inp%n_X,inp%n_params))
            elseif ( inp%mode == 'bands' ) then
                allocate(X_bands2(nk1,nk2,model%n_orb))
            endif

            do k1=0,nk1-1
                do k2=0,nk2-1

                    if (normal == 1) then
                        kf(1) = inp%k_resolved_input%k_fixed
                        kf(2) = dble(k1)/dble(nk1)
                        kf(3) = dble(k2)/dble(nk2)
                    elseif (normal == 2) then
                        kf(1) = dble(k1)/dble(nk1)
                        kf(2) = inp%k_resolved_input%k_fixed
                        kf(3) = dble(k2)/dble(nk2)
                    elseif (normal == 3) then
                        kf(1) = dble(k1)/dble(nk1)
                        kf(2) = dble(k2)/dble(nk2)
                        kf(3) = inp%k_resolved_input%k_fixed
                    endif

                    k(:) = kf(1) * rec_vecs(:,1) + kf(2) * rec_vecs(:,2) + &
                        kf(3) * rec_vecs(:,3)

                    if ( inp%mode == 'k_resolved' ) then
                        call response_k(k,model,inp,X)
                        X_all2(k1+1,k2+1,:,:) = X
                    elseif ( inp%mode == 'bands' ) then
                        call model%create_Hk(k,Hk)
                        call eigen(Hk,w)
                        X_bands2(k1+1,k2+1,:) = w
                    endif

                end do
            end do


            if ( inp%mode == 'k_resolved' ) then
                X_all2 = X_all2 * f
                !reshape to C order to be consistent with d=3
                write(100) reshape(X_all2,[inp%n_params,inp%n_X,nk2,nk1],order=[4,3,2,1])
                write(101,*) 'Shape: ', nk1,nk2,inp%n_X,inp%n_params
            elseif ( inp%mode == 'bands' ) then
                write(100) reshape(X_bands2,[model%n_orb,nk2,nk1],order=[3,2,1])
                write(101,*) 'Shape: ', nk1,nk2,model%n_orb                
            endif

        else
            write(*,*) 'WARNING: No parallelization is implemented for d=2, only one process used!!!'
        endif

    elseif (inp%k_resolved_input%d == 3) then

        nk_tot = nk(1)*nk(2)*nk(3)

        !if ( rank == 0) then
        !    allocate(X_all3(nk_tot,inp%n_X,inp%n_params))
        !endif

        if (rank == 0) then
            call MPI_File_delete(inp%k_resolved_input%output_file//'_para.out', MPI_INFO_NULL, ierr)
        endif

        call MPI_File_open(mpi_comm_world,inp%k_resolved_input%output_file//'.out', &
                           MPI_MODE_CREATE+MPI_MODE_RDWR, MPI_Info_null,fh,ierr)

        call mpi_comm_size(mpi_comm_world,ntasks,ierr)

        call distribute_k(nk_tot,ntasks,rank,ls,lf)

        n_k_local = lf-ls+1
        if ( inp%mode == 'k_resolved' ) then
            allocate(X3_local(n_k_local,inp%n_X,inp%n_params))
        elseif ( inp%mode == 'bands' ) then
            allocate(X_bands3_local(n_k_local,model%n_orb))
        endif

        i_l = 0
        do l = ls,lf
            i_l = i_l + 1
            !ki = kindex2k_2(l,nk)
            ki = kindex2k(l,nk)

            kf(1) = dble(ki(1))/dble(nk(1))
            kf(2) = dble(ki(2))/dble(nk(2))
            kf(3) = dble(ki(3))/dble(nk(3))

            k(:) = kf(1) * rec_vecs(:,1) + kf(2) * rec_vecs(:,2) + &
                kf(3) * rec_vecs(:,3)

            if ( inp%mode == 'k_resolved' ) then
                call response_k(k,model,inp,X)
                X3_local(i_l,:,:) = X
            elseif ( inp%mode == 'bands' ) then
                call model%create_Hk(k,Hk)
                call eigen(Hk,w)
                X_bands3_local(i_l,:) = w
            endif

        enddo

        error = 0
        call MPI_File_set_view(fh,error,mpi_double_precision,mpi_double_precision,'native',MPI_INFO_NULL,ierr)


        if ( inp%mode == 'k_resolved' ) then

            X3_local = X3_local * f

            offset = (ls-1)*inp%n_X*inp%n_params
            call MPI_File_write_at_all(fh,offset,reshape(X3_local,[inp%n_params,inp%n_X,n_k_local], &
                order=[3,2,1]),n_k_local*inp%n_X*inp%n_params,mpi_double_precision,statuss,ierr)

        elseif ( inp%mode == 'bands' ) then

            offset = (ls-1)*model%n_orb
            call MPI_File_write_at_all(fh,offset,reshape(X_bands3_local,[model%n_orb,n_k_local], &
                order=[2,1]),n_k_local*model%n_orb,mpi_double_precision,statuss,ierr)

        endif
        
        !call MPI_File_write_at(fh,0,X3_local,nk_tot*inp%n_X*inp%n_params,mpi_double_precision,statuss,ierr)
        !call MPI_File_write_at(fh,offset,X3_local,nk_tot*inp%n_X*inp%n_params,mpi_double_precision,statuss,ierr)
        !call MPI_File_write(fh,X3_local,nk_tot*inp%n_X*inp%n_params,mpi_double_precision,statuss,ierr)

        call MPI_File_close(fh,ierr)

        if (rank == 0 ) then

            if ( inp%mode == 'k_resolved' ) then
                write(101,*) 'Shape: ', nk(1),nk(2),nk(3),inp%n_X,inp%n_params
            elseif ( inp%mode == 'bands' ) then
                write(101,*) 'Shape: ', nk(1),nk(2),nk(3),model%n_orb
            endif

        endif
    endif

    if (rank == 0) then
        write(101,*) 'k-dimension: ', inp%k_resolved_input%d
        if ( inp%mode == 'k_resolved' ) then
            write(101,*) 'Tensor rank: ', get_tensor_rank(inp)
            write(101,*) 'n_params: ', inp%n_params

            write(101,*) 'Params order: (n_param: n_formula,n_E_F,n_gamma,n_proj)'
            do p=1,inp%n_params
                write(101,*) p,':',inp%params%get(p)
            end do
        endif
    endif

end subroutine


end module
